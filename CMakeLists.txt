cmake_minimum_required(VERSION 2.8)

# include utilty macros
include(${CMAKE_SOURCE_DIR}/cmake/Macros.cmake)

# include configuration
include(${CMAKE_SOURCE_DIR}/cmake/Config.cmake)

# Disable building in source
if("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")
	message(SEND_ERROR "In-source builds are not allowed.")
endif("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")

# version number settings
set(LAB_VERSION_MAJOR 0)
set(LAB_VERSION_MINOR 1)
set(LAB_VERSION_PATCH 0)
set(LAB_VERSION ${LAB_VERSION_MAJOR}.${LAB_VERSION_MINOR}.${LAB_VERSION_PATCH})

# variables for internal usage
set(LAB_ROOT_DIR "${CMAKE_SOURCE_DIR}")
set(LAB_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/include")
set(LAB_LIBS_DIR "${CMAKE_SOURCE_DIR}/libs")

# cmake configuration
set(CMAKE_MODULE_PATH				${CMAKE_SOURCE_DIR}/cmake/Modules)
set(CMAKE_CXX_FLAGS                	"-Wall -std=c++11")
set(CMAKE_CXX_FLAGS_DEBUG          	"-O0 -g")
set(CMAKE_CXX_FLAGS_MINSIZEREL     	"-Os -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELEASE        	"-O4 -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO 	"-O2 -g")

# options exposed to the user
lab_option(BUILD_SHARED_LIBS ON BOOL "Build shared libraries.")

# variables exposed to the user
lab_option(LAB_OUTPUT_DIR "${CMAKE_SOURCE_DIR}/bin" STRING "The output directory of our targets.")
lab_option(LAB_BUILD_EXAMPLES ON BOOL "Build examples project.")
lab_option(LAB_BUILD_TEST ON BOOL "Build test project.")

# we must build the base library
add_subdirectory(src)

# build tests
if(LAB_BUILD_TEST)
	add_subdirectory(test)
endif()	

# build examples
if(LAB_BUILD_EXAMPLES)
	add_subdirectory(examples)
endif()
