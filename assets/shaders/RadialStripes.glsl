-- Vertex

in vec2 Position;
out vec2 vertPos;

void main (void) {
   gl_Position = vec4(Position, 0.0, 1.0);
   vertPos = gl_Position.xy;
}

-- Fragment

uniform float Time;
uniform vec2 SunPos;
uniform vec3 Sun1, Sun2, Sky1, Sky2;
in vec2 vertPos;
out vec3 fragColor;

void main (void) {
   vec2 v = vertPos - SunPos;
   float l = length(v);
   float a = (sin(l)+atan(v.y, v.x))/3.1415;

   if (l < 0.05)
       fragColor = Sun1;
   else if (int(24*(Time*0.1 + 1.0 + a)) % 2 == 0)
       fragColor = mix(Sun1, Sun2, l);
   else
       fragColor = mix(Sky1, Sky2, l);
}