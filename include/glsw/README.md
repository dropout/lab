The OpenGL Shader Wrangler
==========================

[http://prideout.net/blog/?p=11](Link to blogpost)

API
---

```
int glswInit();
int glswShutdown();
int glswSetPath(const char* pathPrefix, const char* pathSuffix);
const char* glswGetShader(const char* effectKey);
const char* glswGetError();
int glswAddDirectiveToken(const char* token, const char* directive);
```

Terminology
-----------

effect
Simple text file that contains a bundle of shaders in any combination. For example, an effect might have 3 vertex shaders and 1 fragment shader. It might have 0 vertex shaders, or it might contain only tessellation shaders. Effects should not be confused with OpenGL program objects. A program object is a set of shaders that are linked together; an effect is just a grouping of various shader strings.

shader key
Identifier for a shader consisting of alphanumeric characters and periods. In a way, the periods serve as path separators, and the shader key is a path into a simple hierarchy of your own design. However, GLSW does not maintain a hierarchy; it stores shaders in a flat list.

effect key
Same as a shader key, but prefaced with a token for the effect. Your C/C++ code uses an effect key when requesting a certain shader string.

token
Contiguous sequence of alphanumeric characters in a shader key (or effect key) delimited by periods.

section divider
Any line in an effect file that starts with two dash characters (--). If the dashes are followed by a shader key, then all the text until the next section divider officially belongs to the corresponding shader.

directive
Any line of GLSL code that starts with a pound (#) symbol.

Example
-------

Blit.glsl

```
-- Vertex

in vec4 Position;
void main() {
    gl_Position = Position;
}

-- Fragment

uniform sampler2D Sampler;
out vec4 FragColor;
void main() {
    ivec2 coord = ivec2(gl_FragCoord.xy);
    FragColor = texelFetch(Sampler, coord, 0);
}
```

main.cpp

```
glswInit();
glswSetPath("./", ".glsl");
const char* vs = glswGetShader("Blit.Vertex");
const char* fs = glswGetShader("Blit.Fragment");
// ... use vs and gs here ...
glswShutdown();
```