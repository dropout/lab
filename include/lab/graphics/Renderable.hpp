#ifndnef LAB_GRAPHICS_RENDERABLE_HPP
#define LAB_GRAPHICS_RENDERABLE_HPP

namespace lab { namespace graphics {

class Renderable {

    public:
        Renderable () {};
        virtual ~Renderable () {}

    protected:
        virtual void render (Renderer& renderer) = 0;

};


}}

#endif // LAB_GRAPHICS_RENDERABLE_HPP