#ifndnef LAB_SG_IMPL_CAMERA_HPP
#define LAB_SG_IMPL_CAMERA_HPP

#include <glm/vec4.hpp>
#include <glm/gtc/quaternion.hpp>

class Camera {

    public:
        Camera ();
        virtual ~Camera () {}

        void setTarget (const float& x, const float& y, const float& z);
        void setPosition ();


    protected:

        float xPos;
        float yPos;
        float zPos;

        float[4] target;
        float[4] position;


        /*

        glm::mat4 projectionMat;
        GLint projectionMatLoc;
        glm::mat4 viewMat;
        GLint viewMatLoc;

        bool bEnableMouseMiddleButton;
        bool bApplyInertia;
        bool bDoTranslate;
        bool bDoRotate;
        bool bDoScrollZoom;
        bool bInsideArcball;
        bool bMouseInputEnabled;
        bool bDistanceSet;
        bool bAutoDistance;
        float lastDistance;

        float drag;

        float xRot;
        float yRot;
        float zRot;

        float moveX;
        float moveY;
        float moveZ;

        float sensitivityXY;
        float sensitivityZ;
        float sensitivityRot;

        float rotationFactor;

        ofVec2f lastMouse, prevMouse;
        ofVec2f mouseVel;

        void updateRotation();
        void updateTranslation();
        void update(ofEventArgs & args);
        void mousePressed(ofMouseEventArgs & mouse);
        void mouseReleased(ofMouseEventArgs & mouse);
        void mouseDragged(ofMouseEventArgs & mouse);
        void mouseScrolled(ofMouseEventArgs & mouse);
        void updateMouse(const ofMouseEventArgs & mouse);

        /// \brief The key used to differentiate between translation and rotation.
        char doTranslationKey;

        /// \brief The time of the last pointer down event.
        unsigned long lastTap;

        /// \brief The current rotation quaternion.
        ofQuaternion curRot;

        /// \brief The previous X axis.
        ofVec3f prevAxisX;

        /// \brief The previous Y axis.
        ofVec3f prevAxisY;

        /// \brief The previous Z axis.
        ofVec3f prevAxisZ;

        /// \brief the previous camera position.
        ofVec3f prevPosition;

        /// \brief The previous camera orientation.
        ofQuaternion prevOrientation;

        /// \brief The viewport.
        ///
        /// A locally cached viewport results in more efficient calculations.
        ofRectangle viewport;
        */
	
};

#endif // LAB_SG_IMPL_CAMERA_HPP

