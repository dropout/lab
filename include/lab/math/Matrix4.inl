template <typename T>
inline Matrix4<T>::Matrix4() {
	this->setIdentity();
}

template <typename T>
inline Matrix4<T>::Matrix4(const T& num) {
	float temp[16] = {num};
	this->set(temp);
}

template <typename T>
inline Matrix4<T>::Matrix4(const Matrix4<T>& mat) {
	this->set(mat.m);
}

template <typename T>
inline Matrix4<T>::Matrix4(const T (&src)[16], const bool rowMajor) {
	this->set(src, rowMajor);
}

template <typename T>
inline Matrix4<T>::Matrix4(T m0, T m1, T m2, T m3, T m4, T m5, T m6, T m7, T m8, T m9, T m10, T m11, T m12, T m13, T m14, T m15) {
	this->set(m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15);
}

template <typename T>
T& Matrix4<T>::operator [] (const int& num) {
	return this->m[num];
}

template <typename T>
inline Matrix4<T> operator + (const Matrix4<T>& left, const Matrix4<T>& right) {
	Matrix4<T> result(0);
	for (int i=0; i < Matrix4<T>::DIM_SQUARED; i++) {
		result.m[i] = left.m[i] + right.m[i];
	}
	return result;
}

template <typename T>
inline Matrix4<T>& operator += (Matrix4<T>& left, const Matrix4<T>& right) {
	for (int i=0; i < Matrix4<T>::DIM_SQUARED; i++) {
		left.m[i] += right.m[i];
	}
	return left;
}

template <typename T>
inline Matrix4<T> operator - (const Matrix4<T>& left, const Matrix4<T>& right) {
	Matrix4<T> result(0);
	for (int i=0; i < Matrix4<T>::DIM_SQUARED; i++) {
		result.m[i] = left.m[i] - right.m[i];
	}
	return result;
}

template <typename T>
inline Matrix4<T>& operator -= (Matrix4<T>& left, const Matrix4<T>& right) {
	for (int i=0; i < Matrix4<T>::DIM_SQUARED; i++) {
		left.m[i] -= right.m[i];
	}
	return left;
}

template <typename T>
inline Matrix4<T> operator * (const Matrix4<T>& l, const Matrix4<T>& r) {
	Matrix4<T> result(0);
	result.m[0]  = l.m[0]*r.m[0]  + l.m[4]*r.m[1]  + l.m[8]*r.m[2]   + l.m[12]*r.m[3];
	result.m[1]  = l.m[1]*r.m[0]  + l.m[5]*r.m[1]  + l.m[9]*r.m[2]   + l.m[13]*r.m[3];
	result.m[2]  = l.m[2]*r.m[0]  + l.m[6]*r.m[1]  + l.m[10]*r.m[2]  + l.m[14]*r.m[3];
	result.m[3]  = l.m[3]*r.m[0]  + l.m[7]*r.m[1]  + l.m[11]*r.m[2]  + l.m[15]*r.m[3];
	result.m[4]  = l.m[0]*r.m[4]  + l.m[4]*r.m[5]  + l.m[8]*r.m[6]   + l.m[12]*r.m[7];
	result.m[5]  = l.m[1]*r.m[4]  + l.m[5]*r.m[5]  + l.m[9]*r.m[6]   + l.m[13]*r.m[7];
	result.m[6]  = l.m[2]*r.m[4]  + l.m[6]*r.m[5]  + l.m[10]*r.m[6]  + l.m[14]*r.m[7];
	result.m[7]  = l.m[3]*r.m[4]  + l.m[7]*r.m[5]  + l.m[11]*r.m[6]  + l.m[15]*r.m[7];
	result.m[8]  = l.m[0]*r.m[8]  + l.m[4]*r.m[9]  + l.m[8]*r.m[10]  + l.m[12]*r.m[11];
	result.m[9]  = l.m[1]*r.m[8]  + l.m[5]*r.m[9]  + l.m[9]*r.m[10]  + l.m[13]*r.m[11];
	result.m[10] = l.m[2]*r.m[8]  + l.m[6]*r.m[9]  + l.m[10]*r.m[10] + l.m[14]*r.m[11];
	result.m[11] = l.m[3]*r.m[8]  + l.m[7]*r.m[9]  + l.m[11]*r.m[10] + l.m[15]*r.m[11];
	result.m[12] = l.m[0]*r.m[12] + l.m[4]*r.m[13] + l.m[8]*r.m[14]  + l.m[12]*r.m[15];
	result.m[13] = l.m[1]*r.m[12] + l.m[5]*r.m[13] + l.m[9]*r.m[14]  + l.m[13]*r.m[15];
	result.m[14] = l.m[2]*r.m[12] + l.m[6]*r.m[13] + l.m[10]*r.m[14] + l.m[14]*r.m[15];
	result.m[15] = l.m[3]*r.m[12] + l.m[7]*r.m[13] + l.m[11]*r.m[14] + l.m[15]*r.m[15];
	return result;
}

template <typename T>
inline Matrix4<T>& operator *= (Matrix4<T>& l, const Matrix4<T>& r) {
	T temp[16];
	std::memcpy(temp, l.m, Matrix4<T>::MEM_LEN);
	l.m[0]  = temp[0]*r.m[0]  + temp[4]*r.m[1]  + temp[8]*r.m[2]   + temp[12]*r.m[3];
	l.m[1]  = temp[1]*r.m[0]  + temp[5]*r.m[1]  + temp[9]*r.m[2]   + temp[13]*r.m[3];
	l.m[2]  = temp[2]*r.m[0]  + temp[6]*r.m[1]  + temp[10]*r.m[2]  + temp[14]*r.m[3];
	l.m[3]  = temp[3]*r.m[0]  + temp[7]*r.m[1]  + temp[11]*r.m[2]  + temp[15]*r.m[3];	
	l.m[4]  = temp[0]*r.m[4]  + temp[4]*r.m[5]  + temp[8]*r.m[6]   + temp[12]*r.m[7];
	l.m[5]  = temp[1]*r.m[4]  + temp[5]*r.m[5]  + temp[9]*r.m[6]   + temp[13]*r.m[7];
	l.m[6]  = temp[2]*r.m[4]  + temp[6]*r.m[5]  + temp[10]*r.m[6]  + temp[14]*r.m[7];
	l.m[7]  = temp[3]*r.m[4]  + temp[7]*r.m[5]  + temp[11]*r.m[6]  + temp[15]*r.m[7];
	l.m[8]  = temp[0]*r.m[8]  + temp[4]*r.m[9]  + temp[8]*r.m[10]  + temp[12]*r.m[11];
	l.m[9]  = temp[1]*r.m[8]  + temp[5]*r.m[9]  + temp[9]*r.m[10]  + temp[13]*r.m[11];
	l.m[10] = temp[2]*r.m[8]  + temp[6]*r.m[9]  + temp[10]*r.m[10] + temp[14]*r.m[11];
	l.m[11] = temp[3]*r.m[8]  + temp[7]*r.m[9]  + temp[11]*r.m[10] + temp[15]*r.m[11];
	l.m[12] = temp[0]*r.m[12] + temp[4]*r.m[13] + temp[8]*r.m[14]  + temp[12]*r.m[15];
	l.m[13] = temp[1]*r.m[12] + temp[5]*r.m[13] + temp[9]*r.m[14]  + temp[13]*r.m[15];
	l.m[14] = temp[2]*r.m[12] + temp[6]*r.m[13] + temp[10]*r.m[14] + temp[14]*r.m[15];
	l.m[15] = temp[3]*r.m[12] + temp[7]*r.m[13] + temp[11]*r.m[14] + temp[15]*r.m[15];
	return l;
}

template <typename T>
inline Matrix4<T> operator * (const Matrix4<T>& left, const T& right) {
	Matrix4<T> result(0);
	for (int i=0; i < Matrix4<T>::DIM_SQUARED; i++) {
		result.m[i] = left.m[i] * right;
	}
	return result;
}

template <typename T>
inline Matrix4<T>& operator *= (Matrix4<T>& left, const T& right) {	
	for (int i=0; i < Matrix4<T>::DIM_SQUARED; i++) {
		left.m[i] *= right;
	}
	return left;
}
template <typename T>
inline std::ostream& operator << (std::ostream& os, const Matrix4<T>& mat) {
	os << mat.m[0] << ", " << mat.m[4] << ", " << mat.m[8]  << ", " << mat.m[12] << std::endl;
	os << mat.m[1] << ", " << mat.m[5] << ", " << mat.m[9]  << ", " << mat.m[13] << std::endl;
	os << mat.m[2] << ", " << mat.m[6] << ", " << mat.m[10] << ", " << mat.m[14] << std::endl;
	os << mat.m[3] << ", " << mat.m[7] << ", " << mat.m[11] << ", " << mat.m[15] << std::endl;
	return os;
}



template <typename T>
inline void Matrix4<T>::set(const T (&src)[16], const bool rowMajor) {
	if (!rowMajor) {
		std::memcpy(this->m, src, Matrix4<T>::MEM_LEN);
	} else {
		m[0] = src[0];		m[4] = src[1];		m[8]  = src[2];		m[12] = src[3];
		m[1] = src[4];		m[5] = src[5];		m[9]  = src[6];		m[13] = src[7];
		m[2] = src[8];		m[6] = src[9];		m[10] = src[10];	m[14] = src[11];
		m[3] = src[12];		m[7] = src[13];		m[11] = src[14];	m[15] = src[15];
	}
}

template <typename T>
inline void Matrix4<T>::set(T m0, T m1, T m2, T m3, T m4, T m5, T m6, T m7, T m8, T m9, T m10, T m11, T m12, T m13, T m14, T m15) {
	m[0] = m0;		m[4] = m1;		m[8]  = m2;		m[12] = m3;
	m[1] = m4;		m[5] = m5;		m[9]  = m6;		m[13] = m7;
	m[2] = m8;		m[6] = m9;		m[10] = m10;	m[14] = m11;
	m[3] = m12;		m[7] = m13;		m[11] = m14;	m[15] = m15;
}

template <typename T>
inline void Matrix4<T>::setIdentity() {
	this->m[0] = 1;		this->m[4] = 0;		this->m[8]	= 0;	this->m[12] = 0;
	this->m[1] = 0;		this->m[5] = 1;		this->m[9]	= 0;	this->m[13] = 0;
	this->m[2] = 0;		this->m[6] = 0;		this->m[10] = 1;	this->m[14] = 0;
	this->m[3] = 0;		this->m[7] = 0;		this->m[11] = 0;	this->m[15] = 1;
}

template <typename T>
inline void Matrix4<T>::setZero() {
	for (int i=0; i < DIM_SQUARED; i++) {
		this->m[i] = 0;
	}
}

template <typename T>
inline T Matrix4<T>::determinant() const {

}