template <typename T>
inline Vector3<T>::Vector3():
x(0),
y(0),
z(0) {}

template <typename T>
inline Vector3<T>::Vector3(T X, T Y, T Z):
x(X),
y(Y),
z(Z) {}

template <typename T>
inline Vector3<T> operator + (const Vector3<T>& left, const Vector3<T>& right) {
	return Vector3<T>(left.x + right.x, left.y + right.y, left.z + right.z);
}

template <typename T>
inline Vector3<T> operator + (const Vector3<T>& left, const T& right) {
	return Vector3<T>(left.x + right, left.y + right, left.z + right);
}

template <typename T>
inline Vector3<T>& operator += (Vector3<T>& left, const Vector3<T>& right) {
	left.x += right.x;
	left.y += right.y;
	left.z += right.z;
	return left;
}

template <typename T>
inline Vector3<T>& operator += (Vector3<T>& left, const T& right) {
	left.x += right;
	left.y += right;
	left.z += right;
	return left;
}

template <typename T>
inline Vector3<T> operator - (const Vector3<T>& left, const Vector3<T>& right) {
	return Vector3<T>(left.x - right.x, left.y - right.y, left.z - right.z);
}

template <typename T>
inline Vector3<T> operator - (const Vector3<T>& left, const T& right) {
	return Vector3<T>(left.x - right, left.y - right, left.z - right);
}

template <typename T>
inline Vector3<T>& operator -= (Vector3<T>& left, const Vector3<T>& right) {
	left.x -= right.x;
	left.y -= right.y;
	left.z -= right.z;
	return left;
}

template <typename T>
inline Vector3<T>& operator -= (Vector3<T>& left, const T& right) {
	left.x -= right;
	left.y -= right;
	left.z -= right;
	return left;
}

template <typename T>
inline Vector3<T> operator * (const Vector3<T>& left, const Vector3<T>& right) {
	return Vector3<T>(left.x * right.x, left.y * right.y, left.z * right.z);
}

template <typename T>
inline Vector3<T> operator * (const Vector3<T>& left, const T& right) {
	return Vector3<T>(left.x * right, left.y * right, left.z * right);
}

template <typename T>
inline Vector3<T>& operator *= (Vector3<T>& left, const Vector3<T>& right) {
	left.x *= right.x;
	left.y *= right.y;
	left.z *= right.z;
	return left;
}

template <typename T>
inline Vector3<T>& operator *= (Vector3<T>& left, const T& right) {
	left.x *= right;
	left.y *= right;
	left.z *= right;
	return left;
}

template <typename T>
inline Vector3<T> operator / (const Vector3<T>& left, const Vector3<T>& right) {
	return Vector3<T>(
		(right.x == 0) ? left.x : left.x/right.x,
		(right.y == 0) ? left.y : left.y/right.y,
		(right.z == 0) ? left.z : left.z/right.z
	);
}

template <typename T>
inline Vector3<T> operator / (const Vector3<T>& left, const T& right) {
	return Vector3<T>(
		(right == 0) ? left.x : left.x/right,
		(right == 0) ? left.y : left.y/right,
		(right == 0) ? left.z : left.z/right
	);
}

template <typename T>
inline Vector3<T>& operator /= (Vector3<T>& left, const Vector3<T>& right) {
	left.x = (right.x == 0) ? left.x : left.x/right.x;
	left.y = (right.y == 0) ? left.y : left.y/right.y;
	left.z = (right.z == 0) ? left.z : left.z/right.z;
	return left;
}

template <typename T>
inline Vector3<T>& operator /= (Vector3<T>& left, const T& right) {
	left.x = (right.x == 0) ? left.x : left.x/right.x;
	left.y = (right.y == 0) ? left.y : left.y/right.y;
	left.z = (right.z == 0) ? left.z : left.z/right.z;
	return left;
}

template <typename T>
inline bool operator == (const Vector3<T>& left, const Vector3<T>& right) {
	return (left.x == right.x) && (left.y == right.y) && (left.z == right.z);
}

template <typename T>
inline bool operator != (const Vector3<T>& left, const Vector3<T>& right) {
	return (left.x != right.x) || (left.y != right.y) || (left.z != right.z);
}

template <typename T>
inline std::ostream& operator << (std::ostream& os, const Vector3<T>& vec) {
	os << vec.x << ", " << vec.y << ", " << vec.z;
	return os;
}

template <typename T>
inline T Vector3<T>::length() const {
	return sqrt(x*x + y*y + z*z);
}

template <typename T>
inline void Vector3<T>::normalize() {
	float l = this->length();
	this->x /= l;
	this->y /= l;
	this->z /= l;
}

template <typename T>
inline Vector3<T> Vector3<T>::getNormalized() const {
	Vector3<T> result(this->x,this->y,this->z);
	result.normalize();
	return result;
}

template <typename T>
inline bool Vector3<T>::match(const Vector3<T> &target) const {
	return (
		lab::math::almostEqualRelativeAndAbs(this->x, target.x, 0.0000000001f, 0.0000000001f) &&
		lab::math::almostEqualRelativeAndAbs(this->y, target.y, 0.0000000001f, 0.0000000001f) &&
		lab::math::almostEqualRelativeAndAbs(this->z, target.z, 0.0000000001f, 0.0000000001f)
	);
}

template <typename T>
inline T Vector3<T>::dot(const Vector3<T> &vec) const {
	return T(this->x*vec.x + this->y*vec.y + this->z*vec.z);
}

template <typename T>
inline Vector3<T> Vector3<T>::cross(const Vector3<T> &vec) const {
	return Vector3<T>(
		this->y*vec.z - vec.y*this->z,
		this->z*vec.x - vec.z*this->x,
		this->x*vec.y - vec.x*this->y
	);
}

template <typename T>
inline void Vector3<T>::lerp(const Vector3<T>& target, const float& delta) {
	this->x += (delta * (target.x - this->x));
	this->y += (delta * (target.y - this->y));
	this->z += (delta * (target.z - this->z));
}

template <typename T>
inline void Vector3<T>::copyTo(Vector3<T>& target) {
	target.x = this->x;
	target.y = this->y;
	target.z = this->z;
}