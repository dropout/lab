#ifndef LAB_MATH_VECTORUTILS_HPP
#define LAB_MATH_VECTORUTILS_HPP

#include <struktur/math/Vector2.hpp>
#include <struktur/math/Vector3.hpp>
#include <struktur/math/Vector4.hpp>

namespace lab {	namespace math { namespace util {
			
template <typename T> T distance(const Vector2<T>& a, const Vector2<T>& b);
template <typename T> T distance(const Vector3<T>& a, const Vector3<T>& b);
template <typename T> T distance(const Vector4<T>& a, const Vector4<T>& b);

template <typename T> T distanceSquared(const Vector2<T>& a, const Vector2<T>& b);
template <typename T> T distanceSquared(const Vector3<T>& a, const Vector3<T>& b);
template <typename T> T distanceSquared(const Vector4<T>& a, const Vector4<T>& b);

template <typename T> bool manhattanDistance(const Vector2<T>& a, const Vector2<T>& b, const T& distance);
template <typename T> bool manhattanDistance(const Vector3<T>& a, const Vector3<T>& b, const T& distance);
template <typename T> bool manhattanDistance(const Vector4<T>& a, const Vector4<T>& b, const T& distance);

/*

TODO: Implement these methods

template <typename T> float angleBetweenRad(const Vector2<T>& a, const Vector2<T>& b);
template <typename T> float angleBetweenRad(const Vector3<T>& a, const Vector3<T>& b);
template <typename T> float angleBetweenRad(const Vector4<T>& a, const Vector4<T>& b);

template <typename T> float angleBetweenDeg(const Vector2<T>& a, const Vector2<T>& b);
template <typename T> float angleBetweenDeg(const Vector3<T>& a, const Vector3<T>& b);
template <typename T> float angleBetweenDeg(const Vector4<T>& a, const Vector4<T>& b);

*/

#include <lab/math/VectorUtils.inl>

}}}

#endif // LAB_MATH_VECTORUTILS_HPP