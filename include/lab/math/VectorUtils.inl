template <typename T>
inline T distance(const Vector2<T>& a, const Vector2<T>& b) {
	T dx, dy;
	dx = b.x - a.x;
	dy = b.y - a.y;
	return T(sqrt(dx*dx + dy*dy));
}

template <typename T>
inline T distance(const Vector3<T>& a, const Vector3<T>& b) {
	T dx, dy, dz;
	dx = b.x - a.x;
	dy = b.y - a.y;
	dz = b.z - a.z;
	return T(sqrt(dx*dx + dy*dy + dz*dz));
}

template <typename T>
inline T distance(const Vector4<T>& a, const Vector4<T>& b) {
	T dx, dy, dz;
	dx = b.x - a.x;
	dy = b.y - a.y;
	dz = b.z - a.z;
	return T(sqrt(dx*dx + dy*dy + dz*dz));
}

template <typename T>
inline T distanceSquared(const Vector2<T>& a, const Vector2<T>& b) {
	T dx, dy;
	dx = b.x - a.x;
	dy = b.y - a.y;
	return (dx*dx + dy*dy);
}

template <typename T>
inline T distanceSquared(const Vector3<T>& a, const Vector3<T>& b) {
	T dx, dy, dz;
	dx = b.x - a.x;
	dy = b.y - a.y;
	dz = b.z - a.z;
	return (dx*dx + dy*dy + dz*dz);
}

template <typename T>
inline T distanceSquared(const Vector4<T>& a, const Vector4<T>& b) {
	T dx, dy, dz;
	dx = b.x - a.x;
	dy = b.y - a.y;
	dz = b.z - a.z;
	return (dx*dx + dy*dy + dz*dz);
}

template <typename T>
inline bool manhattanDistance(const Vector2<T>& a, const Vector2<T>& b, const T& distance) {
	float dx = abs(b.x - a.x);
	if (dx > distance) return false;
	float dy = abs(b.y - a.y);
	if (dy > distance) return false;
	return true;
}

template <typename T>
inline bool manhattanDistance(const Vector3<T>& a, const Vector3<T>& b, const T& distance) {
	float dx = abs(b.x - a.x);
	if (dx > distance) return false;
	float dy = abs(b.y - a.y);
	if (dy > distance) return false;
	float dz = abs(b.z - a.z);
	if (dz > distance) return false;
	return true;
}

template <typename T>
inline bool manhattanDistance(const Vector4<T>& a, const Vector4<T>& b, const T& distance) {
	float dx = abs(b.x - a.x);
	if (dx > distance) return false;
	float dy = abs(b.y - a.y);
	if (dy > distance) return false;
	float dz = abs(b.z - a.z);
	if (dz > distance) return false;
	return true;
}