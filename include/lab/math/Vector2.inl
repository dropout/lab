template <typename T>
inline Vector2<T>::Vector2():
x(0),
y(0) {}

template <typename T>
inline Vector2<T>::Vector2(T X, T Y):
x(X),
y(Y) {}

template <typename T>
inline Vector2<T> operator + (const Vector2<T>& left, const Vector2<T>& right) {
	return Vector2<T>(left.x + right.x, left.y + right.y);
}

template <typename T>
inline Vector2<T> operator + (const Vector2<T>& left, const T& right) {
	return Vector2<T>(left.x + right, left.y + right);
}

template <typename T>
inline Vector2<T>& operator += (Vector2<T>& left, const Vector2<T>& right) {
	left.x += right.x;
	left.y += right.y;
	return left;
}

template <typename T>
inline Vector2<T>& operator += (Vector2<T>& left, const T& right) {
	left.x += right;
	left.y += right;
	return left;
}

template <typename T>
inline Vector2<T> operator - (const Vector2<T>& left, const Vector2<T>& right) {
	return Vector2<T>(left.x - right.x, left.y - right.y);
}

template <typename T>
inline Vector2<T> operator - (const Vector2<T>& left, const T& right) {
	return Vector2<T>(left.x - right, left.y - right);
}

template <typename T>
inline Vector2<T>& operator -= (Vector2<T>& left, const Vector2<T>& right) {
	left.x -= right.x;
	left.y -= right.y;
	return left;
}

template <typename T>
inline Vector2<T>& operator -= (Vector2<T>& left, const T& right) {
	left.x -= right;
	left.y -= right;
	return left;
}

template <typename T>
inline Vector2<T> operator * (const Vector2<T>& left, const Vector2<T>& right) {
	return Vector2<T>(left.x * right.x, left.y * right.y);
}

template <typename T>
inline Vector2<T> operator * (const Vector2<T>& left, const T& right) {
	return Vector2<T>(left.x * right, left.y * right);
}

template <typename T>
inline Vector2<T>& operator *= (Vector2<T>& left, const Vector2<T>& right) {
	left.x *= right.x;
	left.y *= right.y;
	return left;
}

template <typename T>
inline Vector2<T>& operator *= (Vector2<T>& left, const T& right) {
	left.x *= right;
	left.y *= right;
	return left;
}

template <typename T>
inline Vector2<T> operator / (const Vector2<T>& left, const Vector2<T>& right) {
	return Vector2<T>(
		(right.x == 0) ? left.x : left.x/right.x,
		(right.y == 0) ? left.y : left.y/right.y
	);
}

template <typename T>
inline Vector2<T> operator / (const Vector2<T>& left, const T& right) {
	return Vector2<T>(
		(right == 0) ? left.x : left.x/right,
		(right == 0) ? left.y : left.y/right
	);
}

template <typename T>
inline Vector2<T>& operator /= (Vector2<T>& left, const Vector2<T>& right) {
	left.x = (right.x == 0) ? left.x : left.x/right.x;
	left.y = (right.y == 0) ? left.y : left.y/right.y;
	return left;
}

template <typename T>
inline Vector2<T>& operator /= (Vector2<T>& left, const T& right) {
	left.x = (right.x == 0) ? left.x : left.x/right.x;
	left.y = (right.y == 0) ? left.y : left.y/right.y;
	return left;
}

template <typename T>
inline bool operator == (const Vector2<T>& left, const Vector2<T>& right) {
	return (left.x == right.x) && (left.y == right.y);
}

template <typename T>
inline bool operator != (const Vector2<T>& left, const Vector2<T>& right) {
	return (left.x != right.x) || (left.y != right.y);
}

template <typename T>
inline std::ostream& operator << (std::ostream& os, const Vector2<T>& vec) {
	os << vec.x << ", " << vec.y;
	return os;
}

template <typename T>
inline T Vector2<T>::length() const {
	return sqrt(x*x + y*y);
}

template <typename T>
inline void Vector2<T>::normalize() {
	float l = this->length();
	this->x /= l;
	this->y /= l;
}

template <typename T>
inline Vector2<T> Vector2<T>::getNormalized() const {
	Vector2<T> result(this->x,this->y);
	result.normalize();
	return result;
}

template <typename T>
inline bool Vector2<T>::match(const Vector2<T>& target, const float& epsilon) const {
	return (
		lab::math::almostEqualRelativeAndAbs(this->x, target.x, epsilon, epsilon) &&
		lab::math::almostEqualRelativeAndAbs(this->y, target.y, epsilon, epsilon)
	);
}

template <typename T>
inline T Vector2<T>::dot(const Vector2<T> &vec) const {
	return T(this->x*vec.x + this->y*vec.y);
}

template <typename T>
inline void Vector2<T>::lerp(const Vector2<T> &target, const float &delta) {
	this->x += (delta * (target.x - this->x));
	this->y += (delta * (target.y - this->y));
}

template <typename T>
inline void Vector2<T>::copyTo(Vector2<T> &target) {
	target.x = this->x;
	target.y = this->y;
}