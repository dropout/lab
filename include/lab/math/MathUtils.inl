inline bool almostEqualUlps(const float a, const float b, int maxUlpsDiff) {
	Float_t uA(a);
	Float_t uB(b);
 
	// Different signs means they do not match.
	if (uA.Negative() != uB.Negative()) {
		// Check for equality to make sure +0==-0
		if (a == b)
			return true;
		return false;
	}
 
	// Find the difference in ULPs.
	int ulpsDiff = abs(uA.i - uB.i);
	if (ulpsDiff <= maxUlpsDiff)
		return true;
 
	return false;
};
		
inline bool almostEqualRelativeAndAbs(const float a, const float b, float maxDiff, float maxRelDiff) {
	// Check if the numbers are really close -- needed
	// when comparing numbers near zero.
	float diff = fabs(a - b);
	if (diff <= maxDiff) {
		return true;
	}
	float A = fabs(a);
	float B = fabs(b);
	float largest = (B > A) ? B : A; 
	if (diff <= largest * maxRelDiff) {
		return true;
	}
	return false;
}