#ifndef LAB_MATH_VECTOR2_HPP
#define LAB_MATH_VECTOR2_HPP

#include <iostream>
#include <lab/math/MathUtils.hpp>

namespace lab {	namespace math {

template <typename T>
class Vector2
{
	public:
		Vector2();
		Vector2(T X, T Y);

		T length() const;
		void normalize();
		Vector2<T> getNormalized() const;
		bool match(const Vector2<T>& target, const float& epsilon=0.0000001) const;
		T dot(const Vector2<T> &vec) const;
		void lerp(const Vector2<T>& target, const float& delta);
		void copyTo(Vector2<T>& target);

		T x, y;
};

template <typename T>
Vector2<T> operator + (const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T> operator + (const Vector2<T>& left, const T& right);

template <typename T>
Vector2<T>& operator += (Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T>& operator += (Vector2<T>& left, const T& right);

template <typename T>
Vector2<T> operator - (const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T> operator - (const Vector2<T>& left, const T& right);

template <typename T>
Vector2<T>& operator -= (Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T>& operator -= (Vector2<T>& left, const T& right);

template <typename T>
Vector2<T> operator * (const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T> operator * (const Vector2<T>& left, const T& right);

template <typename T>
Vector2<T>& operator *= (Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T>& operator *= (Vector2<T>& left, const T& right);

template <typename T>
Vector2<T> operator / (const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T> operator / (const Vector2<T>& left, const T& right);

template <typename T>
Vector2<T>& operator /= (Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T>& operator /= (Vector2<T>& left, const T& right);

template <typename T>
bool operator == (const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
bool operator != (const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
std::ostream& operator << (std::ostream& os, const Vector2<T>& vec);

#include <lab/math/Vector2.inl>

typedef Vector2<int> Vector2i;
typedef Vector2<float> Vector2f;
typedef Vector2<double> Vector2d;

}}

#endif // !LAB_MATH_VECTOR2_HPP