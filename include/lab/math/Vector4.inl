template <typename T>
inline Vector4<T>::Vector4():
x(0),
y(0),
z(0),
w(0) {}

template <typename T>
inline Vector4<T>::Vector4(const Vector3<T>& vec):
x(vec.x),
y(vec.y),
z(vec.z),
w(0) {}

template <typename T>
inline Vector4<T>::Vector4(T X, T Y, T Z):
x(X),
y(Y),
z(Z),
w(0) {}

template <typename T>
inline Vector4<T>::Vector4(T X, T Y, T Z, T W):
x(X),
y(Y),
z(Z),
w(W) {}

template <typename T>
inline Vector4<T> operator +(const Vector4<T>& left, const Vector4<T>& right) {
	return Vector4<T>(
		left.x + right.x, 
		left.y + right.y, 
		left.z + right.z,
		left.w + right.w
	);
}

template <typename T>
inline Vector4<T> operator + (const Vector4<T>& left, const T& right) {
	return Vector4<T>(
		left.x + right, 
		left.y + right, 
		left.z + right,
		left.w + right
	);
}

template <typename T>
inline Vector4<T>& operator += (Vector4<T>& left, const Vector4<T>& right) {
	left.x += right.x;
	left.y += right.y;
	left.z += right.z;
	left.w += right.w;
	return left;
}

template <typename T>
inline Vector4<T>& operator += (Vector4<T>& left, const T& right) {
	left.x += right;
	left.y += right;
	left.z += right;
	left.w += right;
	return left;
}

template <typename T>
inline Vector4<T> operator - (const Vector4<T>& left, const Vector4<T>& right) {
	return Vector4<T>(
		left.x - right.x, 
		left.y - right.y, 
		left.z - right.z, 
		left.w - right.w
	);
}

template <typename T>
inline Vector4<T> operator - (const Vector4<T>& left, const T& right) {
	return Vector4<T>(
		left.x - right, 
		left.y - right, 
		left.z - right,
		left.w - right
	);
}

template <typename T>
inline Vector4<T>& operator -= (Vector4<T>& left, const Vector4<T>& right) {
	left.x -= right.x;
	left.y -= right.y;
	left.z -= right.z;
	left.w -= right.w;
	return left;
}

template <typename T>
inline Vector4<T>& operator -= (Vector4<T>& left, const T& right) {
	left.x -= right;
	left.y -= right;
	left.z -= right;
	left.w -= right;
	return left;
}

template <typename T>
inline Vector4<T> operator * (const Vector4<T>& left, const Vector4<T>& right) {
	return Vector4<T>(
		left.x * right.x,
		left.y * right.y,
		left.z * right.z,
		left.w * right.w
	);
}

template <typename T>
inline Vector4<T> operator * (const Vector4<T>& left, const T& right) {
	return Vector4<T>(
		left.x * right, 
		left.y * right, 
		left.z * right,
		left.w * right
	);
}

template <typename T>
inline Vector4<T>& operator *= (Vector4<T>& left, const Vector4<T>& right) {
	left.x *= right.x;
	left.y *= right.y;
	left.z *= right.z;
	left.w *= right.w;
	return left;
}

template <typename T>
inline Vector4<T>& operator *= (Vector4<T>& left, const T& right) {
	left.x *= right;
	left.y *= right;
	left.z *= right;
	left.w *= right;
	return left;
}

template <typename T>
inline Vector4<T> operator / (const Vector4<T>& left, const Vector4<T>& right) {
	return Vector4<T>(
		(right.x == 0) ? left.x : left.x/right.x,
		(right.y == 0) ? left.y : left.y/right.y,
		(right.z == 0) ? left.z : left.z/right.z,
		(right.w == 0) ? left.w : left.w/right.w
	);
}

template <typename T>
inline Vector4<T> operator / (const Vector4<T>& left, const T& right) {
	return Vector4<T>(
		(right == 0) ? left.x : left.x/right,
		(right == 0) ? left.y : left.y/right,
		(right == 0) ? left.z : left.z/right,
		(right == 0) ? left.w : left.w/right
	);
}

template <typename T>
inline Vector4<T>& operator /= (Vector4<T>& left, const Vector4<T>& right) {
	left.x = (right.x == 0) ? left.x : left.x/right.x;
	left.y = (right.y == 0) ? left.y : left.y/right.y;
	left.z = (right.z == 0) ? left.z : left.z/right.z;
	left.w = (right.w == 0) ? left.w : left.w/right.w;
	return left;
}

template <typename T>
inline Vector4<T>& operator /= (Vector4<T>& left, const T& right) {
	left.x = (right.x == 0) ? left.x : left.x/right.x;
	left.y = (right.y == 0) ? left.y : left.y/right.y;
	left.z = (right.z == 0) ? left.z : left.z/right.z;
	left.w = (right.w == 0) ? left.w : left.w/right.w;
	return left;
}

template <typename T>
inline bool operator == (const Vector4<T>& left, const Vector4<T>& right) {
	return (left.x == right.x) && (left.y == right.y) && (left.z == right.z) && (left.w == right.w);
}

template <typename T>
inline bool operator != (const Vector4<T>& left, const Vector4<T>& right) {
	return (left.x != right.x) || (left.y != right.y) || (left.z != right.z) || (left.w != right.w);
}

template <typename T>
inline std::ostream& operator << (std::ostream& os, const Vector4<T>& vec) {
	os << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w;
	return os;
}

template <typename T>
inline T Vector4<T>::length() const {
	return sqrt(x*x + y*y + z*z + w*w);
}

template <typename T>
inline void Vector4<T>::normalize() {
	float l = this->length();
	this->x /= l;
	this->y /= l;
	this->z /= l;
	this->w /= l;
}

template <typename T>
inline Vector4<T> Vector4<T>::getNormalized() const {
	Vector4<T> result(this->x,this->y,this->z,this->w);
	result.normalize();
	return result;
}

template <typename T>
inline bool Vector4<T>::match(const Vector4<T> &target, const float& epsilon) const {	
	return (
		lab::math::almostEqualRelativeAndAbs(this->x, target.x, epsilon, epsilon) &&
		lab::math::almostEqualRelativeAndAbs(this->y, target.y, epsilon, epsilon) &&
		lab::math::almostEqualRelativeAndAbs(this->z, target.z, epsilon, epsilon) &&
		lab::math::almostEqualRelativeAndAbs(this->w, target.w, epsilon, epsilon)
	);
}

template <typename T>
inline bool Vector4<T>::matchXYZ(const Vector4<T> &target, const float& epsilon) const {
	return (
		lab::math::almostEqualRelativeAndAbs(this->x, target.x, epsilon, epsilon) &&
		lab::math::almostEqualRelativeAndAbs(this->y, target.y, epsilon, epsilon) &&
		lab::math::almostEqualRelativeAndAbs(this->z, target.z, epsilon, epsilon)
	);
}

template <typename T>
inline T Vector4<T>::dot(const Vector4<T> &vec) const {
	return T(this->x*vec.x + this->y*vec.y + this->z*vec.z + this->w*vec.w);
}

template <typename T>
inline Vector4<T> Vector4<T>::cross(const Vector4<T>& vec) const {
	return Vector4<T>(
		this->y*vec.z - vec.y*this->z,
		this->z*vec.x - vec.z*this->x,
		this->x*vec.y - vec.x*this->y,
		this->w
	);
}

template <typename T>
inline void Vector4<T>::lerp(const Vector4<T>& target, const float& delta) {
	this->x += (delta * (target.x - this->x));
	this->y += (delta * (target.y - this->y));
	this->z += (delta * (target.z - this->z));
	this->w += (delta * (target.w - this->w));
}

template <typename T>
inline void Vector4<T>::copyTo(Vector4<T>& target) {
	target.x = this->x;
	target.y = this->y;
	target.z = this->z;
	target.w = this->w;
}