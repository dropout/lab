#ifndef LAB_MATH_MATRIX4_H
#define LAB_MATH_MATRIX4_H

#include <cstring>
#include <lab/math/Vector4.hpp>

namespace lab {	namespace math {

template <typename T>
class Matrix4 {

	/*
		This matrix is column major.

		m00	m04	m08 m12
		m01	m05	m09 m13
		m02	m06	m10 m14
		m03	m07	m11 m15
	*/

	public:

		static const int DIM = 4;
		static const int DIM_SQUARED = 16;
		static const size_t MEM_LEN	= sizeof(T) * 16;

		Matrix4();
		Matrix4(const T& num);
		Matrix4(const Matrix4<T>& mat);
		Matrix4(const T (&src)[16], const bool rowMajor=false);
		Matrix4(T m0, T m1, T m2, T m3, T m4, T m5, T m6, T m7, T m8, T m9, T m10, T m11, T m12, T m13, T m14, T m15);

		void set(const T (&src)[16], const bool rowMajor=false);
		void set(T m0, T m1, T m2, T m3, T m4, T m5, T m6, T m7, T m8, T m9, T m10, T m11, T m12, T m13, T m14, T m15);

		void setRow(const Vector4<T> row, const int numRow);
		Vector4<T> getRow(const int numRow) const;

		void setColumn(const Vector4<T> row, const int numColumn);
		Vector4<T> getColumn(const int numRow) const;

		void setIdentity();
		void setZero();
		T determinant() const;
		void transpose();
		Matrix4<T> getTransposed() const;
		void invert();
		Matrix4<T> getInverted() const;

		T m[16];

		T& operator [] (const int& num);
};

template <typename T>
Matrix4<T> operator + (const Matrix4<T>& left, const Matrix4<T>& right);

template <typename T>
Matrix4<T>& operator += (Matrix4<T>& left, const Matrix4<T>& right);

template <typename T>
Matrix4<T> operator - (const Matrix4<T>& left, const Matrix4<T>& right);

template <typename T>
Matrix4<T>& operator -= (Matrix4<T>& left, const Matrix4<T>& right);

template <typename T>
Matrix4<T> operator * (const Matrix4<T>& l, const Matrix4<T>& r);

template <typename T>
Matrix4<T>& operator *= (Matrix4<T>& l, const Matrix4<T>& r);

template <typename T>
Matrix4<T> operator * (const Matrix4<T>& left, const T& right);

template <typename T>
Matrix4<T> operator *= (Matrix4<T>& left, const T& right);

template <typename T>
std::ostream& operator << (std::ostream& os, const Matrix4<T>& mat);

#include <lab/math/Matrix4.inl>

typedef Matrix4<float> Matrix4f;
typedef Matrix4<double> Matrix4d;

}}

#endif // LAB_MATH_MATRIX4_H
