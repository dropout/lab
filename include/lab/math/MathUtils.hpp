#pragma once
#ifndef STRUKTUR_MATH_UTILS_H
#define STRUKTUR_MATH_UTILS_H

#include <stdint.h>
#include <math.h>

namespace lab {	namespace math {

static const float DEG_TO_RAD = 0.017453292f;
static const float RAD_TO_DEG = 57.29577951f;

/*
	Float comparison code is borrowed from Bruce Dawson.
	I saw it on his blog: http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
*/
union Float_t
{
	Float_t(float num = 0.0f) : f(num) {}
	// Portable extraction of components.
	bool Negative() const { return (i >> 31) != 0; }
	int32_t RawMantissa() const { return i & ((1 << 23) - 1); }
	int32_t RawExponent() const { return (i >> 23) & 0xFF; }

	int32_t i;
	float f;
	#ifdef _DEBUG
		struct
		{   // Bitfields for exploration. Do not use in production code.
			uint32_t mantissa : 23;
			uint32_t exponent : 8;
			uint32_t sign : 1;
		} parts;
	#endif
};

bool almostEqualUlps(const float a, const float b, int maxUlpsDiff);		
bool almostEqualRelativeAndAbs(const float a, const float b, float maxDiff, float maxRelDiff);			

#include <lab/math/MathUtils.inl>

}}

#endif // !STRUKTUR_MATH_UTILS_H