#ifndef LAB_MATH_VECTOR4_HPP
#define LAB_MATH_VECTOR4_HPP

#include <iostream>
#include <lab/math/MathUtils.hpp>
#include <lab/math/Vector3.hpp>

namespace lab { namespace math {

template <typename T>
class Vector4
{
	public:
		Vector4();
		Vector4(T X, T Y, T Z);
		Vector4(T X, T Y, T Z, T W);
		Vector4(const Vector3<T>& vec);

		T length() const;
		void normalize();
		Vector4<T> getNormalized() const;				
		bool match(const Vector4<T> &target, const float& epsilon=0.0000001) const;
		bool matchXYZ(const Vector4<T>& target, const float& epsilon=0.0000001) const;
		T dot(const Vector4<T> &vec) const;
		Vector4<T> cross(const Vector4<T>& vec) const;
		void lerp(const Vector4<T>& target, const float& delta);
		void copyTo(Vector4<T>& target);

		T x, y, z, w;
};

template <typename T>
Vector4<T> operator + (const Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T> operator + (const Vector4<T>& left, const T& right);

template <typename T>
Vector4<T>& operator += (Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T>& operator += (Vector4<T>& left, const T& right);

template <typename T>
Vector4<T> operator - (const Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T> operator - (const Vector4<T>& left, const T& right);

template <typename T>
Vector4<T>& operator -= (Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T>& operator -= (Vector4<T>& left, const T& right);

template <typename T>
Vector4<T> operator * (const Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T> operator * (const Vector4<T>& left, const T& right);

template <typename T>
Vector4<T>& operator *= (Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T>& operator *=(Vector4<T>& left, const T& right);

template <typename T>
Vector4<T> operator / (const Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T> operator / (const Vector4<T>& left, const T& right);

template <typename T>
Vector4<T>& operator /= (Vector4<T>& left, const Vector4<T>& right);

template <typename T>
Vector4<T>& operator /= (Vector4<T>& left, const T& right);

template <typename T>
bool operator == (const Vector4<T>& left, const Vector4<T>& right);

template <typename T>
bool operator != (const Vector4<T>& left, const Vector4<T>& right);

template <typename T>
std::ostream& operator << (std::ostream& os, const Vector4<T>& vec);

#include <lab/math/Vector4.inl>

typedef Vector4<int> Vector4i;
typedef Vector4<float> Vector4f;
typedef Vector4<double> Vector4d;

}}

#endif // LAB_MATH_VECTOR4_HPP