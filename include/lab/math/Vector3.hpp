#ifndef LAB_MATH_VECTOR3_HPP
#define LAB_MATH_VECTOR3_HPP

#include <iostream>
#include <lab/math/MathUtils.hpp>
#include <lab/math/Vector2.hpp>

namespace lab { namespace math {

template <typename T>
class Vector3
{
	public:
		Vector3();
		Vector3(T X, T Y, T Z);
		Vector3(const Vector2<T>& vec);

		T length() const;
		void normalize();
		Vector3<T> getNormalized() const;
		bool match(const Vector3<T>& target) const;
		T dot(const Vector3<T> &vec) const;
		Vector3<T> cross(const Vector3<T>& vec) const;
		void lerp(const Vector3<T> &target, const float& delta);
		void copyTo(Vector3<T>& target);

		T x, y, z;
};

template <typename T>
Vector3<T> operator + (const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T> operator + (const Vector3<T>& left, const T& right);

template <typename T>
Vector3<T>& operator += (Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T>& operator += (Vector3<T>& left, const T& right);

template <typename T>
Vector3<T> operator - (const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T> operator - (const Vector3<T>& left, const T& right);

template <typename T>
Vector3<T>& operator -= (Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T>& operator -= (Vector3<T>& left, const T& right);

template <typename T>
Vector3<T> operator * (const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T> operator * (const Vector3<T>& left, const T& right);

template <typename T>
Vector3<T>& operator *= (Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T>& operator *= (Vector3<T>& left, const T& right);

template <typename T>
Vector3<T> operator / (const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T> operator / (const Vector3<T>& left, const T& right);

template <typename T>
Vector3<T>& operator /= (Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T>& operator /= (Vector3<T>& left, const T& right);

template <typename T>
bool operator == (const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
bool operator != (const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
std::ostream& operator << (std::ostream& os, const Vector3<T>& vec);

#include <lab/math/Vector3.inl>

typedef Vector3<int> Vector3i;
typedef Vector3<float> Vector3f;
typedef Vector3<double> Vector3d;

}}

#endif // !LAB_MATH_VECTOR3_HPP