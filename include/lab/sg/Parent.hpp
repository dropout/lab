#include <bits/shared_ptr.h>

#ifndef LAB_SG_PARENT_HPP
#define LAB_SG_PARENT_HPP

namespace lab { namespace sg {

template <typename Child>
class Parent {

    public:
        Parent ();
        virtual ~Parent () {}

        const Child* getChild (size_t child_number) const {
            return m_children.at(child_number).get();
        }

        Child* getChild (size_t child_number) {
            return m_children.at(child_number).get();
        }

        size_t getNumberOfChildren() const {
            return this->m_children.size();
        }

        void addChild (std::unique_ptr<Child> child) {
            children_.emplace_back(std::move(child));
        }

    private:
        std::vector<std::unique_ptr<Child>> m_children;



};

}}

#endif // LAB_SG_PARENT_HPP