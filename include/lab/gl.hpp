#ifndef LAB_GL_HPP
#define LAB_GL_HPP

#include "lab/gl/Context.hpp"
#include "lab/gl/Buffer.hpp"
#include "lab/gl/Shader.hpp"
#include "lab/gl/Program.hpp"
#include "lab/gl/VertexArray.hpp"

#endif // LAB_GL_HPP