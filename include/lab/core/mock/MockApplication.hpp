#ifndef LAB_CORE_MOCKAPPLICATION_HPP
#define LAB_CORE_MOCKAPPLICATION_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <GL/glew.h>
#include <lab/core/Application.hpp>

using ::testing::AtLeast;
using ::testing::InSequence;
using ::testing::DefaultValue;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::Gt;
using ::testing::_;

namespace lab { namespace core {

class MockApplication : public lab::core::Application {

    public:
        using lab::core::Application::Application;

        MOCK_METHOD0(setup, void());
        MOCK_METHOD1(update, void(const double &time));
        MOCK_METHOD0(render, void());
        MOCK_METHOD0(teardown, void());

        MOCK_METHOD4(onKeyboardInput, void(
            lab::core::enums::KeyboardButton keyboardButton, lab::core::enums::ButtonActionType
            buttonActionType,
                    int
            modKeysBitField,
                    int
            scanCode
        ));

        MOCK_METHOD3(onMouseButtonInput, void(
                lab::core::enums::MouseButton
                mouseButton,
                        lab::core::enums::ButtonActionType
                buttonActionType,
                        int
                modKeysBitField
        ));

        MOCK_METHOD2(onMouseMoveInput, void(double
                xPos, double
                yPos));

        MOCK_METHOD2(onMouseScrollInput, void(double
                xOffset, double
                yOffset));

};

}}

#endif // LAB_CORE_MOCKAPPLICATION_HPP