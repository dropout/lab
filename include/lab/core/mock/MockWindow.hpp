#ifndef LAB_CORE_MOCKWINDOW_HPP
#define LAB_CORE_MOCKWINDOW_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <GL/glew.h>
#include <lab/core/Window.hpp>

using ::testing::AtLeast;
using ::testing::InSequence;
using ::testing::DefaultValue;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::Gt;
using ::testing::_;

namespace lab { namespace core {

class MockWindow : public lab::core::Window {

    public:
        MOCK_CONST_METHOD0(getWidth, int());
        MOCK_CONST_METHOD0(getHeight, int());
        MOCK_CONST_METHOD0(getTime, double());
        MOCK_CONST_METHOD0(getWindowCloseFlag, bool());
        MOCK_METHOD0(pollEvents, void());
        MOCK_METHOD0(swapBuffers, void());

};

}}

#endif // LAB_CORE_MOCKWINDOW_HPP