inline static auto KeyboardButtonCallback (GLFWwindow *win, int keyCode, int scanCode, int actionType, int modKeysBitfield) -> void {
    GLFWWindow *wm = static_cast<GLFWWindow*>(glfwGetWindowUserPointer(win));
    wm->keyboardButtonSignal.Emit(
            static_cast<lab::core::enums::KeyboardButton>(keyCode),
            static_cast<lab::core::enums::ButtonActionType>(actionType),
            modKeysBitfield,
            scanCode
    );
}

inline static auto MouseButtonCallback (GLFWwindow *win, int buttonCode, int actionType, int modKeysBitfield) -> void {
    GLFWWindow *wm = static_cast<GLFWWindow*>(glfwGetWindowUserPointer(win));
    wm->mouseButtonSignal.Emit(
            static_cast<lab::core::enums::MouseButton>(buttonCode),
            static_cast<lab::core::enums::ButtonActionType>(actionType),
            modKeysBitfield
    );
}

inline static auto MouseMoveCallback (GLFWwindow *win, double xPos, double yPos) -> void {
    GLFWWindow *wm = static_cast<GLFWWindow*>(glfwGetWindowUserPointer(win));
    wm->mouseMoveSignal.Emit(xPos, yPos);
}

inline static auto MouseScrollCallback (GLFWwindow *win, double xOffset, double yOffset) -> void {
    GLFWWindow *wm = static_cast<GLFWWindow*>(glfwGetWindowUserPointer(win));
    wm->mouseScrollSignal.Emit(xOffset,	yOffset);
}

inline static auto ErrorCallback (int error, const char* description) -> void {
    LOG_ERROR("GLFW Error occured: %s", description);
}
