#ifndef LAB_CORE_GLFWWINDOW_HPP
#define LAB_CORE_GLFWWINDOW_HPP

#include <memory>
#include <string>
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "lab/core/Window.hpp"
#include "lab/core/WindowException.hpp"
#include "macrologger.h"

namespace lab { namespace core {

class GLFWWindow : public Window {

    private:
        // Note the stinky GLFWWindow GLFWwindow difference
        class GLFWwindowDeleter {
            public:
                void operator()(GLFWwindow* w) {
                    glfwTerminate();
                    glfwDestroyWindow(w);
                }
        };

    public:
        GLFWWindow ();
        GLFWWindow (
            const int& width,
            const int& height,
            const std::string& title,
            const int& refreshRate,
            const int& multiSampling,
            const int& oglMajor,
            const int& oglMinor
        );
        virtual ~GLFWWindow () {}

    int getWidth () const;
        int getHeight () const;
        double getTime () const;
        bool getWindowCloseFlag () const;
        void pollEvents ();
        void swapBuffers ();
        void setFullScreen (const bool& isFullScreen);

    protected:
        std::unique_ptr<GLFWwindow, GLFWwindowDeleter> m_windowPtr;

        void init ();
        void create (
            const int& width,
            const int& height,
            const std::string& title,
            const int& refreshRate,
            const int& multiSampling,
            const int& oglMajor,
            const int& oglMinor
        );

        // static inline methods
        #include <lab/core/impl/GLFWWindow.inl>

};

}}

#endif // LAB_CORE_GLFWWINDOW_HPP
