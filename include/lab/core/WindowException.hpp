#ifndef LAB_CORE_WINDOWEXCEPTION_HPP
#define LAB_CORE_WINDOWEXCEPTION_HPP

#include <exception>
#include <iostream>

namespace lab { namespace core {

class WindowException : public std::exception {

	public:
        WindowException (const std::string& path) : m_path(path) {}

		virtual const char* what () const throw () {
			return m_path.c_str();
		}

	protected:
		std::string m_path;

};

}}

#endif // LAB_CORE_WINDOWEXCEPTION_HPP