#ifndef LAB_CORE_MAPSETTINGSSTORE_HPP
#define LAB_CORE_MAPSETTINGSSTORE_HPP

#include <map>
#include <string>
#include <vector>

namespace lab { namespace core {

typedef std::map<std::string, std::string> SettingsMap;

class SettingsStore {

    public:
        SettingsStore () {};
        SettingsStore (const SettingsMap& values) : m_values(values) {}

        void load (const SettingsMap& values);

        int getInt (const std::string& key);
        float getFloat (const std::string& key);
        double getDouble (const std::string& key);
        std::string getString (const std::string& key);
        bool getBool (const std::string& key);

        void set (const std::string& key, const int& val);
        void set (const std::string& key, const float& val);
        void set (const std::string& key, const double& val);
        void set (const std::string& key, const char* val);
        void set (const std::string& key, const std::string& val);
        void set (const std::string& key, const bool& val);

        unsigned int size () const;
        std::vector<std::string> keys ();

    private:
        SettingsMap m_values;
        std::string safeGet (const std::string& key);

};

}}

#endif // LAB_CORE_MAPSETTINGSSTORE_HPP
