#ifndef LAB_CORE_ARGUMENTS_HPP
#define LAB_CORE_ARGUMENTS_HPP

namespace lab { namespace core {

class Arguments {

	public:
		Arguments () {}
		Arguments(int argc, char** argv) : m_count(argc), m_values(argv) {}
		virtual ~Arguments () = default;

		int getCount () { return this->m_count; }
		void setCount (int value) { this->m_count = value; }
		
		char** getValues () { return this->m_values; }
		void setValues (char** value) { this->m_values = value; }

	protected:
		int			m_count;
		char**		m_values;

};

}}

#endif // LAB_CORE_ARGUMENTS_HPP
