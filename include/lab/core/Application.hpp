#ifndef LAB_CORE_APPLICATION_HPP
#define LAB_CORE_APPLICATION_HPP

#include <memory>
#include "lab/core/SettingsStore.hpp"
#include "lab/core/Window.hpp"

namespace lab { namespace core {

class Application {

	public:
        Application () = delete;
        Application (std::unique_ptr<Window> window);
        virtual ~Application () {};

		int run ();

    protected:
        SettingsStore m_settings;
        std::unique_ptr<Window> m_window;

		virtual void setup () {}
		virtual void update (const double& time) {}
		virtual void render () {}
        virtual void teardown () {}

        virtual void onKeyboardInput (
			lab::core::enums::KeyboardButton keyboardButton,
			lab::core::enums::ButtonActionType buttonActionType,
			int modKeysBitField,
			int scanCode
        ) {}

        virtual void onMouseButtonInput (
			lab::core::enums::MouseButton mouseButton,
			lab::core::enums::ButtonActionType buttonActionType,
			int modKeysBitField
		) {}

        virtual void onMouseMoveInput (double xPos, double yPos) {}
        virtual void onMouseScrollInput (double xOffset, double yOffset) {}

};

}}

#endif // LAB_CORE_APPLICATION_HPP
