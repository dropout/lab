#ifndef LAB_EVENT_ENUMS_MOUSEBUTTON_HPP
#define LAB_EVENT_ENUMS_MOUSEBUTTON_HPP

namespace lab { namespace core { namespace enums {

enum class MouseButton {
	Left = 0,
	Right = 1,
	Middle = 2	
};

}}}

#endif // LAB_EVENT_ENUMS_MOUSEBUTTON_HPP