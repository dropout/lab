#ifndef LAB_EVENT_ENUMS_KEYBOARDBUTTON_HPP
#define LAB_EVENT_ENUMS_KEYBOARDBUTTON_HPP

namespace lab { namespace core { namespace enums {

enum class KeyboardButton {

	// Regular keys
	Up = 256,	
	Right = 262,
	Left = 263,
	Down = 264,
	Escape = 265,
	PageUp = 266,
	PageDown = 267,
	Home = 268,
	End = 269,
	F1 = 290,
	F2 = 291,
	F3 = 292,
	F4 = 293,
	F5 = 294,
	F6 = 295,
	F7 = 296,
	F8 = 297,
	F9 = 298,
	F10 = 299,
	F11 = 300,
	F12 = 301,	

	// Mod keys	
	Shift = 0x0001,
	Control = 0x002,
	Alt = 0x0004,
	Super = 0x0008

};

}}}

#endif // LAB_EVENT_ENUMS_KEYBOARDBUTTON_HPP