#ifndef LAB_EVENT_ENUMS_BUTTONACTIONTYPE_HPP
#define LAB_EVENT_ENUMS_BUTTONACTIONTYPE_HPP

namespace lab { namespace core { namespace enums {

enum class ButtonActionType {
	Release = 0,
	Press = 1,
	Repeat = 2
};

}}}

#endif // LAB_EVENT_ENUMS_BUTTONACTIONTYPE_HPP