//#pragma GCC diagnostic ignored "-Wno-unused-local-typedefs"

#ifndef LAB_CORE_WINDOW_HPP
#define LAB_CORE_WINDOW_HPP

#include <gallant/Signal.h>
#include <lab/core/enums/ButtonActionType.hpp>
#include <lab/core/enums/KeyboardButton.hpp>
#include <lab/core/enums/MouseButton.hpp>

namespace lab { namespace core {

typedef Gallant::Signal4<lab::core::enums::KeyboardButton, lab::core::enums::ButtonActionType, int, int> KeyboardButtonSignal;
typedef Gallant::Signal3<lab::core::enums::MouseButton, lab::core::enums::ButtonActionType, int> MouseButtonSignal;
typedef Gallant::Signal2<double, double> MouseMoveSignal;
typedef Gallant::Signal2<double, double> MouseScrollSignal;

class Window {

	public:
        Window () {};
        virtual ~Window () {};

        virtual int getWidth () const = 0;
        virtual int getHeight () const = 0;
        virtual double getTime () const = 0;
        virtual void swapBuffers () = 0;
        virtual void pollEvents () = 0;
        virtual bool getWindowCloseFlag () const = 0;

        KeyboardButtonSignal keyboardButtonSignal;
        MouseButtonSignal mouseButtonSignal;
        MouseMoveSignal mouseMoveSignal;
        MouseScrollSignal mouseScrollSignal;

};

}}

#endif // LAB_CORE_WINDOW_HPP
