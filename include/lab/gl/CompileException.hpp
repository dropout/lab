#ifndef LAB_GL_SHADEREXCEPTION_HPP
#define LAB_GL_SHADEREXCEPTION_HPP

#include <exception>
#include <lab/gl/GlException.hpp>

namespace lab { namespace gl {

class CompileException : public GlException {
    using GlException::GlException;

};

}}

#endif // LAB_GL_SHADEREXCEPTION_HPP
