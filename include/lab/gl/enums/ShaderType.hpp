#ifndef LAB_GL_ENUMS_SHADERTYPE_HPP
#define LAB_GL_ENUMS_SHADERTYPE_HPP

#include <GL/glew.h>

namespace lab { namespace gl {

namespace ShaderType {
    enum T {
        VertexShader = GL_VERTEX_SHADER,
        FragmentShader = GL_FRAGMENT_SHADER
    };
}

}}

#endif // LAB_GL_ENUMS_SHADERTYPE_HPP
