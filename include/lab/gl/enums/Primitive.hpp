#ifndef LAB_GL_ENUMS_PRIMITIVE_HPP
#define LAB_GL_ENUMS_PRIMITIVE_HPP

#include <GL/glew.h>

namespace lab { namespace gl {

namespace Primitive {
    enum T {
        Triangles = GL_TRIANGLES,
        Lines = GL_LINES,
        Points = GL_POINTS
    };
}

}}

#endif // LAB_GL_ENUMS_PRIMITIVE_HPP
