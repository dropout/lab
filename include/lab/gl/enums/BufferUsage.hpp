#ifndef LAB_GL_ENUMS_BUFFERUSAGE_HPP
#define LAB_GL_ENUMS_BUFFERUSAGE_HPP

#include <GL/glew.h>

namespace lab { namespace gl {

namespace BufferUsage {
    enum T {
        StreamDraw = GL_STREAM_DRAW,
        StreamRead = GL_STREAM_READ,
        StreamCopy = GL_STREAM_COPY,
        StaticDraw = GL_STATIC_DRAW,
        StaticRead = GL_STATIC_READ,
        StaticCopy = GL_STATIC_COPY,
        DynamicDraw = GL_DYNAMIC_DRAW,
        DynamicRead = GL_DYNAMIC_READ,
        DynamicCopy = GL_DYNAMIC_COPY
    };
}

}}

#endif // LAB_GL_ENUMS_BUFFERUSAGE_HPP
