#ifndef LAB_GL_ENUMS_CAPABILITIES_HPP
#define LAB_GL_ENUMS_CAPABILITIES_HPP

#include <lab/gl/OpenGL.hpp>

namespace lab { namespace gl {

namespace Capability {
    enum T {
        DepthTest = api::DEPTH_TEST,
        StencilTest = api::STENCIL_TEST,
        CullFace = api::CULL_FACE,
        RasterizerDiscard = api::RASTERIZER_DISCARD
    };
}

}}

#endif // LAB_GL_ENUMS_CAPABILITIES_HPP
