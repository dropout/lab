#ifndef LAB_GL_ENUMS_TYPE_HPP
#define LAB_GL_ENUMS_TYPE_HPP

#include <GL/glew.h>

namespace lab { namespace gl {

namespace Type {
    enum T {
        Byte = GL_BYTE,
        UnsignedByte = GL_UNSIGNED_BYTE,
        Short = GL_SHORT,
        UnsignedShort = GL_UNSIGNED_SHORT,
        Int = GL_INT,
        UnsignedInt = GL_UNSIGNED_INT,
        Float = GL_FLOAT,
        Double = GL_DOUBLE
    };
}

}}

#endif // LAB_GL_ENUMS_TYPE_HPP
