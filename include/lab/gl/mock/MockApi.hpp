#ifndef LAB_GL_MOCK_MOCKAPI_HPP
#define LAB_GL_MOCK_MOCKAPI_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <GL/glew.h>

using ::testing::AtLeast;
using ::testing::InSequence;
using ::testing::DefaultValue;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::Gt;
using ::testing::_;

namespace lab { namespace gl {

class MockApi {

    public:

        MOCK_CONST_METHOD2(_AttachShader, void(const GLint& programId, const GLint& shaderId));
        MOCK_CONST_METHOD2(_BindBuffer, void(const GLenum& target, const GLuint& buffer));
        MOCK_CONST_METHOD4(_BufferData, void(const GLenum& target, const GLsizeiptr& size, const GLvoid* data, const GLenum& usage));
        MOCK_CONST_METHOD4(_BufferSubData, void(const GLenum& target, const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data));
        MOCK_CONST_METHOD1(_CreateShader, GLuint(const GLenum& shaderType));
        MOCK_CONST_METHOD0(_CreateProgram, GLuint());
        MOCK_CONST_METHOD1(_CompileShader, void(const GLuint& shader));
        MOCK_CONST_METHOD2(_GenBuffers, void(const GLuint& count, GLuint* names));
        MOCK_CONST_METHOD4(_GetBufferSubData, void(const GLenum& target, const GLintptr& offset, const GLsizeiptr& size, GLvoid* data));
        MOCK_CONST_METHOD3(_GetProgramiv, void(GLuint program, GLenum pname, GLint* params));
        MOCK_CONST_METHOD4(_GetProgramInfoLog, void(GLuint programId, GLsizei maxLength, GLsizei* infoLogLength, GLchar* infoLog));
        MOCK_CONST_METHOD3(_GetShaderiv, void(GLuint shader, GLenum pname, GLint* params));
        MOCK_CONST_METHOD4(_GetShaderInfoLog, void(GLuint shaderId, GLsizei maxLength, GLsizei* infoLogLength, GLchar* infoLog));
        MOCK_CONST_METHOD2(_DeleteBuffers, void(const GLuint& count, GLuint* names));
        MOCK_CONST_METHOD1(_DeleteProgram, void(const GLuint& programId));
        MOCK_CONST_METHOD1(_DeleteShader, void(const GLuint& shaderId));
        MOCK_CONST_METHOD4(_ShaderSource, void(const GLuint& shaderId, const GLsizei& count, GLchar const **string, const GLint* length));
        MOCK_CONST_METHOD1(_LinkProgram, void(const GLint& programId));
        MOCK_CONST_METHOD4(_NamedBufferData, void(const GLuint& buffer, const GLsizei& size, const void* data, const GLenum& usage));

        MOCK_CONST_METHOD4(_NamedBufferSubData, void());
        MOCK_CONST_METHOD4(_GetNamedBufferSubData, void());

        void reset () {
            ::testing::Mock::VerifyAndClear(this);
        }

        static MockApi& GetInstance () {
            static MockApi mockApi;
            return mockApi;
        }

        static inline void AttachShader (const GLint& shaderId, const GLint& programId) {
            MockApi::GetInstance()._AttachShader(shaderId, programId);
        }

        static inline void BindBuffer (const GLenum& target, const GLuint& buffer) {
            MockApi::GetInstance()._BindBuffer(target, buffer);
        }

        static inline void BufferData (const GLenum& target, const GLsizeiptr& size, const GLvoid* data, const GLenum& usage) {
            MockApi::GetInstance()._BufferData(target, size, data, usage);
        }

        static inline void BufferSubData (const GLenum& target, const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data) {
            MockApi::GetInstance()._BufferSubData(target, offset, size, data);
        }

        static inline GLuint CreateProgram () {
            return MockApi::GetInstance()._CreateProgram();
        }

        static inline GLuint CreateShader (const GLenum& shaderType) {
            return MockApi::GetInstance()._CreateShader(shaderType);
        }

        static inline void CompileShader (const GLuint& shader) {
            MockApi::GetInstance()._CompileShader(shader);
        }

        static inline void GenBuffers (const GLuint& count, GLuint* names) {
            MockApi::GetInstance()._GenBuffers(count, names);
        }

        static inline void GetBufferSubData (const GLenum target, const GLintptr& offset, const GLsizeiptr& size, GLvoid* data) {
            MockApi::GetInstance()._GetBufferSubData(target, offset, size, data);
        }

        static inline void GetProgramiv (GLuint shader, GLenum pname, GLint* params) {
            MockApi::GetInstance()._GetProgramiv(shader, pname, params);
        }

        static inline void GetShaderiv (GLuint shader, GLenum pname, GLint* params) {
            MockApi::GetInstance()._GetShaderiv(shader, pname, params);
        }

        static inline void DeleteBuffer (GLuint* name) {
            MockApi::GetInstance()._DeleteBuffers(1, name);
        }

        static inline void DeleteBuffers (const GLuint& count, GLuint* names) {
            MockApi::GetInstance()._DeleteBuffers(count, names);
        }

        static inline void DeleteProgram (const GLuint& programId) {
            MockApi::GetInstance()._DeleteProgram(programId);
        }

        static inline void DeleteShader (const GLuint& shader) {
            MockApi::GetInstance()._DeleteShader(shader);
        }

        static inline void GetShaderInfoLog (GLuint shaderId, GLsizei maxLength, GLsizei* infoLogLength, GLchar* infoLog) {
            MockApi::GetInstance()._GetShaderInfoLog(shaderId, maxLength, infoLogLength, infoLog);
        }

        static inline void GetProgramInfoLog (GLuint programId, GLsizei maxLength, GLsizei* infoLogLength, GLchar* infoLog) {
            MockApi::GetInstance()._GetProgramInfoLog(programId, maxLength, infoLogLength, infoLog);
        }

        static inline void LinkProgram (const GLint& programId) {
            MockApi::GetInstance()._LinkProgram(programId);
        }

        static inline void NamedBufferData (const GLuint& buffer, const GLsizei& size, const void *data, const GLenum& usage) {
            MockApi::GetInstance()._NamedBufferData(buffer, size, data, usage);
        }

        static inline void NamedBufferSubData(const GLuint buffer, const GLintptr offset, const GLsizei size, const void *data) {
            MockApi::GetInstance()._NamedBufferSubData(buffer, offset, size, data);
        }

        static inline void GetNamedBufferSubData (const GLuint buffer, GLintptr offset, GLsizei size, void *data () {
            MockApi::GetInstance()._GetNamedBufferSubData(buffer, offset, size, data);
        }

        static inline void ShaderSource (const GLuint& shader, const GLsizei& count, GLchar const **string, const GLint* length) {
            MockApi::GetInstance()._ShaderSource(shader, count, string, length);
        }

};

}}

#endif // LAB_GL_MOCK_MOCKAPI_HPP
