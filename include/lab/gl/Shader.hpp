#ifndef LAB_GL_SHADER_HPP
#define LAB_GL_SHADER_HPP

#include <memory>
#include <lab/gl/Api.hpp>
#include <lab/gl/Object.hpp>
#include <lab/gl/CompileException.hpp>
#include <lab/gl/enums/ShaderType.hpp>
#include <lab/gl/traits/FragmentShaderTraits.hpp>
#include <lab/gl/traits/VertexShaderTraits.hpp>

namespace lab { namespace gl {

namespace detail {

template <typename GLAPI = lab::gl::Api>
class Shader {

    public:
        Shader () {}
        virtual ~Shader () {}

    protected:
        void compileShader (const GLuint& shaderId, const std::string& shaderSource);
        std::string getInfoLog (const GLuint& shaderId) const;

};

#include <lab/gl/Shader.inl>

} // eof 'detail' namespace

template <typename GLAPI = lab::gl::Api>
class VertexShaderObject : public detail::Shader<GLAPI>, public Object<VertexShaderTraits<GLAPI>> {

    public:
        VertexShaderObject () : Object<VertexShaderTraits<GLAPI>>() {}
        VertexShaderObject (const std::string& shaderSource) : Object<VertexShaderTraits<GLAPI>>() {
            this->compileShader(this->getName(), shaderSource);
        }

        virtual ~VertexShaderObject () {}

        void compile (const std::string& shaderSource) {
            this->compileShader(this->getName(), shaderSource);
        };

};

typedef VertexShaderObject<lab::gl::Api> VertexShader;

template <typename GLAPI = lab::gl::Api>
class FragmentShaderObject : public detail::Shader<GLAPI>, public Object<FragmentShaderTraits<GLAPI>> {

public:
    FragmentShaderObject () {}

    FragmentShaderObject (const std::string& shaderSource) {
        this->compileShader(this->getName(), shaderSource);
    }

    void compile (const std::string& shaderSource) {
        this->compileShader(this->getName(), shaderSource);
    };

};

typedef FragmentShaderObject<lab::gl::Api> FragmentShader;

}}

#endif // LAB_GL_SHADER_HPP
