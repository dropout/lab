#ifndef LAB_GL_OBJECT_HPP
#define LAB_GL_OBJECT_HPP

#include <GL/glew.h>
#include <macrologger.h>
#include <assert.h>

namespace lab { namespace gl {

template <typename T>
class Object {

    public:
        Object () : m_name(0) {
            this->m_name = T::Allocate();
        }
        Object (const Object& other) = delete; // no copy
        Object& operator = (const Object& other) = delete; // no assign

        Object (Object&& rhs) : m_name(0) {
            *this = std::move(rhs);
        }

        Object& operator = (Object&& rhs) {
            if (this != &rhs) {
                std::swap(this->m_name, rhs.m_name);
            }
            return *this;
        }

        ~Object () {
            if (this->m_name > 0) {
                T::Release(this->m_name);
            }
        }

        GLuint getName () const {
            assert(this->m_name > 0);
            return this->m_name;
        }

    private:
        GLuint m_name;

};

}}

#endif // LAB_GL_OBJECT_HPP
