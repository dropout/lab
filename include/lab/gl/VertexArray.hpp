#ifndef LAB_GL_VERTEXARRAY_HPP
#define LAB_GL_VERTEXARRAY_HPP

#include <lab/gl/Object.hpp>
#include <lab/gl/traits/VertexArrayTraits.hpp>
#include <lab/gl/enums/Type.hpp>

namespace lab { namespace gl {

template <typename GLAPI = lab::gl::Api>
class VertexArrayObject : public lab::gl::Object<VertexArrayTraits<GLAPI>> {

    public:
        VertexArrayObject ();
        virtual ~VertexArrayObject () {};

        void bind();
        void enableAttribute (
            const GLint& attrLoc,
            const GLuint& itemCount,
            lab::gl::Type::T type,
            GLuint count,
            GLuint stride,
            std::intptr_t offset
        );

};

#include <lab/gl/VertexArray.inl>

typedef VertexArrayObject<lab::gl::Api> VertexArray;

}}

#endif // LAB_GL_VERTEXARRAY_HPP
