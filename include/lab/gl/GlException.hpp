#ifndef LAB_GL_GLEXCEPTION_HPP
#define LAB_GL_GLEXCEPTION_HPP

#include <exception>
#include <iostream>

namespace lab { namespace gl {

    class GlException : public std::runtime_error {
        public:
            using std::runtime_error::runtime_error;
    };

}}

#endif // LAB_GL_GLEXCEPTION_HPP
