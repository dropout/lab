#ifndef LAB_GL_BUFFER_HPP
#define LAB_GL_BUFFER_HPP

#include <memory>
#include <lab/gl/Api.hpp>
#include <lab/gl/Object.hpp>
#include <lab/gl/enums/BufferUsage.hpp>
#include <lab/gl/traits/BufferTraits.hpp>

namespace lab { namespace gl {

template <typename GLAPI = lab::gl::Api>
class BufferObject : public Object<BufferTraits<GLAPI>> {

    public:
        BufferObject ();
        BufferObject (const std::size_t& size, const void* data, const BufferUsage::T& bufferUsage);
        BufferObject (BufferObject&& rhs) : Object<BufferTraits<GLAPI>>(std::move(rhs)) {}
        BufferObject& operator = (BufferObject&& rhs);
        virtual ~BufferObject () {}

        void bind ();
        inline void setData (const std::size_t& size, const void* data, BufferUsage::T bufferUsage);
        void setSubData (const std::size_t& offset, const std::size_t& size, const void* data);
        void getSubData (const std::size_t& offset, const std::size_t& size, void* data);

};

#include <lab/gl/Buffer.inl>

typedef BufferObject<lab::gl::Api> Buffer;

}}

#endif // LAB_GL_BUFFER_HPP
