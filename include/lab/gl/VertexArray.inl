template <typename GLAPI>
VertexArrayObject<GLAPI>::VertexArrayObject () : Object<VertexArrayTraits<GLAPI>> () {}

template <typename GLAPI>
void VertexArrayObject<GLAPI>::bind () {
    GLAPI::BindVertexArray(this->getName());
}

template <typename GLAPI>
void VertexArrayObject<GLAPI>::enableAttribute (
    const GLint& attrLoc,
    const GLuint& itemCount,
    lab::gl::Type::T type,
    GLuint count,
    GLuint stride,
    std::intptr_t offset
) {
    GLAPI::EnableVertexArrayAttrib(this->getName(), attrLoc);
    GLAPI::VertexAttribPointer(
        attrLoc,            // Pointer location
        2,                  // Items in array
        GL_FLOAT,           // Type of array
        GL_FALSE,           // Normalize flag
        2*sizeof(GLfloat),  // stride
        0                   // Array buffer offset
    );
}

/*
glEnableVertexArrayAttrib(...);
glVertexArrayVertexBuffer(...);
glVertexArrayAttribFormat(...);
glVertexArrayAttribBinding(...);
*/

/*
void glVertexArrayAttribBinding(    GLuint vaobj,
    GLuint attribindex,
    GLuint bindingindex);

    */

