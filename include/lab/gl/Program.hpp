#ifndef LAB_GL_PROGRAM_HPP
#define LAB_GL_PROGRAM_HPP

#include <map>
#include <vector>
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "lab/gl/Object.hpp"
#include "lab/gl/Shader.hpp"
#include "lab/gl/traits/ProgramTraits.hpp"
#include "lab/gl/LinkException.hpp"
#include "macrologger.h"

namespace lab { namespace gl {

template <typename GLAPI = lab::gl::Api>
class ProgramObject : public Object<ProgramTraits<GLAPI>> {

    typedef std::map<std::string, GLuint> LocationMap;

    public:
        ProgramObject () {}
        ProgramObject (const VertexShaderObject<GLAPI>& vertexShader, const FragmentShaderObject<GLAPI>& fragmentShader);
        virtual ~ProgramObject (void) {}

        void link (const VertexShaderObject<GLAPI>& vertexShader, const FragmentShaderObject<GLAPI>& fragmentShader);
        void use ();

        bool isLinked () const;
        std::string getInfoLog () const;

        GLint getAttributeLocation(const std::string& name);
        GLint getUniformLocation(const std::string& name);

        void bindFragDataLocation(const std::string& name, const GLuint colorNumber=0);

        void uniform (const std::string& name, const int& value);
        void uniform (const std::string& name, const int& value, const int& count);
        void uniform (const std::string& name, const float& value);
        void uniform (const std::string& name, const float& value, const int& count);

        /*
        void uniform (const std::string& name, const Vec2& value);
        void uniform (const std::string& name, const Vec3& value);
        void uniform (const std::string& name, const Vec4& value);
        void uniform (const std::string& name, const float* values, uint count);
        void uniform (const std::string& name, const Vec2* values, uint count);
        void uniform (const std::string& name, const Vec3* values, uint count);
        void uniform (const std::string& name, const Vec4* values, uint count);
        void uniform (const std::string& name, const Mat3& value);
        void uniform (const std::string& name, const Mat4& value);
        */

    protected:
        bool m_isLinked;
        LocationMap m_uniformLocations;
        LocationMap m_attributeLocations;


        std::string getInfoLog (const GLint& programId) const;

};

#include <lab/gl/Program.inl>

typedef ProgramObject<lab::gl::Api> Program;

}}

#endif // LAB_GL_PROGRAM_HPP
