#ifndef LAB_GL_ATTRIBUTE_HPP
#define LAB_GL_ATTRIBUTE_HPP

namespace lab { namespace gl {

	class Attribute {

		public:
			Attribute ();
			virtual ~Attribute ();

			void enable ();
			void disable ();

		private:
			Buffer m_buffer;
			VertexArray m_vao;
			AttributeFormat m_format;

	};


}}


#endif // LAB_GL_ATTRIBUTE_HPP
