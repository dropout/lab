#ifndef LAB_GL_LINKEXCEPTION_HPP
#define LAB_GL_LINKEXCEPTION_HPP

#include <exception>
#include <lab/gl/GlException.hpp>

namespace lab { namespace gl {

class LinkException : public GlException {
    using GlException::GlException;

};

}}

#endif // LAB_GL_LINKEXCEPTION_HPP
