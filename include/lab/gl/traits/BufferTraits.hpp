#ifndef LAB_GL_TRAITS_BUFFERTRAITS_HPP
#define LAB_GL_TRAITS_BUFFERTRAITS_HPP

namespace lab { namespace gl {

template <typename GLAPI>
class BufferTraits {

    public:

        inline static GLuint Allocate () {
            return GLAPI::GenBuffer();
        }

        inline static void Release (GLuint value) {
            GLAPI::DeleteBuffer(&value);
        }

};

}}

#endif // LAB_GL_TRAITS_BUFFERTRAITS_HPP
