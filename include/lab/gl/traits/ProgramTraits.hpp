#ifndef LAB_GL_TRAITS_PROGRAMTRAITS_HPP
#define LAB_GL_TRAITS_PROGRAMTRAITS_HPP

namespace lab { namespace gl {

template <typename GLAPI>
class ProgramTraits {

    public:

        typedef GLuint value_type;

        inline static value_type Allocate () {
            GLuint programId = GLAPI::CreateProgram();
            if (programId == 0) {
                throw GlException("Unable to allocate resource for program");
            } else {
                return programId;
            }
        }

        inline static void Release (value_type value) {
            GLAPI::DeleteProgram(value);
        }

};

}}

#endif // LAB_GL_TRAITS_PROGRAMTRAITS_HPP
