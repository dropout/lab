#ifndef LAB_GL_TRAITS_VERTEXSHADERTRAITS_HPP
#define LAB_GL_TRAITS_VERTEXSHADERTRAITS_HPP

#include <lab/gl/GlException.hpp>
#include <lab/gl/enums/ShaderType.hpp>
#include <macrologger.h>

namespace lab { namespace gl {

template <typename GLAPI>
class VertexShaderTraits {

    public:

        typedef GLuint value_type;

        inline static value_type Allocate () {
            GLuint shaderId = GLAPI::CreateShader(ShaderType::VertexShader);
            if (shaderId == 0) {
                throw GlException("Unable to allocate resource for shader");
            } else {
                return shaderId;
            }
        }

        inline static void Release (value_type value) {
            GLAPI::DeleteShader(value);
        }

    };

}}

#endif // LAB_GL_TRAITS_VERTEXSHADERTRAITS_HPP
