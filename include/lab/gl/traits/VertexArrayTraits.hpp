#ifndef LAB_GL_TRAITS_VERTEXARRAYTRAITS
#define LAB_GL_TRAITS_VERTEXARRAYTRAITS

#include <GL/glew.h>

namespace lab { namespace gl {

template <typename GLAPI>
class VertexArrayTraits {

    public:
        typedef GLuint value_type;

        inline static value_type Allocate () {
            GLuint name;
            GLAPI::GenVertexArrays(1, &name);
            return name;
        }

        inline static void Release (value_type value) {
            GLAPI::DeleteVertexArray(&value);
        }

};

}}

#endif // LAB_GL_TRAITS_VERTEXARRAYTRAITS

