#ifndef LAB_GL_API_HPP
#define LAB_GL_API_HPP

#include <GL/glew.h>

namespace lab { namespace gl {

class Api {

    public:

        static inline void AttachShader (const GLuint& programId, const GLuint& shaderId) {
            glAttachShader(programId, shaderId);
        };

        static inline void BindBuffer (const GLenum& target, const GLuint& buffer) {
            glBindBuffer(target, buffer);
        }

        static inline void BindVertexArray (const GLuint& name) {
            glBindVertexArray(name);
        }

        static inline void BufferData (const GLenum& target, const GLsizeiptr& size, const GLvoid* data, const GLenum& usage) {
            glBufferData(target, size, data, usage);
        }

        static inline void BufferSubData (const GLenum& target, const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data) {
            glBufferSubData(target, offset, size, data);
        }

        static inline void Clear (const GLbitfield& mask) {
            glClear(mask);
        }

        static inline void ClearColor (const GLfloat& red, const GLfloat& green, const GLfloat& blue, const GLfloat& alpha) {
            glClearColor(red, green, blue, alpha);
        }

        static inline GLuint CreateProgram () {
            return glCreateProgram();
        }

        static inline GLuint CreateShader (const GLenum& shaderType) {
            return glCreateShader(shaderType);
        }

        static inline void CompileShader (const GLuint& shaderId) {
            glCompileShader(shaderId);
        }

        static inline void DeleteBuffer (GLuint* name) {
            glDeleteBuffers(1, name);
        }

        static inline void DeleteBuffers (const GLuint& count, GLuint* names) {
            glDeleteBuffers(count, names);
        }

        static inline void DeleteProgram (const GLuint& programId) {
            glDeleteProgram(programId);
        }

        static inline void DeleteShader (const GLuint& shader) {
            glDeleteShader(shader);
        }

        static inline void DeleteVertexArray (GLuint* name) {
            glDeleteVertexArrays(1, name);
        }

        static inline void DrawArrays (const GLenum& mode, const GLint& first, const GLsizei& count) {
            glDrawArrays(mode, first, count);
        }

        static inline void EnableVertexAttribArray (const GLuint& index) {
            glEnableVertexAttribArray(index);
        }

        static inline GLuint GenBuffer () {
            GLuint name;
            glGenBuffers(1, &name);
            return name;
        }

        static inline void GenBuffers (const GLuint& count, GLuint* names) {
            glGenBuffers(count, names);
        }

        static inline GLuint GenVertexArray () {
            GLuint name;
            glGenVertexArrays(1, &name);
            return name;
        }

        static inline void GenVertexArrays (const GLsizei count, GLuint* arrays) {
            glGenVertexArrays(count, arrays);
        }

        static inline GLint GetAttribLocation (GLuint program, const GLchar* name) {
            return glGetAttribLocation(program, name);
        }

        static inline void GetBufferSubData (const GLenum& target, const GLintptr& offset, const GLsizeiptr& size, GLvoid* data) {
            glGetBufferSubData(target, offset, size, data);
        }

        static inline void GetProgramiv (GLuint program, GLenum pname, GLint* params) {
            glGetShaderiv(program, pname, params);
        }

        static inline void GetShaderiv (GLuint shader, GLenum pname, GLint* params) {
            glGetShaderiv(shader, pname, params);
        }

        static inline void GetShaderInfoLog (GLuint shaderId, GLsizei maxLength, GLsizei* infoLogLength, GLchar* infoLog) {
            glGetShaderInfoLog(shaderId, maxLength, infoLogLength, infoLog);
        }

        static inline void GetProgramInfoLog (GLuint programId, GLsizei maxLength, GLsizei* infoLogLength, GLchar* infoLog) {
            glGetProgramInfoLog(programId, maxLength, infoLogLength, infoLog);
        }

        static inline void LinkProgram (const GLint& programId) {
            glLinkProgram(programId);
        }
    
        static inline void NamedBufferData (const GLuint& buffer, const GLsizei& size, const void *data, const GLenum& usage) {
            glNamedBufferDataEXT(buffer, size, data, usage);
        }

        static inline void UseProgram (const GLuint& name) {
            glUseProgram(name);
        }

        static inline void ShaderSource (const GLuint& shader, const GLsizei& count, GLchar const **string, const GLint *length) {
            glShaderSource(shader, count, string, length);
        }

        static inline void VertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean isNormalized, GLsizei stride, const GLvoid* pointer) {
            glVertexAttribPointer(index, size, type, isNormalized, stride, pointer);
        }

};

}}

#endif // LAB_GL_API_HPP
