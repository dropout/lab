#include <lab/gl/Program.hpp>

using namespace lab::gl;

template <typename GLAPI>
ProgramObject<GLAPI>::ProgramObject (const VertexShaderObject<GLAPI>& vertexShader, const FragmentShaderObject<GLAPI>& fragmentShader) : Object<ProgramTraits<GLAPI>>() {
    this->link(vertexShader, fragmentShader);
}

template <typename GLAPI>
void ProgramObject<GLAPI>::use () {
    GLAPI::UseProgram(this->getName());
}

template <typename GLAPI>
GLint ProgramObject<GLAPI>::getAttributeLocation (const std::string& name) {
    return GLAPI::GetAttribLocation(this->getName(), name.c_str());
}

template <typename GLAPI>
GLint ProgramObject<GLAPI>::getUniformLocation (const std::string& name) {
    LocationMap::const_iterator uniformIt = this->m_uniformLocations.find(name);
    if (uniformIt == m_uniformLocations.end()) {
        GLint loc = glGetUniformLocation(this->getName(), name.c_str());
        this->m_uniformLocations[name] = loc;
        return loc;
    } else {
        return uniformIt->second;
    }
}

template <typename GLAPI>
void ProgramObject<GLAPI>::uniform (const std::string &name, const int &value) {
    GLuint loc = this->getUniformLocation(name);
    GLAPI::Uniform1i(loc, value);
}

template <typename GLAPI>
void ProgramObject<GLAPI>::uniform (const std::string &name, const float &value) {
    GLuint loc = this->getUniformLocation(name);
    GLAPI::Uniform1f(loc, value);
}

template <typename GLAPI>
void ProgramObject<GLAPI>::link (const VertexShaderObject<GLAPI>& vertexShader, const FragmentShaderObject<GLAPI>& fragmentShader) {

    GLAPI::AttachShader(this->getName(), vertexShader.getName());
    GLAPI::AttachShader(this->getName(), fragmentShader.getName());
    GLAPI::LinkProgram(this->getName());

    GLint linkStatus;
    GLAPI::GetProgramiv(this->getName(), GL_LINK_STATUS, &linkStatus);

    if (linkStatus == GL_FALSE) {
        std::string errorMessage(this->getInfoLog(this->getName()));
        throw LinkException(errorMessage);
    }
}

template <typename GLAPI>
std::string ProgramObject<GLAPI>::getInfoLog (const GLint& programId) const {
    GLint infoLogLength;
    GLAPI::GetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0) {
        std::string infoLog(infoLogLength, 0);
        GLAPI::GetProgramInfoLog(programId, infoLogLength, &infoLogLength, &infoLog[0]);
        return infoLog;
    } else {
        GLenum glErr;
        glErr = glGetError();

        std::string errorMessage(reinterpret_cast<const char *>(glewGetErrorString(glErr)));
        // fprintf(errorMessage.c_str());

        LOG_DEBUG("Error occured %s", errorMessage.c_str());
        //std::out << errorMessage; 




        return "Error occured while linking shaders and the infolog is empty.";
    }
}
