template <typename GLAPI>
BufferObject<GLAPI>::BufferObject () : Object<BufferTraits<GLAPI>>() {}

template <typename GLAPI>
BufferObject<GLAPI>::BufferObject (const std::size_t& size, const void* data, const BufferUsage::T& bufferUsage) : Object<BufferTraits<GLAPI>>() {
    this->setData(size, data, bufferUsage);
}

template <typename GLAPI>
BufferObject<GLAPI>& BufferObject<GLAPI>::operator = (BufferObject<GLAPI>&& rhs) {
    // Call base class move assignment operator
    Object<BufferTraits<GLAPI>>::operator=(std::move(rhs));
    return *this;
}

template <typename GLAPI>
void BufferObject<GLAPI>::bind () {
    GLAPI::BindBuffer(GL_ARRAY_BUFFER, this->getName());
}

template <typename GLAPI>
void BufferObject<GLAPI>::setData (const std::size_t& size, const void* data, BufferUsage::T bufferUsage) {
    GLAPI::NamedBufferData(this->getName(), size, data, bufferUsage);
}

template <typename GLAPI>
void BufferObject<GLAPI>::setSubData (const std::size_t& offset, const std::size_t& size, const void* data) {
    GLAPI::NamedBufferSubData(this->getName(), offset, size, data);
}

template <typename GLAPI>
void BufferObject<GLAPI>::getSubData (const std::size_t& offset, const std::size_t& size, void* data) {
    GLAPI::GetNamedBufferSubData(this->getName(), offset, size, data);
}
