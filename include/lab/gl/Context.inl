template <typename GLAPI>
void Context<GLAPI>::clear () {
    GLAPI::ClearColor(0, 0, 0, 1);
    GLAPI::Clear(GL_COLOR_BUFFER_BIT);
}

template <typename GLAPI>
void Context<GLAPI>::drawArrays (const lab::gl::Primitive::T& mode, const GLint& first, const GLint& count) {
    GLAPI::DrawArrays(mode, first, count);
}