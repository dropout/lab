template <typename GLAPI>
void Shader<GLAPI>::compileShader (const GLuint& shaderId, const std::string& shaderSource) {

    // Compile shader code
    const char *c_str_source = shaderSource.c_str();
    GLAPI::ShaderSource(shaderId, 1, &c_str_source, nullptr);
    GLAPI::CompileShader(shaderId);

    // Get compile status
    GLint compileStatus;
    GLAPI::GetShaderiv(shaderId, GL_COMPILE_STATUS, &compileStatus);

    // Throw error if failed to compile
    if (compileStatus == GL_FALSE) {
        std::string infoLog(this->getInfoLog(shaderId));
        throw CompileException(infoLog);
    }

}

template <typename GLAPI>
std::string Shader<GLAPI>::getInfoLog (const GLuint& shaderId) const {

    // get length of log
    GLint infoLogLength;
    GLAPI::GetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);

    // write log into string
    if (infoLogLength > 0) {
        std::string infoLog(infoLogLength, 0);
        GLAPI::GetShaderInfoLog(shaderId, infoLogLength, &infoLogLength, &infoLog[0]);
        return infoLog;
    } else {
        return "";
    }

}
