#ifndef LAB_GL_CONTEXT_HPP
#define LAB_GL_CONTEXT_HPP

#include <lab/gl/Api.hpp>
#include <lab/gl/enums/Primitive.hpp>

namespace lab { namespace gl {

namespace detail {

template <typename GLAPI>
class Context {

public:
    Context () {}
    virtual ~Context () {}

    void clear ();
    void drawArrays (const lab::gl::Primitive::T& mode, const GLint& first, const GLint& count);
    //void drawElements (const lab::gl::Primitive::T& mode, const GLint& first, const GLint& count);

};

#include <lab/gl/Context.inl>

}

typedef detail::Context<lab::gl::Api> Context;

}}

#endif // LAB_GL_CONTEXT_HPP
