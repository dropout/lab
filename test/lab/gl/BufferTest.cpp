#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <lab/gl/mock/MockApi.hpp>
#include <lab/gl/Buffer.hpp>

using ::testing::InSequence;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::_;

TEST(lab_gl_BufferTest, Constructor_empty) {
    lab::gl::MockApi::GetInstance().reset();
    GLuint objectName = 1;
    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GenBuffers(objectName, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteBuffers(objectName, _)).Times(1);
    }

    lab::gl::BufferObject<lab::gl::MockApi> buffer;
    ASSERT_EQ(objectName, buffer.getName());
}


TEST(lab_gl_BufferTest, Constructor_full) {
    lab::gl::MockApi::GetInstance().reset();
    GLuint objectName = 1;
    const GLfloat data[6] = {
         1.0f,  0.0f,
         0.0f,  1.0f,
         0.0f,  0.0f
    };

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GenBuffers(objectName, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BindBuffer(GL_ARRAY_BUFFER, objectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BufferData(GL_ARRAY_BUFFER, sizeof(data), _, lab::gl::BufferUsage::T::StaticDraw)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteBuffers(objectName, _)).Times(1);
    }

    lab::gl::BufferObject<lab::gl::MockApi> buffer(sizeof(data), &data, lab::gl::BufferUsage::T::StaticDraw);
}

TEST(lab_gl_BufferTest, Constructor_move) {
    lab::gl::MockApi::GetInstance().reset();
    GLuint objectName = 1;
    const GLfloat data[6] = {
         1.0f,  0.0f,
         0.0f,  1.0f,
         0.0f,  0.0f
    };

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GenBuffers(1, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BindBuffer(GL_ARRAY_BUFFER, 1)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BufferData(GL_ARRAY_BUFFER, sizeof(data), _, lab::gl::BufferUsage::T::StaticDraw)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteBuffers(1, _)).Times(1);
    }

    lab::gl::BufferObject<lab::gl::MockApi> buffer(sizeof(data), &data, lab::gl::BufferUsage::T::StaticDraw);
    ASSERT_EQ(objectName, buffer.getName());
    lab::gl::BufferObject<lab::gl::MockApi> otherBuffer(std::move(buffer));
    ASSERT_EQ(objectName, otherBuffer.getName());
}

TEST(lab_gl_BufferTest, setData) {
    lab::gl::MockApi::GetInstance().reset();

    const GLfloat data[6] = {
         1.0f,  0.0f,
         0.0f,  1.0f,
         0.0f,  0.0f
    };
    GLuint objectName = 1;

    {
        InSequence s;        
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GenBuffers(objectName, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BindBuffer(GL_ARRAY_BUFFER, objectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BufferData(GL_ARRAY_BUFFER, sizeof(data), _, lab::gl::BufferUsage::T::StaticDraw)).Times(1);        
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteBuffers(objectName, _)).Times(1);
    }

    lab::gl::BufferObject<lab::gl::MockApi> buffer;
    buffer.setData(sizeof(data), &data, lab::gl::BufferUsage::StaticDraw);
}

TEST(lab_gl_BufferTest, setSubData) {
    lab::gl::MockApi::GetInstance().reset();

    const GLfloat data[6] = {
         1.0f,  0.0f,
         0.0f,  1.0f,
         0.0f,  0.0f
    };
    const GLfloat subData[2] = {
         0.5f,  0.5f
    };
    GLuint objectName = 1;

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GenBuffers(objectName, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BindBuffer(GL_ARRAY_BUFFER, objectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BufferData(GL_ARRAY_BUFFER, sizeof(data), _, lab::gl::BufferUsage::T::StaticDraw)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BufferSubData(objectName, sizeof(GLfloat)*2, sizeof(subData), _)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteBuffers(objectName, _)).Times(1);
    }

    lab::gl::BufferObject<lab::gl::MockApi> buffer(sizeof(data), &data, lab::gl::BufferUsage::T::StaticDraw);
    buffer.setSubData(sizeof(GLfloat)*2, sizeof(subData), &data);

}

TEST(lab_gl_BufferTest, getSubData) {
    lab::gl::MockApi::GetInstance().reset();

    const GLfloat data[6] = {
         1.0f,  0.0f,
         0.0f,  1.0f,
         0.0f,  0.0f
    };

    GLfloat subData[2];

    GLuint objectName = 1;

    EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GenBuffers(1, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
    EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BindBuffer(GL_ARRAY_BUFFER, 1)).Times(1);
    EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BufferData(GL_ARRAY_BUFFER, sizeof(data), _, lab::gl::BufferUsage::T::StaticDraw)).Times(1);
    EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GetBufferSubData(1, sizeof(GLfloat)*2, sizeof(GLfloat)*2, _)).Times(1);
    EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteBuffers(1, _)).Times(1);

    lab::gl::BufferObject<lab::gl::MockApi> buffer(sizeof(data), &data, lab::gl::BufferUsage::T::StaticDraw);
    buffer.getSubData(sizeof(GLfloat)*2, sizeof(GLfloat)*2, &subData);
}
