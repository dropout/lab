#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <lab/gl/mock/MockApi.hpp>
#include <lab/gl/Program.hpp>
#include <lab/gl/Shader.hpp>

using ::testing::InSequence;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::_;

TEST(lab_gl_ProgramTest, Constructor_empty) {
    lab::gl::MockApi::GetInstance().reset();

    GLuint objectName = 1;

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CreateProgram()).WillOnce(Return(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteProgram(objectName)).Times(1);
    }

    lab::gl::ProgramObject<lab::gl::MockApi> program;
    ASSERT_EQ(objectName, program.getName());
}

TEST(lab_gl_ProgramTest, link) {
    lab::gl::MockApi::GetInstance().reset();

    const char* vertexShaderSource = R"vs(
        #version 420
        in vec2 vertPosition;
        void main (void) {
            gl_Position = vec4(vertPosition, 0.0, 1.0);
        }
    )vs";

    const char* fragmentShaderSource = R"fs(
        #version 420
        out vec4 fragColor;
        void main (void) {
            fragColor = vec4(0.0, 0.82, 0.82, 1.0);
        }
    )fs";

    GLuint fragmentShaderObjectName = 1;
    GLuint vertexShaderObjectName = 2;
    GLuint programObjectName = 3;

    // Shader expectations
    {
        InSequence shaderSequence;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CreateShader(lab::gl::ShaderType::VertexShader)).WillOnce(Return(vertexShaderObjectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _ShaderSource(vertexShaderObjectName, 1, ::testing::Pointee(::testing::StrEq(vertexShaderSource)), nullptr)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CompileShader(vertexShaderObjectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GetShaderiv(vertexShaderObjectName, GL_COMPILE_STATUS, _)).WillOnce(::testing::SetArgPointee<2>(GL_TRUE));

        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CreateShader(lab::gl::ShaderType::FragmentShader)).WillOnce(Return(fragmentShaderObjectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _ShaderSource(fragmentShaderObjectName, 1, ::testing::Pointee(::testing::StrEq(fragmentShaderSource)), nullptr)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CompileShader(fragmentShaderObjectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GetShaderiv(fragmentShaderObjectName, GL_COMPILE_STATUS, _)).WillOnce(::testing::SetArgPointee<2>(GL_TRUE));

        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteShader(fragmentShaderObjectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteShader(vertexShaderObjectName)).Times(1);
    }

    // Program expectations
    {
        InSequence programSequence;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CreateProgram()).WillOnce(Return(programObjectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _AttachShader(programObjectName, vertexShaderObjectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _AttachShader(programObjectName, fragmentShaderObjectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _LinkProgram(programObjectName));

        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GetProgramiv(programObjectName, GL_LINK_STATUS, _)).WillOnce(::testing::SetArgPointee<2>(GL_TRUE));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteProgram(programObjectName));
    }

    lab::gl::VertexShaderObject<lab::gl::MockApi> vertexShader(vertexShaderSource);
    lab::gl::FragmentShaderObject<lab::gl::MockApi> fragmentShader(fragmentShaderSource);

    lab::gl::ProgramObject<lab::gl::MockApi> program;

    program.link(vertexShader, fragmentShader);

    ASSERT_EQ(programObjectName, program.getName());
}
