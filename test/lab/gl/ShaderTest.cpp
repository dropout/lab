#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <lab/gl/mock/MockApi.hpp>
#include <lab/gl/Shader.hpp>

using ::testing::InSequence;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::_;

TEST(lab_gl_VertexShaderTest, Constructor_empty) {
    lab::gl::MockApi::GetInstance().reset();

    GLuint objectName = 1;

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CreateShader(lab::gl::ShaderType::VertexShader)).WillOnce(Return(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteShader(objectName)).Times(1);
    }

    lab::gl::VertexShaderObject<lab::gl::MockApi> vertexShader;
    ASSERT_EQ(objectName, vertexShader.getName());
}

TEST(lab_gl_VertexShaderTest, Constructor_full) {
    lab::gl::MockApi::GetInstance().reset();

    GLuint objectName = 1;
    std::string source = R"vs(
        #version 420
        in vec2 vertPosition;
        void main (void) {
            gl_Position = vec4(vertPosition, 0.0, 1.0);`
        }
    )vs";

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CreateShader(lab::gl::ShaderType::VertexShader)).WillOnce(Return(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _ShaderSource(objectName, 1, ::testing::Pointee(::testing::StrEq(source)), nullptr)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CompileShader(objectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GetShaderiv(objectName, GL_COMPILE_STATUS, _)).WillOnce(::testing::SetArgPointee<2>(GL_TRUE));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteShader(objectName)).Times(1);
    }

    lab::gl::VertexShaderObject<lab::gl::MockApi> vertexShader(source);
    ASSERT_EQ(objectName, vertexShader.getName());
}

/*
TEST(lab_gl_VertexShaderTest, Constructor_source) {
    lab::gl::MockApi::GetInstance().reset();
    GLuint objectName = 1;

    const char* source = R"vs(
        #version 420
        in vec2 vertPosition;
        void main (void) {
            gl_Position = vec4(vertPosition, 0.0, 1.0);
        }
    )vs";

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CreateShader(objectName, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _ShaderSource(GL_ARRAY_BUFFER, objectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _CompileShader(objectName, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteShader(GL_ARRAY_BUFFER, sizeof(data), _, lab::gl::BufferUsage::T::StaticDraw)).Times(1);
    }

    lab::gl::VertexShader<lab::gl::MockApi> vertexShader(source);
    ASSERT_EQ(objectName, vertexShader.getName());
    ASSERT_EQ(lab::gl::ShaderType::VertexShader, vertexShader.getShaderType());
}

TEST(lab_gl_ShaderTest, Constructor_full) {
    lab::gl::MockApi::GetInstance().reset();
    GLuint objectName = 1;
    const GLfloat data[6] = {
        1.0f,  0.0f,
        0.0f,  1.0f,
        0.0f,  0.0f
    };

    {
        InSequence s;
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _GenBuffers(objectName, _)).WillOnce(::testing::SetArgPointee<1>(objectName));
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BindBuffer(GL_ARRAY_BUFFER, objectName)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _BufferData(GL_ARRAY_BUFFER, sizeof(data), _, lab::gl::BufferUsage::T::StaticDraw)).Times(1);
        EXPECT_CALL(lab::gl::MockApi::GetInstance(), _DeleteBuffers(objectName, _)).Times(1);
    }

    lab::gl::BufferObject<lab::gl::MockApi> buffer(sizeof(data), &data, lab::gl::BufferUsage::T::StaticDraw);
}
*/