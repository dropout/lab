#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include <memory>
#include <macrologger.h>
#include <lab/core/Window.hpp>
#include <lab/core/Application.hpp>
#include <lab/core/mock/MockWindow.hpp>
#include <lab/core/mock/MockApplication.hpp>

using ::testing::AtLeast;
using ::testing::InSequence;
using ::testing::DefaultValue;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::Gt;
using ::testing::_;

using namespace lab::core;

TEST(DesktopApplication, run) {
    typedef NiceMock<MockWindow> NiceWindowManagerMock;
    NiceWindowManagerMock* wmp = new NiceWindowManagerMock();

    EXPECT_CALL(*wmp, pollEvents()).Times(1);

    std::unique_ptr<lab::core::Window> wm(wmp);
    MockApplication app(std::move(wm));

    EXPECT_CALL(*wmp, getWindowCloseFlag())
        .WillOnce(Return(false))
        .WillOnce(Return(true));

    EXPECT_CALL(*wmp, getTime())
        .WillOnce(Return(0.001))
        .WillOnce(Return(0.002))
        .WillOnce(Return(0.003))
        .WillOnce(Return(0.004));

    {
        InSequence s;
        EXPECT_CALL(app, setup()).Times(1);
    }

    EXPECT_CALL(app, render()).Times(1);
    EXPECT_CALL(app, update(Gt(0))).Times(1);
    EXPECT_CALL(app, teardown()).Times(1);

    app.run();
}


TEST(DesktopApplication, inputHandlers) {
    typedef NiceMock<MockWindow> NiceWindowManagerMock;
    NiceWindowManagerMock *wmp = new NiceWindowManagerMock();

    std::unique_ptr<lab::core::Window> wm(wmp);
    MockApplication app(std::move(wm));

    EXPECT_CALL(*wmp, getWindowCloseFlag())
            .WillOnce(Return(false))
            .WillOnce(Return(true));

    EXPECT_CALL(*wmp, getTime())
            .WillOnce(Return(0.001))
            .WillOnce(Return(0.002))
            .WillOnce(Return(0.003))
            .WillOnce(Return(0.004));

    EXPECT_CALL(app, setup()).Times(1);
    EXPECT_CALL(app, render()).Times(1);
    EXPECT_CALL(app, update(Gt(0))).Times(1);
    EXPECT_CALL(app, teardown()).Times(1);
    app.run();

    EXPECT_CALL(app, onKeyboardInput(
            lab::core::enums::KeyboardButton::F1,
            lab::core::enums::ButtonActionType::Press,
            0x000001,
            1
    )).Times(1);

    EXPECT_CALL(app, onMouseButtonInput(
            lab::core::enums::MouseButton::Left,
            lab::core::enums::ButtonActionType::Press,
            0x000001
    )).Times(1);

    EXPECT_CALL(app, onMouseMoveInput(
            Gt(0.0f),
            Gt(0.0f)
    )).Times(1);

    EXPECT_CALL(app, onMouseScrollInput(
            Gt(0.0f),
            Gt(0.0f)
    )).Times(1);

    wmp->keyboardButtonSignal.Emit(
        lab::core::enums::KeyboardButton::F1,
        lab::core::enums::ButtonActionType::Press,
        0x000001,
        1
    );

    wmp->mouseButtonSignal.Emit(
        lab::core::enums::MouseButton::Left,
        lab::core::enums::ButtonActionType::Press,
        0x00001
    );

    wmp->mouseMoveSignal.Emit(1.5, 2.5);
    wmp->mouseScrollSignal.Emit(5.0, 1.0);
}
