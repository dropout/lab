//#pragma GCC diagnostic ignored "-Wunused-local-typedefs"

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include <string>
#include <gallant/Signal.h>

using ::testing::AtLeast;
using ::testing::InSequence;
using ::testing::DefaultValue;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::Gt;
using ::testing::_;

namespace lab { namespace signaltest {

class SignalSender {
public:
    Gallant::Signal0<void> signal1;
    Gallant::Signal1<int> signal2;
    Gallant::Signal2<int, float&> signal3;
    Gallant::Signal1<std::string> signal4;
    Gallant::Signal1<std::string> signal5;
};

class SignalReceiver {
public:
    SignalReceiver ():
            signal1Count(0),
            signal2Count(0),
            signal3Count(0) {}

    int signal1Count, signal2Count, signal3Count;
    static int signal4Count;

    void handleSignal1 () {
        this->signal1Count++;
    }

    void handleSignal2 (int i) {
        this->signal2Count++;
        ASSERT_EQ(3, i);
    }

    void handleSignal3 (int i, float& f) {
        this->signal3Count++;
        ASSERT_EQ(2, i);
        ASSERT_EQ(3.14f, f);
    }

    static void staticHandler (std::string value) {
        SignalReceiver::signal4Count++;
        ASSERT_EQ("Hello world", value);
    }

    virtual void specificHandler (std::string) {};

};

int SignalReceiver::signal4Count = 0;

class SpecificSignalReceiver : public SignalReceiver {
public:
    SpecificSignalReceiver ():
            specificSignalCount(0) {}

    int specificSignalCount;
    Gallant::Signal1<std::string> specificSignal;

    virtual void specificHandler (std::string value) override {
        this->specificSignalCount++;
        ASSERT_EQ(value, "Hello virtual world");
    }

};

TEST(SignalTest, Method_no_arguments) {

    SignalSender sender;
    SignalReceiver receiver;
    SpecificSignalReceiver specificReceiver;

    sender.signal1.Connect(&receiver, &SignalReceiver::handleSignal1);
    sender.signal2.Connect(&receiver, &SignalReceiver::handleSignal2);
    sender.signal3.Connect(&receiver, &SignalReceiver::handleSignal3);
    sender.signal4.Connect(&SignalReceiver::staticHandler);
    sender.signal5.Connect(&specificReceiver, &SpecificSignalReceiver::specificHandler);

    sender.signal1();
    ASSERT_EQ(1, receiver.signal1Count);
}

TEST(SignalTest, Method_1_argument) {

    SignalSender sender;
    SignalReceiver receiver;
    SpecificSignalReceiver specificReceiver;

    sender.signal1.Connect(&receiver, &SignalReceiver::handleSignal1);
    sender.signal2.Connect(&receiver, &SignalReceiver::handleSignal2);
    sender.signal3.Connect(&receiver, &SignalReceiver::handleSignal3);
    sender.signal4.Connect(&SignalReceiver::staticHandler);
    sender.signal5.Connect(&specificReceiver, &SpecificSignalReceiver::specificHandler);

    sender.signal2(3);
    ASSERT_EQ(1, receiver.signal2Count);
}

TEST(SignalTest, Method_2_arguments) {

    SignalSender sender;
    SignalReceiver receiver;
    SpecificSignalReceiver specificReceiver;

    sender.signal1.Connect(&receiver, &SignalReceiver::handleSignal1);
    sender.signal2.Connect(&receiver, &SignalReceiver::handleSignal2);
    sender.signal3.Connect(&receiver, &SignalReceiver::handleSignal3);
    sender.signal4.Connect(&SignalReceiver::staticHandler);
    sender.signal5.Connect(&specificReceiver, &SpecificSignalReceiver::specificHandler);

    float f = 3.14f;
    sender.signal3(2, f);
    ASSERT_EQ(1, receiver.signal3Count);
}

TEST(SignalTest, Static_method_1_argument) {

    SignalSender sender;
    SignalReceiver receiver;
    SpecificSignalReceiver specificReceiver;

    sender.signal1.Connect(&receiver, &SignalReceiver::handleSignal1);
    sender.signal2.Connect(&receiver, &SignalReceiver::handleSignal2);
    sender.signal3.Connect(&receiver, &SignalReceiver::handleSignal3);
    sender.signal4.Connect(&SignalReceiver::staticHandler);
    sender.signal5.Connect(&specificReceiver, &SpecificSignalReceiver::specificHandler);

    sender.signal4("Hello world");
    ASSERT_EQ(1, receiver.signal4Count);
}

TEST(SignalTest, Virtual_method_1_argument) {

    SignalSender sender;
    SignalReceiver receiver;
    SpecificSignalReceiver specificReceiver;

    sender.signal1.Connect(&receiver, &SignalReceiver::handleSignal1);
    sender.signal2.Connect(&receiver, &SignalReceiver::handleSignal2);
    sender.signal3.Connect(&receiver, &SignalReceiver::handleSignal3);
    sender.signal4.Connect(&SignalReceiver::staticHandler);
    sender.signal5.Connect(&specificReceiver, &SpecificSignalReceiver::specificHandler);

    sender.signal5("Hello virtual world");
    ASSERT_EQ(1, specificReceiver.specificSignalCount);
}

}}
