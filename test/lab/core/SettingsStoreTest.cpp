#include <string>
#include <stdexcept>
#include <gtest/gtest.h>
#include <lab/core/SettingsStore.hpp>

TEST(SettingsStore, DefaultConstructor) {
    lab::core::SettingsStore settings;
    ASSERT_EQ(0, settings.size());
    ASSERT_EQ(0, settings.keys().size());
}

TEST(SettingsStore, SettingsMapConstructor) {

    std::map<std::string, std::string> TEST_CONFIG_MAP = {
        {"width", "1024"}
        ,{"height", "768"}
        ,{"frameRate", "60.0"}
        ,{"fullScreen", "false"}
        ,{"title", "Default title"}
    };

    lab::core::SettingsStore settings(TEST_CONFIG_MAP);

    ASSERT_EQ(5, settings.size());
    ASSERT_EQ(5, settings.keys().size());
    std::vector<std::string> keys = settings.keys();
    ASSERT_EQ("frameRate", keys.at(0));
    ASSERT_EQ("fullScreen", keys.at(1));
    ASSERT_EQ("height", keys.at(2));
    ASSERT_EQ("title", keys.at(3));
    ASSERT_EQ("width", keys.at(4));
}


TEST(SettingsStore, load) {

    std::map<std::string, std::string> TEST_CONFIG_MAP = {
        {"width", "1024"}
        ,{"height", "768"}
        ,{"frameRate", "60.0"}
        ,{"fullScreen", "false"}
        ,{"title", "Default title"}
    };

    lab::core::SettingsStore settings;
    settings.load(TEST_CONFIG_MAP);

    ASSERT_EQ(5, settings.size());
    ASSERT_EQ(5, settings.keys().size());
    std::vector<std::string> keys = settings.keys();
    ASSERT_EQ("frameRate", keys.at(0));
    ASSERT_EQ("fullScreen", keys.at(1));
    ASSERT_EQ("height", keys.at(2));
    ASSERT_EQ("title", keys.at(3));
    ASSERT_EQ("width", keys.at(4));
}


TEST(SettingsStore, setInt) {
    lab::core::SettingsStore settings;
    settings.set("key", 777);
    ASSERT_EQ(777, settings.getInt("key"));
}

TEST(SettingsStore, setFloat) {
    lab::core::SettingsStore settings;
    settings.set("key", 3.14f);
    ASSERT_FLOAT_EQ(3.14f, settings.getFloat("key"));
}

TEST(SettingsStore, setDouble) {
    lab::core::SettingsStore settings;
    settings.set("key", 3.14159);
    ASSERT_DOUBLE_EQ(3.14159, settings.getDouble("key"));
}

TEST(SettingsStore, setConstChar) {
    lab::core::SettingsStore settings;
    settings.set("key", "hello");
    ASSERT_EQ("hello", settings.getString("key"));
}

TEST(SettingsStore, setString) {
    lab::core::SettingsStore settings;
    std::string val("testValue");
    settings.set("key", val);
    ASSERT_EQ(val, settings.getString("key"));
}

TEST(SettingsStore, setBool) {
    lab::core::SettingsStore settings;
    settings.set("key", true);
    ASSERT_TRUE(settings.getBool("key"));
}

TEST(SettingsStore, getNonExistingKey) {
    lab::core::SettingsStore settings;
    settings.set("key", "exists");
    ASSERT_THROW(settings.getInt("sdfasd"), std::invalid_argument);
    EXPECT_NO_THROW({
        ASSERT_EQ("exists", settings.getString("key"));
    });
}

TEST(SettingsStore, getInt) {
    lab::core::SettingsStore settings;
    settings.set("int", 1);
    ASSERT_EQ(1, settings.getInt("int"));

    settings.set("float1", 1.45);
    ASSERT_EQ(1, settings.getInt("float1"));

    settings.set("float2", 1.99f);
    ASSERT_EQ(1, settings.getInt("float2"));

    settings.set("double", 2.0634632346);
    ASSERT_EQ(2, settings.getInt("double"));

    settings.set("bool", true);
    ASSERT_EQ(1, settings.getInt("bool"));

    settings.set("bool2", false);
    ASSERT_EQ(0, settings.getInt("bool2"));

    settings.set("string", "dsdfs lkjdsfoi");
    ASSERT_EQ(0, settings.getInt("string"));

    ASSERT_THROW(settings.getInt("blah blah"), std::invalid_argument);
}

TEST(SettingsStore, getFloat) {
    lab::core::SettingsStore settings;
    settings.set("float", 1.34f);
    ASSERT_FLOAT_EQ(1.34f, settings.getFloat("float"));

    settings.set("float2", -1.34f);
    ASSERT_FLOAT_EQ(-1.34f, settings.getFloat("float2"));

    settings.set("int", 2);
    ASSERT_FLOAT_EQ(2.0f, settings.getFloat("int"));

    settings.set("double", 2.4973);
    ASSERT_FLOAT_EQ(2.4973f, settings.getFloat("double"));

    settings.set("string", "jojo");
    ASSERT_FLOAT_EQ(0.0f, settings.getFloat("string"));

    ASSERT_THROW(settings.getFloat("blah blah"), std::invalid_argument);
}

TEST(SettingsStore, getDouble) {
    lab::core::SettingsStore settings;
    settings.set("float", 1.34);
    ASSERT_DOUBLE_EQ(1.34, settings.getDouble("float"));

    settings.set("float2", -1.34);
    ASSERT_DOUBLE_EQ(-1.34, settings.getDouble("float2"));

    settings.set("int", 2);
    ASSERT_DOUBLE_EQ(2.0, settings.getDouble("int"));

    settings.set("double", 2.4973);
    ASSERT_DOUBLE_EQ(2.4973, settings.getDouble("double"));

    settings.set("string", "jojo");
    ASSERT_DOUBLE_EQ(0.0, settings.getDouble("string"));

    ASSERT_THROW(settings.getDouble("blah blah"), std::invalid_argument);
}

TEST(SettingsStore, getString) {
    lab::core::SettingsStore settings;
    settings.set("float", 1.34);
    ASSERT_EQ("1.340000", settings.getString("float"));

    settings.set("float2", -1.34);
    ASSERT_EQ("-1.340000", settings.getString("float2"));

    settings.set("int", 2);
    ASSERT_EQ("2", settings.getString("int"));

    settings.set("int2", -78932);
    ASSERT_EQ("-78932", settings.getString("int2"));

    settings.set("double", 2.4973);
    ASSERT_EQ("2.497300", settings.getString("double"));

    settings.set("string", "jojo");
    ASSERT_EQ("jojo", settings.getString("string"));

    settings.set("bool1", true);
    ASSERT_EQ("true", settings.getString("bool1"));

    settings.set("bool2", false);
    ASSERT_EQ("false", settings.getString("bool2"));

    ASSERT_THROW(settings.getString("blah blah"), std::invalid_argument);
}

TEST(SettingsStore, getBool) {
    lab::core::SettingsStore settings;

    settings.set("bool1", false);
    ASSERT_FALSE(settings.getBool("bool1"));

    settings.set("bool2", false);
    ASSERT_FALSE(settings.getBool("bool2"));

    settings.set("int", 1);
    ASSERT_TRUE(settings.getBool("int"));

    settings.set("int2", 0);
    ASSERT_FALSE(settings.getBool("int2"));

    settings.set("string1", "tRuE");
    ASSERT_TRUE(settings.getBool("string1"));

    settings.set("string2", "true");
    ASSERT_TRUE(settings.getBool("string2"));

    settings.set("string3", "falSE");
    ASSERT_FALSE(settings.getBool("string3"));

    settings.set("string4", "false");
    ASSERT_FALSE(settings.getBool("string4"));

    settings.set("float", 0.0f);
    ASSERT_FALSE(settings.getBool("float"));

    settings.set("float2", 1.0f);
    ASSERT_TRUE(settings.getBool("float2"));

    settings.set("double", 3421.322);
    ASSERT_TRUE(settings.getBool("double"));

    ASSERT_THROW(settings.getBool("blah blah"), std::invalid_argument);
}

TEST(SettingsStore, size) {
    lab::core::SettingsStore settings;
    settings.set("string3", "falSE");
    settings.set("string4", "false");
    settings.set("float", 0.0f);
    settings.set("float2", 1.0f);
    settings.set("double", 3421.322);
    ASSERT_EQ(5, settings.size());
}

TEST(SettingsStore, keys) {
    lab::core::SettingsStore settings;
    settings.set("string3", "falSE");
    settings.set("string4", "false");
    settings.set("float", 0.0f);
    settings.set("float2", 1.0f);
    settings.set("double", 3421.322);

    std::vector<std::string> keys = settings.keys();
    ASSERT_EQ("double", keys.at(0));
    ASSERT_EQ("float", keys.at(1));
    ASSERT_EQ("float2", keys.at(2));
    ASSERT_EQ("string3", keys.at(3));
    ASSERT_EQ("string4", keys.at(4));
}
