#include <gtest/gtest.h>
#include <lab/math/Vector3.hpp>

TEST(TestVector3f, DefaultConstructor) {
	lab::math::Vector3f vec;
    EXPECT_FLOAT_EQ(0.0f, vec.x);
	EXPECT_FLOAT_EQ(0.0f, vec.y);
	EXPECT_FLOAT_EQ(0.0f, vec.z);
}

TEST(TestVector3f, CopyConstructor) {
	const float x1=4.0f, y1=57.0f, z1=127.0f;
	lab::math::Vector3f vec(x1, y1, z1);
    EXPECT_FLOAT_EQ(x1, vec.x);
	EXPECT_FLOAT_EQ(y1, vec.y);
	EXPECT_FLOAT_EQ(z1, vec.z);
}

TEST(TestVector3f, AddVector3) {
	lab::math::Vector3f vec1(777.0f, 451.0f, 73544.0f);
	lab::math::Vector3f vec2(4.0f, 849.0f, -154.0f);
	lab::math::Vector3f result;
	result = vec1 + vec2;
    EXPECT_FLOAT_EQ(781.0f, result.x);
	EXPECT_FLOAT_EQ(1300.0f, result.y);
	EXPECT_FLOAT_EQ(73390.0f, result.z);
}

TEST(TestVector3f, AddFloat) {
	lab::math::Vector3f vec1(777.0f, 451.0f, 73544.0f);
	float num(5467.0f);
	lab::math::Vector3f result;
	result = vec1 + num;
    EXPECT_FLOAT_EQ(6244.0f, result.x);
	EXPECT_FLOAT_EQ(5918.0f, result.y);
	EXPECT_FLOAT_EQ(79011.0f, result.z);
}

TEST(TestVector3f, AddVector3ToSelf) {
	lab::math::Vector3f vec1(101.0f, -205.0f, 8300.0f);
	lab::math::Vector3f vec2(0.0f, -1.0f, 13.0f);
	vec1 += vec2;
    EXPECT_FLOAT_EQ(101.0f, vec1.x);
	EXPECT_FLOAT_EQ(-206.0f, vec1.y);
	EXPECT_FLOAT_EQ(8313.0f, vec1.z);
}

TEST(TestVector3f, AddFloatToSelf) {
	lab::math::Vector3f vec1(101.0f, -205.0f, 8300.0f);
	float num(4297.0f);
	vec1 += num;
    EXPECT_FLOAT_EQ(4398.0f, vec1.x);
	EXPECT_FLOAT_EQ(4092.0f, vec1.y);
	EXPECT_FLOAT_EQ(12597.0f, vec1.z);
}

TEST(TestVector3f, SubtractVector3) {
	lab::math::Vector3f vec1(751.0f, -7954.0f, 1293.0f);
	lab::math::Vector3f vec2(45.0f, 89.0f, -37.0f);
	lab::math::Vector3f result;
	result = vec1 - vec2;
    EXPECT_FLOAT_EQ(706.0f, result.x);
	EXPECT_FLOAT_EQ(-8043.0f, result.y);
	EXPECT_FLOAT_EQ(1330.0f, result.z);
}

TEST(TestVector3f, SubtractInt) {
	lab::math::Vector3f vec1(751.0f, -7954.0f, 1293.0f);
	float num(117.0f);
	lab::math::Vector3f result;
	result = vec1 - num;
    EXPECT_FLOAT_EQ(634.0f, result.x);
	EXPECT_FLOAT_EQ(-8071.0f, result.y);
	EXPECT_FLOAT_EQ(1176.0f, result.z);
}

TEST(TestVector3f, SubtractVector3FromSelf) {
	lab::math::Vector3f vec1(576.0f, -6.0f, 586.0f);
	lab::math::Vector3f vec2(32.0f, 164.0f, 4913.0f);
	vec1 -= vec2;
    EXPECT_FLOAT_EQ(544.0f, vec1.x);
	EXPECT_FLOAT_EQ(-170.0f, vec1.y);
	EXPECT_FLOAT_EQ(-4327.0f, vec1.z);
}

TEST(TestVector3f, MultiplyVector3) {
	lab::math::Vector3f vec1(16.0f, 3.0f, 8.0f);
	lab::math::Vector3f vec2(3.0f, 7.0f, 2.0f);
	lab::math::Vector3f result;
	result = vec1*vec2;
    EXPECT_FLOAT_EQ(48.0f, result.x);
	EXPECT_FLOAT_EQ(21.0f, result.y);
	EXPECT_FLOAT_EQ(16.0f, result.z);
}

TEST(TestVector3f, MultiplyFloat) {
	lab::math::Vector3f vec1(16.0f, 3.0f, 8.0f);
	float num(5.0f);
	vec1 *= num;
    EXPECT_FLOAT_EQ(80.0f, vec1.x);
	EXPECT_FLOAT_EQ(15.0f, vec1.y);
	EXPECT_FLOAT_EQ(40.0f, vec1.z);
}

TEST(TestVector3f, MultiplyVector3WithSelf) {
	lab::math::Vector3f vec1(16.0f, 3.0f, 8.0f);
	lab::math::Vector3f vec2(3.0f, 7.0f, 2.0f);
	vec1 *= vec2;
    EXPECT_FLOAT_EQ(48.0f, vec1.x);
	EXPECT_FLOAT_EQ(21.0f, vec1.y);
	EXPECT_FLOAT_EQ(16.0f, vec1.z);
}

TEST(TestVector3f, MultiplyFloatWithSelf) {
	lab::math::Vector3f vec1(16.0f, 3.0f, 8.0f);
	float num(6.0f);
	vec1 *= num;
    EXPECT_FLOAT_EQ(96.0f, vec1.x);
	EXPECT_FLOAT_EQ(18.0f, vec1.y);
	EXPECT_FLOAT_EQ(48.0f, vec1.z);
}

TEST(TestVector3f, DivideVector3) {
	lab::math::Vector3f vec1(16.0f, 29.0f, 8.0f);
	lab::math::Vector3f vec2(4.0f, 5.0f, 2.0f);
	lab::math::Vector3f result;
	result = vec1/vec2;
    EXPECT_FLOAT_EQ(4.0f, result.x);
	EXPECT_FLOAT_EQ(5.8f, result.y);
	EXPECT_FLOAT_EQ(4.0f, result.z);
}

TEST(TestVector3f, DivideVector3Zero) {
	lab::math::Vector3f vec1(16.0f, 29.0f, 8.0f);
	lab::math::Vector3f vec2(0.0f, 0.0f, 0.0f);
	lab::math::Vector3f result;
	result = vec1/vec2;
    EXPECT_FLOAT_EQ(16.0f, result.x);
	EXPECT_FLOAT_EQ(29.0f, result.y);
	EXPECT_FLOAT_EQ(8.0f, result.z);
}

TEST(TestVector3i, CompareVector3NotEqual) {
	lab::math::Vector3i vec1(16, 29, 8);
	lab::math::Vector3i vec2(11, 29, 3);
	bool result;
	result = (vec1 != vec2);
	EXPECT_TRUE(result);
	result = (vec1 == vec2);
	EXPECT_FALSE(result);
}

TEST(TestVector3i, CompareVector3Equal) {
	lab::math::Vector3i vec1(16, 29, 8);
	lab::math::Vector3i vec2(16, 29, 8);
	bool result;
	result = (vec1 == vec2);
	EXPECT_TRUE(result);
	vec1.x = 17;
	result = (vec1 == vec2);
	EXPECT_FALSE(result);
}

TEST(TestVector3f, StreamOutput) {
	std::stringstream result;
	std::stringstream expected;
	lab::math::Vector3f vec1(16.3f, 29.34f, 8.56f);
	result << vec1;
	expected << "16.3, 29.34, 8.56";
	EXPECT_STREQ(result.str().c_str(), expected.str().c_str());
}

TEST(TestVector3f, Match01) {
	lab::math::Vector3f vec1(0.0000000001f, 5.0f*2-10.0f, 0.0f);
	lab::math::Vector3f vec2(0.0f, 0.1f - 2*0.05f, -10.5f+10.5f);
	bool result;
	result = vec1.match(vec2);
	EXPECT_TRUE(result);
}

TEST(TestVector3f, Match02) {
	lab::math::Vector3f vec1(2.0f, 6.0f, 3.5f);	
	for (int i = 0; i < 5; i++) {
		vec1 *= 2.0f;
	}
	lab::math::Vector3f vec2(pow(2.0f,5.0f)*2.0f, 192.0f, 3.5f*2.0f*2.0f*2.0f*2.0f*2.0f);
	bool result(vec1.match(vec2));
	EXPECT_TRUE(result);
}

TEST(TestVector3f, Length) {
	lab::math::Vector3f vec1(2.0f, 6.0f, 3.5f);	
	float result = vec1.length();
	EXPECT_FLOAT_EQ(7.228416147f, result);	
}

TEST(TestVector3f, Normalize) {
	lab::math::Vector3f vec1(23.2f, 18.123f, 3.5879f);	
	vec1.normalize();
	EXPECT_FLOAT_EQ(0.782269141f, vec1.x);
	EXPECT_FLOAT_EQ(0.611080329f, vec1.y);
	EXPECT_FLOAT_EQ(0.120978597f, vec1.z);
}

TEST(TestVector3f, GetNormalized) {
	lab::math::Vector3f vec1(23.2f, 18.123f, 3.5879f);	
	lab::math::Vector3f result;
	result = vec1.getNormalized();
	EXPECT_FLOAT_EQ(0.782269141f, result.x);
	EXPECT_FLOAT_EQ(0.611080329f, result.y);
	EXPECT_FLOAT_EQ(0.120978597f, result.z);
}

TEST(TestVector3f, Dot) {
	lab::math::Vector3f vec1(23.2f, 18.123f, 2.9f);
	lab::math::Vector3f vec2(314.2f, 41.0f, 3.23f);
	float result;
	result = vec1.dot(vec2);
	EXPECT_FLOAT_EQ(8041.8506f, result);
}

TEST(TestVector3f, Cross) {
	lab::math::Vector3f vec1(23.2f, 18.123f, 2.9f);
	lab::math::Vector3f vec2(314.2f, 41.0f, 3.23f);
	lab::math::Vector3f result;
	result = vec1.cross(vec2);
	EXPECT_FLOAT_EQ(-60.362713f, result.x);
	EXPECT_FLOAT_EQ(836.24402f, result.y);
	EXPECT_FLOAT_EQ(-4743.0464f, result.z);
}

TEST(TestVector3f, Lerp) {
	lab::math::Vector3f vec1(4.3f, 7.5f, 12.4f);
	lab::math::Vector3f vec2(1.12f, 3.98f, -2.95f);
	vec1.lerp(vec2, 0.81f);
	EXPECT_FLOAT_EQ(1.7242f, vec1.x);
	EXPECT_FLOAT_EQ(4.6487999f, vec1.y);
	EXPECT_FLOAT_EQ(-0.033499718f, vec1.z);
}

TEST(TestVector4f, CopyTo) {
	lab::math::Vector3f vec1(-1.0f, 0.0f, -8.0f);
	lab::math::Vector3f result;
	vec1.copyTo(result);
	EXPECT_FLOAT_EQ(vec1.x, result.x);
	EXPECT_FLOAT_EQ(vec1.y, result.y);
	EXPECT_FLOAT_EQ(vec1.z, result.z);
}