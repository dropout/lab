#include <gtest/gtest.h>
#include <lab/math/Vector3.hpp>
#include <lab/math/Vector4.hpp>

TEST(TestVector4f, DefaultConstructor)
{
	lab::math::Vector4f vec;
    EXPECT_FLOAT_EQ(0.0f, vec.x);
	EXPECT_FLOAT_EQ(0.0f, vec.y);
	EXPECT_FLOAT_EQ(0.0f, vec.z);
	EXPECT_FLOAT_EQ(0.0f, vec.w);
}

TEST(TestVector4f, CopyConstructor3Params)
{
	const float x1=4.0f, y1=57.0f, z1=127.0f;
	lab::math::Vector4f vec(x1, y1, z1);
    EXPECT_FLOAT_EQ(x1, vec.x);
	EXPECT_FLOAT_EQ(y1, vec.y);
	EXPECT_FLOAT_EQ(z1, vec.z);
	EXPECT_FLOAT_EQ(0.0f, vec.w);
}

TEST(TestVector4f, CopyConstructor4Params)
{
	const float x1=4.0f, y1=57.0f, z1=127.0f, w1=-43.8f;
	lab::math::Vector4f vec(x1, y1, z1, w1);
    EXPECT_FLOAT_EQ(x1, vec.x);
	EXPECT_FLOAT_EQ(y1, vec.y);
	EXPECT_FLOAT_EQ(z1, vec.z);
	EXPECT_FLOAT_EQ(w1, vec.w);
}

TEST(TestVector4f, CopyConstructorFromVector3)
{
	const float x1=4.0f, y1=57.0f, z1=127.0f;
	lab::math::Vector3f vec1(x1, y1, z1);	
	lab::math::Vector4f vec2(vec1);
    EXPECT_FLOAT_EQ(x1, vec2.x);
	EXPECT_FLOAT_EQ(y1, vec2.y);
	EXPECT_FLOAT_EQ(z1, vec2.z);
	EXPECT_FLOAT_EQ(0.0f, vec2.w);
}

TEST(TestVector4f, AddVector4)
{
	lab::math::Vector4f vec1(777.0f, 451.0f, 73544.0f, 0.0f);
	lab::math::Vector4f vec2(4.0f, 849.0f, -154.0f, -48.2f );
	lab::math::Vector4f result;
	result = vec1 + vec2;
    EXPECT_FLOAT_EQ(781.0f, result.x);
	EXPECT_FLOAT_EQ(1300.0f, result.y);
	EXPECT_FLOAT_EQ(73390.0f, result.z);
	EXPECT_FLOAT_EQ(-48.2f, result.w);
}

TEST(TestVector4f, AddFloat)
{
	lab::math::Vector4f vec1(777.0f, 451.0f, 73544.0f, 0.0f);
	float num(5467.0f);
	lab::math::Vector4f result;
	result = vec1 + num;
    EXPECT_FLOAT_EQ(6244.0f, result.x);
	EXPECT_FLOAT_EQ(5918.0f, result.y);
	EXPECT_FLOAT_EQ(79011.0f, result.z);
	EXPECT_FLOAT_EQ(5467.0f, result.w);
}

TEST(TestVector4f, AddVector4ToSelf)
{
	lab::math::Vector4f vec1(101.0f, -205.0f, 8300.0f, 0.0F);
	lab::math::Vector4f vec2(0.0f, -1.0f, 13.0f, 746.4587f);
	vec1 += vec2;
    EXPECT_FLOAT_EQ(101.0f, vec1.x);
	EXPECT_FLOAT_EQ(-206.0f, vec1.y);
	EXPECT_FLOAT_EQ(8313.0f, vec1.z);
	EXPECT_FLOAT_EQ(746.4587f, vec1.w);
}

TEST(TestVector4f, AddFloatToSelf)
{
	lab::math::Vector4f vec1(101.0f, -205.0f, 8300.0f, 1.3f);
	float num(4297.0f);
	vec1 += num;
    EXPECT_FLOAT_EQ(4398.0f, vec1.x);
	EXPECT_FLOAT_EQ(4092.0f, vec1.y);
	EXPECT_FLOAT_EQ(12597.0f, vec1.z);
	EXPECT_FLOAT_EQ(4298.3f, vec1.w);
}

TEST(TestVector4f, SubtractVector3)
{
	lab::math::Vector4f vec1(751.0f, -7954.0f, 1293.0f, 0.0f);
	lab::math::Vector4f vec2(45.0f, 89.0f, -37.0f, 1.0f);
	lab::math::Vector4f result;
	result = vec1 - vec2;
    EXPECT_FLOAT_EQ(706.0f, result.x);
	EXPECT_FLOAT_EQ(-8043.0f, result.y);
	EXPECT_FLOAT_EQ(1330.0f, result.z);
	EXPECT_FLOAT_EQ(-1.0f, result.w);
}

TEST(TestVector4f, SubtractInt)
{
	lab::math::Vector4f vec1(751.0f, -7954.0f, 1293.0f, -1.0f);
	float num(117.0f);
	lab::math::Vector4f result;
	result = vec1 - num;
    EXPECT_FLOAT_EQ(634.0f, result.x);
	EXPECT_FLOAT_EQ(-8071.0f, result.y);
	EXPECT_FLOAT_EQ(1176.0f, result.z);
	EXPECT_FLOAT_EQ(-118.0f, result.w);
}

TEST(TestVector4f, SubtractVector3FromSelf)
{
	lab::math::Vector4f vec1(576.0f, -6.0f, 586.0f, 5.46f);
	lab::math::Vector4f vec2(32.0f, 164.0f, 4913.0f, 3.43f );
	vec1 -= vec2;
    EXPECT_FLOAT_EQ(544.0f, vec1.x);
	EXPECT_FLOAT_EQ(-170.0f, vec1.y);
	EXPECT_FLOAT_EQ(-4327.0f, vec1.z);
	EXPECT_FLOAT_EQ(2.03f, vec1.w);
}

TEST(TestVector4f, MultiplyVector3)
{
	lab::math::Vector4f vec1(16.0f, 3.0f, 8.0f, 1.5f) ;
	lab::math::Vector4f vec2(3.0f, 7.0f, 2.0f, 10.0f);
	lab::math::Vector4f result;
	result = vec1*vec2;
    EXPECT_FLOAT_EQ(48.0f, result.x);
	EXPECT_FLOAT_EQ(21.0f, result.y);
	EXPECT_FLOAT_EQ(16.0f, result.z);
	EXPECT_FLOAT_EQ(15.0f, result.w);
}

TEST(TestVector4f, MultiplyFloat)
{
	lab::math::Vector4f vec1(16.0f, 3.0f, 8.0f, 2.0f);
	float num(5.0f);
	vec1 *= num;
    EXPECT_FLOAT_EQ(80.0f, vec1.x);
	EXPECT_FLOAT_EQ(15.0f, vec1.y);
	EXPECT_FLOAT_EQ(40.0f, vec1.z);
	EXPECT_FLOAT_EQ(10.0f, vec1.w);
}

TEST(TestVector4f, MultiplyVector4WithSelf)
{
	lab::math::Vector4f vec1(16.0f, 3.0f, 8.0f, 0.0f);
	lab::math::Vector4f vec2(3.0f, 7.0f, 2.0f, 489.034f);
	vec1 *= vec2;
    EXPECT_FLOAT_EQ(48.0f, vec1.x);
	EXPECT_FLOAT_EQ(21.0f, vec1.y);
	EXPECT_FLOAT_EQ(16.0f, vec1.z);
	EXPECT_FLOAT_EQ(0.0f, vec1.w);
}

TEST(TestVector4f, MultiplyFloatWithSelf)
{
	lab::math::Vector4f vec1(16.0f, 3.0f, 8.0f, 7.5f);
	float num(6.0f);
	vec1 *= num;
    EXPECT_FLOAT_EQ(96.0f, vec1.x);
	EXPECT_FLOAT_EQ(18.0f, vec1.y);
	EXPECT_FLOAT_EQ(48.0f, vec1.z);
	EXPECT_FLOAT_EQ(45.0f, vec1.w);
}

TEST(TestVector4f, DivideVector4)
{
	lab::math::Vector4f vec1(16.0f, 29.0f, 8.0f, 12.0f);
	lab::math::Vector4f vec2(4.0f, 5.0f, 2.0f, 4.0f);
	lab::math::Vector4f result;
	result = vec1/vec2;
    EXPECT_FLOAT_EQ(4.0f, result.x);
	EXPECT_FLOAT_EQ(5.8f, result.y);
	EXPECT_FLOAT_EQ(4.0f, result.z);
	EXPECT_FLOAT_EQ(3.0f, result.w);
}

TEST(TestVector4f, DivideVector4Zero)
{
	lab::math::Vector4f vec1(16.0f, 29.0f, 8.0f, 88.0f );
	lab::math::Vector4f vec2(0.0f, 0.0f, 0.0f, 0.0f);
	lab::math::Vector4f result;
	result = vec1/vec2;
    EXPECT_FLOAT_EQ(16.0f, result.x);
	EXPECT_FLOAT_EQ(29.0f, result.y);
	EXPECT_FLOAT_EQ(8.0f, result.z);
	EXPECT_FLOAT_EQ(88.0f, result.w);
}

TEST(TestVector4i, CompareVector4NotEqual)
{
	lab::math::Vector4i vec1(16, 29, 8, 3);
	lab::math::Vector4i vec2(11, 29, 3, 48);
	bool result;
	result = (vec1 != vec2);
	EXPECT_TRUE(result);
	result = (vec1 == vec2);
	EXPECT_FALSE(result);
}

TEST(TestVector4i, CompareVector4Equal)
{
	lab::math::Vector4i vec1(16, 29, 8, 546);
	lab::math::Vector4i vec2(16, 29, 8, 546);
	bool result;
	result = (vec1 == vec2);
	EXPECT_TRUE(result);
	vec1.w = 800;
	result = (vec1 == vec2);
	EXPECT_FALSE(result);
}

TEST(TestVector4f, StreamOutput)
{
	std::stringstream result;
	std::stringstream expected;
	lab::math::Vector4f vec1(16.3f, 29.34f, 8.56f, 87.342f);
	result << vec1;
	expected << "16.3, 29.34, 8.56, 87.342";
	EXPECT_STREQ(result.str().c_str(), expected.str().c_str());
}

TEST(TestVector4f, Match)
{
	lab::math::Vector4f vec1(2.0f, 6.0f, 3.5f, 0.0100001f);
	lab::math::Vector4f vec2(2.0f, 6.0f, 3.5f, 0.0000999f);
	bool result(vec1.match(vec2));
	EXPECT_FALSE(result);
	vec2.w = 0.0100001f;
	result = vec1.match(vec2);
	EXPECT_TRUE(result);
}

TEST(TestVector4f, MatchXYZ)
{
	lab::math::Vector4f vec1(2.0f, 6.0f, 3.5f, 0.0100001f);
	lab::math::Vector4f vec2(2.0f, 6.0f, 3.5f, 0.0000999f);
	bool result(vec1.match(vec2));
	EXPECT_FALSE(result);
	result = vec1.matchXYZ(vec2);
	EXPECT_TRUE(result);
	vec2.w = 0.0100001f;
	result = vec1.matchXYZ(vec2);
	EXPECT_TRUE(result);
}

TEST(TestVector4f, Length)
{
	lab::math::Vector4f vec1(2.0f, 6.0f, 3.5f, 3.3f);
	float result = vec1.length();
	EXPECT_FLOAT_EQ(7.94606821f, result);	
}

TEST(TestVector4f, Normalize)
{
	lab::math::Vector4f vec1(23.2f, 18.123f, 3.5879f, 7.42f);	
	vec1.normalize();
	EXPECT_FLOAT_EQ(0.75887841f, vec1.x);
	EXPECT_FLOAT_EQ(0.59280831f, vec1.y);
	EXPECT_FLOAT_EQ(0.11736119f, vec1.z);
	EXPECT_FLOAT_EQ(0.24271023f, vec1.w);
}

TEST(TestVector4f, GetNormalized)
{
	lab::math::Vector4f vec1(23.2f, 18.123f, 3.5879f, 7.42f);	
	lab::math::Vector4f result;
	result = vec1.getNormalized();
	EXPECT_FLOAT_EQ(0.75887841f, result.x);
	EXPECT_FLOAT_EQ(0.59280831f, result.y);
	EXPECT_FLOAT_EQ(0.11736119f, result.z);
	EXPECT_FLOAT_EQ(0.24271023f, result.w);
}

TEST(TestVector4f, Dot)
{
	lab::math::Vector4f vec1(23.2f, 18.123f, 2.9f, 1.0f);
	lab::math::Vector4f vec2(314.2f, 41.0f, 3.23f, 4.3f);
	float result;
	result = vec1.dot(vec2);
	EXPECT_FLOAT_EQ(8046.15f, result);
}

TEST(TestVector4f, Cross)
{
	lab::math::Vector4f vec1(23.2f, 18.123f, 2.9f, 1.0f);
	lab::math::Vector4f vec2(314.2f, 41.0f, 3.23f, 4.3f);
	lab::math::Vector4f result;
	result = vec1.cross(vec2);
	EXPECT_FLOAT_EQ(-60.362713f, result.x);
	EXPECT_FLOAT_EQ(836.24402f, result.y);
	EXPECT_FLOAT_EQ(-4743.0464f, result.z);
	EXPECT_FLOAT_EQ(1.0f, result.w);
}

TEST(TestVector4f, Lerp)
{
	lab::math::Vector4f vec1(4.3f, 7.5f, 12.4f, 12.9f);
	lab::math::Vector4f vec2(1.12f, 3.98f, -2.95f, -6.3f);
	vec1.lerp(vec2, 0.81f);
	EXPECT_FLOAT_EQ(1.7242f, vec1.x);
	EXPECT_FLOAT_EQ(4.6487999f, vec1.y);
	EXPECT_FLOAT_EQ(-0.033499718f, vec1.z);
	EXPECT_FLOAT_EQ(-2.6520014f, vec1.w);
}

TEST(TestVector4f, Clone)
{
	lab::math::Vector4f vec1(-1.0f, 0.0f, -8.0f, 777.2134f);
	lab::math::Vector4f result;
	vec1.copyTo(result);
	EXPECT_FLOAT_EQ(vec1.x, result.x);
	EXPECT_FLOAT_EQ(vec1.y, result.y);
	EXPECT_FLOAT_EQ(vec1.z, result.z);
	EXPECT_FLOAT_EQ(vec1.w, result.w);
}
