#include <gtest/gtest.h>
#include <lab/math/VectorUtils.hpp>

TEST(TestVectorUtils, DistanceVector2)
{
	lab::math::Vector2f vec1(4.6f, 9.3f);
	lab::math::Vector2f vec2(1.0f, 0.0f);
	float result;
	result = lab::math::util::distance(vec1, vec2);
	EXPECT_FLOAT_EQ(9.972462083f, result);
}

TEST(TestVectorUtils, DistanceVector3)
{
	lab::math::Vector3f vec1(4.6f, 9.3f, -4.5f);
	lab::math::Vector3f vec2(1.0f, 0.0f, 0.0f);
	float result;
	result = lab::math::util::distance(vec1, vec2);
	EXPECT_FLOAT_EQ(10.94075f, result);
}

TEST(TestVectorUtils, DistanceVector4)
{
	lab::math::Vector4f vec1(4.6f, 9.3f, -4.5f, 45.0f);
	lab::math::Vector4f vec2(1.0f, 0.0f, 0.0f, -12.0f);
	float result;
	result = lab::math::util::distance(vec1, vec2);
	EXPECT_FLOAT_EQ(10.94075f, result);
}

TEST(TestVectorUtils, DistanceSquaredVector2)
{
	lab::math::Vector2f vec1(5.0f, 13.5f);
	lab::math::Vector2f vec2(-1.0f, 0.0f);
	float result;
	result = lab::math::util::distanceSquared(vec1, vec2);
	EXPECT_FLOAT_EQ(218.25f, result);
}

TEST(TestVectorUtils, DistanceSquaredVector3)
{
	lab::math::Vector3f vec1(5.0f, 13.5f, 0.4f);
	lab::math::Vector3f vec2(-1.0f, 0.0f, -8.0f);
	float result;
	result = lab::math::util::distanceSquared(vec1, vec2);
	EXPECT_FLOAT_EQ(288.81f, result);
}

TEST(TestVectorUtils, DistanceSquaredVector4)
{
	lab::math::Vector4f vec1(5.0f, 13.5f, 0.4f, -11.1f);
	lab::math::Vector4f vec2(-1.0f, 0.0f, -8.0f, 2.89f);
	float result;
	result = lab::math::util::distanceSquared(vec1, vec2);
	EXPECT_FLOAT_EQ(288.81f, result);
}

TEST(TestVectorUtils, ManhattanDistanceVector2)
{
	lab::math::Vector2f vec1(0.0f, 23.0f);
	lab::math::Vector2f vec2(-12.8f, 0.0f);
	bool result;
	result = lab::math::util::manhattanDistance(vec1, vec2, 15.0f);
	EXPECT_FALSE(result);
	result = lab::math::util::manhattanDistance(vec1, vec2, 60.2f);
	EXPECT_TRUE(result);
}

TEST(TestVectorUtils, ManhattanDistanceVector3)
{
	lab::math::Vector3f vec1(0.0f, 23.0f, 0.0f);
	lab::math::Vector3f vec2(0.0f, 0.0f, 60.123f);
	bool result;
	result = lab::math::util::manhattanDistance(vec1, vec2, 50.0f);
	EXPECT_FALSE(result);
	result = lab::math::util::manhattanDistance(vec1, vec2, 60.2f);
	EXPECT_TRUE(result);
}

TEST(TestVectorUtils, ManhattanDistanceVector4)
{
	lab::math::Vector4f vec1(0.0f, 23.0f, 0.0f, 1.84f);
	lab::math::Vector4f vec2(0.0f, 0.0f, 60.123f, 6.45f);
	bool result;
	result = lab::math::util::manhattanDistance(vec1, vec2, 50.0f);
	EXPECT_FALSE(result);
	result = lab::math::util::manhattanDistance(vec1, vec2, 60.2f);
	EXPECT_TRUE(result);
}