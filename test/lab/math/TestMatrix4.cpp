#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <lab/math/Matrix4.hpp>
#include <lab/math/Vector4.hpp>

using ::testing::ContainerEq;

TEST(TestMatrix4, DefaultConstructor) {
	float reference[16] = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	lab::math::Matrix4f mat;
	ASSERT_THAT(mat.m, ContainerEq(reference));
}

TEST(TestMatrix4, CopyConstructorNum) {
	float reference[16] = {0.0f};
	lab::math::Matrix4f mat(0);
	ASSERT_THAT(mat.m, ContainerEq(reference));
}

TEST(TestMatrix4, CopyConstructorMatrix) {
	float reference[16] = {
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	};
	lab::math::Matrix4f mat1(reference);
	lab::math::Matrix4f mat2(mat1);
	EXPECT_THAT( mat1.m, ContainerEq(mat2.m) );
}

TEST(TestMatrix4, CopyConstructorArrayColumnMajor) {
	float reference[16] = {
		 1.0f,  2.0f,  3.0f,  4.0f,
		 5.0f,  6.0f,  7.0f,  8.0f,
		 9.0f, 10.0f, 11.0f, 12.0f,
		13.0f, 14.0f, 15.0f, 16.0f
	};

	float src[16] = {0.0f};
	int i = sizeof(src)/sizeof(src[0])-1;
	while (i >= 0) {
		src[i] = (float) i + 1.0f;
		i--;
	}
	lab::math::Matrix4f mat(src);
	ASSERT_THAT(mat.m, ContainerEq(reference));
}

TEST(TestMatrix4, CopyConstructorArrayRowMajor) {
	float reference[16] = {
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	};

	float src[16] = {0.0f};
	int i = sizeof(src)/sizeof(src[0])-1;
	while (i >= 0) {
		src[i] = (float) i+1.0f;
		i--;
	}
	lab::math::Matrix4f mat(src, true);
	ASSERT_THAT(mat.m, ContainerEq(reference));
}

TEST(TestMatrix4, CopyConstructor16args) {
	float reference[16] = {
		 1.0f,  2.0f,  3.0f,  4.0f,
		 5.0f,  6.0f,  7.0f,  8.0f,
		 9.0f, 10.0f, 11.0f, 12.0f,
		13.0f, 14.0f, 15.0f, 16.0f
	};

	lab::math::Matrix4f mat(
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	);
	
	ASSERT_THAT(mat.m, ContainerEq(reference));
}

TEST(TestMatrix4, OperatorAdd) {
	float reference[16] = {
		 2.0f,  4.0f,  6.0f,  8.0f,
		10.0f, 12.0f, 14.0f, 16.0f,
		18.0f, 20.0f, 22.0f, 24.0f,
		26.0f, 28.0f, 30.0f, 32.0f
	};
	
	lab::math::Matrix4f mat1(
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	);

	lab::math::Matrix4f mat2(
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	);

	lab::math::Matrix4f result;
	result = mat1 + mat2;
	ASSERT_THAT(result.m, ContainerEq(reference));

	mat1 += mat2;
	ASSERT_THAT(mat1.m, ContainerEq(reference));
}

TEST(TestMatrix4, OperatorSubtract) {
	float reference[16] = {
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f
	};	
	
	lab::math::Matrix4f mat1(
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	);

	lab::math::Matrix4f mat2(
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	);

	lab::math::Matrix4f result;
	result = mat1 - mat2;
	ASSERT_THAT(result.m, ContainerEq(reference));

	mat1 -= mat2;
	ASSERT_THAT(mat1.m, ContainerEq(reference));
}

TEST(TestMatrix4, OperatorMultiply) {
	float reference[16] = {
		30.0f,   70.0f, 110.0f, 150.0f,
		70.0f,  174.0f, 278.0f, 382.0f,
		110.0f, 278.0f, 446.0f, 614.0f,
		150.0f, 382.0f, 614.0f, 846.0f
	};	
	
	lab::math::Matrix4f mat1(
		 1.0f,  2.0f,  3.0f,  4.0f,
		 5.0f,  6.0f,  7.0f,  8.0f,
		 9.0f, 10.0f, 11.0f, 12.0f,
		13.0f, 14.0f, 15.0f, 16.0f
	);

	lab::math::Matrix4f mat2(
		1.0f, 5.0f,  9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	);	

	lab::math::Matrix4f result;
	result = mat1 * mat2;

	ASSERT_THAT(result.m, ContainerEq(reference));

	mat1 *= mat2;
	ASSERT_THAT(mat1.m, ContainerEq(reference));
}
