#include <gtest/gtest.h>
#include <lab/math/Vector2.hpp>

TEST(TestVector2f, DefaultConstructor) {
	lab::math::Vector2f vec;
    EXPECT_FLOAT_EQ(0.0f, vec.x);
	EXPECT_FLOAT_EQ(0.0f, vec.y);
}

TEST(TestVector2f, CopyConstructor) {
	const float x1=4.0f, y1=57.0f;
	lab::math::Vector2f vec(x1, y1);
    EXPECT_FLOAT_EQ(x1, vec.x);
	EXPECT_FLOAT_EQ(y1, vec.y);
}

TEST(TestVector2f, AddVector2) {
	lab::math::Vector2f vec1(777.0f, 451.0f);
	lab::math::Vector2f vec2(4.0f, 849.0f);
	lab::math::Vector2f result;
	result = vec1 + vec2;
    EXPECT_FLOAT_EQ(781.0f, result.x);
	EXPECT_FLOAT_EQ(1300.0f, result.y);
}

TEST(TestVector2f, AddFloat) {
	lab::math::Vector2f vec1(777.0f, 451.0f);
	float num(5467.0f);
	lab::math::Vector2f result;
	result = vec1 + num;
    EXPECT_FLOAT_EQ(6244.0f, result.x);
	EXPECT_FLOAT_EQ(5918.0f, result.y);
}

TEST(TestVector2f, AddVector2ToSelf) {
	lab::math::Vector2f vec1(101.0f, -205.0f);
	lab::math::Vector2f vec2(0.0f, -1.0f);
	vec1 += vec2;
    EXPECT_FLOAT_EQ(101.0f, vec1.x);
	EXPECT_FLOAT_EQ(-206.0f, vec1.y);
}

TEST(TestVector2f, AddFloatToSelf) {
	lab::math::Vector2f vec1(101.0f, -205.0f);
	float num(4297.0f);
	vec1 += num;
    EXPECT_FLOAT_EQ(4398.0f, vec1.x);
	EXPECT_FLOAT_EQ(4092.0f, vec1.y);
}

TEST(TestVector2f, SubtractVector2) {
	lab::math::Vector2f vec1(751.0f, -7954.0f);
	lab::math::Vector2f vec2(45.0f, 89.0f);
	lab::math::Vector2f result;
	result = vec1 - vec2;
    EXPECT_FLOAT_EQ(706.0f, result.x);
	EXPECT_FLOAT_EQ(-8043.0f, result.y);
}

TEST(TestVector2f, SubtractInt) {
	lab::math::Vector2f vec1(751.0f, -7954.0f);
	float num(117.0f);
	lab::math::Vector2f result;
	result = vec1 - num;
    EXPECT_FLOAT_EQ(634.0f, result.x);
	EXPECT_FLOAT_EQ(-8071.0f, result.y);
}

TEST(TestVector2f, SubtractVector2fromSelf) {
	lab::math::Vector2f vec1(576.0f, -6.0f);
	lab::math::Vector2f vec2(32.0f, 164.0f);
	vec1 -= vec2;
    EXPECT_FLOAT_EQ(544.0f, vec1.x);
	EXPECT_FLOAT_EQ(-170.0f, vec1.y);
}

TEST(TestVector2f, MultiplyVector2) {
	lab::math::Vector2f vec1(16.0f, 3.0f);
	lab::math::Vector2f vec2(3.0f, 7.0f);
	lab::math::Vector2f result;
	result = vec1*vec2;
    EXPECT_FLOAT_EQ(48.0f, result.x);
	EXPECT_FLOAT_EQ(21.0f, result.y);
}

TEST(TestVector2f, MultiplyFloat) {
	lab::math::Vector2f vec1(16.0f, 3.0f);
	float num(5.0f);
	vec1 *= num;
    EXPECT_FLOAT_EQ(80.0f, vec1.x);
	EXPECT_FLOAT_EQ(15.0f, vec1.y);
}

TEST(TestVector2f, MultiplyVector2WithSelf) {
	lab::math::Vector2f vec1(16.0f, 3.0f);
	lab::math::Vector2f vec2(3.0f, 7.0f);
	vec1 *= vec2;
    EXPECT_FLOAT_EQ(48.0f, vec1.x);
	EXPECT_FLOAT_EQ(21.0f, vec1.y);
}

TEST(TestVector2f, MultiplyFloatWithSelf) {
	lab::math::Vector2f vec1(16.0f, 3.0f);
	float num(6.0f);
	vec1 *= num;
    EXPECT_FLOAT_EQ(96.0f, vec1.x);
	EXPECT_FLOAT_EQ(18.0f, vec1.y);
}

TEST(TestVector2f, DivideVector2) {
	lab::math::Vector2f vec1(16.0f, 29.0f);
	lab::math::Vector2f vec2(4.0f, 5.0f);
	lab::math::Vector2f result;
	result = vec1/vec2;
    EXPECT_FLOAT_EQ(4.0f, result.x);
	EXPECT_FLOAT_EQ(5.8f, result.y);
}

TEST(TestVector2f, DivideVector2Zero) {
	lab::math::Vector2f vec1(16.0f, 29.0f);
	lab::math::Vector2f vec2(0.0f, 0.0f);
	lab::math::Vector2f result;
	result = vec1/vec2;
    EXPECT_FLOAT_EQ(16.0f, result.x);
	EXPECT_FLOAT_EQ(29.0f, result.y);
}

TEST(TestVector2i, CompareVector2NotEqual) {
	lab::math::Vector2i vec1(16, 29);
	lab::math::Vector2i vec2(11, 29);
	bool result;
	result = (vec1 != vec2);
	EXPECT_TRUE(result);
	result = (vec1 == vec2);
	EXPECT_FALSE(result);
}

TEST(TestVector2i, CompareVector2Equal) {
	lab::math::Vector2i vec1(16, 29);
	lab::math::Vector2i vec2(16, 29);
	bool result;
	result = (vec1 == vec2);
	EXPECT_TRUE(result);
	vec1.x = 17;
	result = (vec1 == vec2);
	EXPECT_FALSE(result);
}

TEST(TestVector2f, StreamOutput) {
	std::stringstream result;
	std::stringstream expected;
	lab::math::Vector2f vec1(16.3f, 29.34f);
	result << vec1;
	expected << "16.3, 29.34";
	EXPECT_STREQ(result.str().c_str(), expected.str().c_str());
}

TEST(TestVector2f, Match01) {
	lab::math::Vector2f vec1(0.0000000001f, 5.0f*2-10.0f);
	lab::math::Vector2f vec2(0.0f, 0.1f - 2*0.05f);
	bool result;
	result = vec1.match(vec2);
	EXPECT_TRUE(result);
}

TEST(TestVector2f, Match02) {
	lab::math::Vector2f vec1(2.0f, 6.0f);	
	for (int i = 0; i < 5; i++) {
		vec1 *= 2.0f;
	}
	lab::math::Vector2f vec2(pow(2.0f,5.0f)*2.0f, 192.0f);
	bool result(vec1.match(vec2));
	EXPECT_TRUE(result);
}

TEST(TestVector2f, Length) {
	lab::math::Vector2f vec1(2.0f, 6.0f);	
	float result = vec1.length();
	EXPECT_FLOAT_EQ(6.32455532f, result);	
}

TEST(TestVector2f, Normalize) {
	lab::math::Vector2f vec1(23.2f, 18.123f);
	vec1.normalize();
	EXPECT_FLOAT_EQ(0.788057331f, vec1.x);
	EXPECT_FLOAT_EQ(0.615601854f, vec1.y);
}

TEST(TestVector2f, GetNormalized) {
	lab::math::Vector2f vec1(23.2f, 18.123f);	
	lab::math::Vector2f result;
	result = vec1.getNormalized();
	EXPECT_FLOAT_EQ(0.788057331f, result.x);
	EXPECT_FLOAT_EQ(0.615601854f, result.y);
}

TEST(TestVector2f, Dot) {
	lab::math::Vector2f vec1(23.2f, 18.123f);
	lab::math::Vector2f vec2(314.2f, 41.0f);
	float result;
	result = vec1.dot(vec2);
	EXPECT_FLOAT_EQ(8032.483f, result);
}

TEST(TestVector2f, Lerp) {
	lab::math::Vector2f vec1(4.3f, 7.5f);
	lab::math::Vector2f vec2(1.12f, 3.98f);
	vec1.lerp(vec2, 0.81f);
	EXPECT_FLOAT_EQ(1.7242f, vec1.x);
	EXPECT_FLOAT_EQ(4.6487999f, vec1.y);
}

TEST(TestVector2f, CopyTo) {
	lab::math::Vector2f vec1(-1.0f, 0.0f);
	lab::math::Vector2f result;
	vec1.copyTo(result);
	EXPECT_FLOAT_EQ(vec1.x, result.x);
	EXPECT_FLOAT_EQ(vec1.y, result.y);
}
