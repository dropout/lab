Lab project
===========

I am experimenting with GPU rendering and UI development in modern C++ and OpenGL.

Requirements:
-------------

- CMake for cross platform project generation.
- GLEW and OpenGL libraries installed, and available.

Usage:
------

To quickly to see what this is about run these scripts and inspect the `/bin` folder.

1. `setup.sh` for automatically generate the project
2. `build.sh` for automatically build the project

Known issues:
-------------

- Ninja generator doesn't build the `lab` target as a dependency for `examples` and `test` targets. One has to explicitly build the `lab` target and then can use `all` or any other targets. Does not happen with generator Unix Makefile. [Ticket](http://www.cmake.org/Bug/view.php?id=14553 "CMake project ticket")