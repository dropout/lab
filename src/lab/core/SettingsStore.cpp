#include <cstring>
#include <iostream>
#include <sstream>
#include <algorithm>
#include "lab/core/SettingsStore.hpp"

using namespace lab::core;

void SettingsStore::load (const SettingsMap& values) {
	this->m_values = values;
}

unsigned int SettingsStore::size () const {
    return this->m_values.size();
}

std::vector<std::string> SettingsStore::keys () {
    std::vector<std::string> result;
    for (std::map<std::string, std::string>::iterator it = this->m_values.begin(); it != this->m_values.end(); ++it) {
        result.push_back(it->first);
    }
    return result;
}

std::string SettingsStore::safeGet (const std::string& key) {
    SettingsMap::iterator result = this->m_values.find(key);
    if (result == this->m_values.end()) {
        throw std::invalid_argument("Unable to get value for the given key: " + key);
    } else {
        return result->second;
    }
};

int SettingsStore::getInt (const std::string& key) {
    std::string v = this->safeGet(key);
    if (v.compare("true") == 0) {
        return true;
    } else {
        return atoi(v.c_str());
    }
}

float SettingsStore::getFloat (const std::string& key) {
    return atof(this->safeGet(key).c_str());
}

double SettingsStore::getDouble (const std::string& key) {
    return std::strtod(this->safeGet(key).c_str(), nullptr);
}

std::string SettingsStore::getString (const std::string& key) {
    return this->safeGet(key);
}

bool SettingsStore::getBool (const std::string& key) {
    bool result;
    std::string v = this->safeGet(key);
    std::transform(v.begin(), v.end(), v.begin(), ::tolower);
    result = !v.empty() && (v.compare("true") == 0 || atoi(v.c_str ()) != 0);
    return result;
}

void SettingsStore::set (const std::string& key, const int& val) {
	this->m_values[key] = std::to_string(val);
}

void SettingsStore::set (const std::string& key, const float& val) {
	this->m_values[key] = std::to_string(val);
}

void SettingsStore::set (const std::string& key, const double& val) {
	this->m_values[key] = std::to_string(val);
}

void SettingsStore::set (const std::string& key, const char* val) {
    std::string strVal(val, std::strlen(val));
    this->m_values[key] = strVal;
}

void SettingsStore::set (const std::string& key, const std::string& val) {
	this->m_values[key] = val;
}

void SettingsStore::set (const std::string& key, const bool& val) {
    std::ostringstream ss;
    ss << std::boolalpha << val;
    this->m_values[key] = ss.str();
}
