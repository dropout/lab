#include <algorithm>
#include <chrono>
#include <thread>
#include <memory>
#include <macrologger.h>
#include <lab/core/Application.hpp>
#include <stdlib.h>

using namespace lab::core;

Application::Application (std::unique_ptr<Window> window) : m_window(std::move(window)) {
    Window* windowPtr = m_window.get();
    windowPtr->keyboardButtonSignal.Connect(this, &Application::onKeyboardInput);
    windowPtr->mouseButtonSignal.Connect(this, &Application::onMouseButtonInput);
    windowPtr->mouseMoveSignal.Connect(this, &Application::onMouseMoveInput);
    windowPtr->mouseScrollSignal.Connect(this, &Application::onMouseScrollInput);
};

/*
 *  Main loop needs improvement when sleepTime goes negative:
 *
 *  http://gameprogrammingpatterns.com/game-loop.html
 *  http://gafferongames.com/game-physics/fix-your-timestep/
 *  http://www.koonsolo.com/news/dewitters-gameloop/
 *
 **/
int Application::run () {
	this->setup();

    Window* windowPtr = this->m_window.get();

    int frameRateLimit = 60;
    double currentTime = windowPtr->getTime();
	double deltaTime = 1.0 / frameRateLimit;
    double newTime = windowPtr->getTime();
	double sleepTime = 0.0;
    while (windowPtr->getWindowCloseFlag() == false) {
        currentTime = windowPtr->getTime();
        windowPtr->pollEvents();
		this->update(currentTime);
        this->render();
        windowPtr->swapBuffers();
        newTime = windowPtr->getTime();
		sleepTime = deltaTime - (newTime - currentTime);
		if (sleepTime > 0.0) {
			std::this_thread::sleep_for(std::chrono::duration<double>(sleepTime));
		} else {
            //LOG_DEBUG("Main loop ran out of desired time interval with %f", sleepTime * -1.0);
		}
	}

    this->teardown();
    return EXIT_SUCCESS;
}
