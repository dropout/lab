#include <lab/core/impl/GLFWWindow.hpp>

using namespace lab::core;

GLFWWindow::GLFWWindow () {
    this->init();
    this->create(1024, 768, "Default title", 60, 4, 4, 1);
}

GLFWWindow::GLFWWindow (
    const int& width,
    const int& height,
    const std::string& title,
    const int& refreshRate,
    const int& multiSampling,
    const int& oglMajor,
    const int& oglMinor
) {
    this->init();
    this->create(
        width,
        height,
        title,
        refreshRate,
        multiSampling,
        oglMajor,
        oglMinor
    );
}

void GLFWWindow::init () {
    GLboolean isSuccessful = glfwInit();
    if (isSuccessful == GL_FALSE) {
		throw WindowException("Unable to initialize GLFW");
	}
}

void GLFWWindow::create (
    const int& width,
    const int& height,
    const std::string& title,
    const int& refreshRate,
    const int& multiSampling,
    const int& oglMajor,
    const int& oglMinor
) {

    // Force defaults
    glfwDefaultWindowHints();

    // OpenGL configuration
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, oglMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, oglMinor);

    // Display configuration
    glfwWindowHint(GLFW_REFRESH_RATE, refreshRate);
    glfwWindowHint(GLFW_SAMPLES, multiSampling);

    // create the window
    GLFWwindow* windowPtr = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (windowPtr == nullptr) {
        throw WindowException("Unable to create window");
    }
    glfwSwapInterval(1);

    // make the application own the window pointer
    this->m_windowPtr = std::unique_ptr<GLFWwindow, GLFWwindowDeleter>(windowPtr);

    // set user pointer in glfw
	glfwSetWindowUserPointer(this->m_windowPtr.get(), this);
	glfwMakeContextCurrent(this->m_windowPtr.get());			

    // Initialize glew library
    glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
  		std::string errorMessage(reinterpret_cast<const char *>(glewGetErrorString(err)));
  		throw WindowException("Unable to initialize GLEW: " + errorMessage);
	}

    // setup input
    glfwSetKeyCallback(this->m_windowPtr.get(), GLFWWindow::KeyboardButtonCallback);
	glfwSetMouseButtonCallback(this->m_windowPtr.get(), GLFWWindow::MouseButtonCallback);
	glfwSetCursorPosCallback(this->m_windowPtr.get(), GLFWWindow::MouseMoveCallback);
	glfwSetScrollCallback(this->m_windowPtr.get(), GLFWWindow::MouseScrollCallback);

    LOG_DEBUG("OpenGL version -> %s", glGetString(GL_VERSION));
}

int GLFWWindow::getWidth () const {
    int result;
    glfwGetFramebufferSize(this->m_windowPtr.get(), &result, nullptr);
    return result;
}

int GLFWWindow::getHeight () const {
    int result;
    glfwGetFramebufferSize(this->m_windowPtr.get(), nullptr, &result);
    return result;
}

double GLFWWindow::getTime () const {
    return glfwGetTime();
}

bool GLFWWindow::getWindowCloseFlag () const {
	return (glfwWindowShouldClose(this->m_windowPtr.get()) != 0);
}

void GLFWWindow::pollEvents () {
    glfwPollEvents();
}

void GLFWWindow::swapBuffers () {
    glfwSwapBuffers(this->m_windowPtr.get());
}
