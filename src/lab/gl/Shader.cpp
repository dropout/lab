#include <lab/gl/Shader.hpp>

using namespace lab::gl;

Shader::operator GLuint () const {
    return this->m_glObjectHandle.get()->getObjectName();
}

bool Shader::isCompiled() const {
    return this->m_isCompiled;
}

void Shader::compile (const std::string& shaderSource) {

    // If already compiled free previous resource
    if (this->m_isCompiled) {
        this->m_glObjectHandle.reset();
    }

    // Initialize resource
    ShaderObject* shaderObject = new ShaderObject(this->getShaderType());
    GLuint shaderId = shaderObject->getObjectName();

    // Compile shader code
    const char* code = shaderSource.c_str();
    api::ShaderSource(shaderId, 1, &code, NULL);
    api::CompileShader(shaderId);

    // Get compile status
    GLint compileStatus;
    api::GetShaderiv(shaderId, GL_COMPILE_STATUS, &compileStatus);

    if (compileStatus == GL_FALSE) { // Throw error
        std::string errorMessage(this->getInfoLog(shaderId));
        delete shaderObject;
        throw CompileException(errorMessage);
    } else { // Register ownership
        this->m_isCompiled = true;
        this->m_glObjectHandle = std::unique_ptr<ShaderObject>(shaderObject);
    }
}

std::string Shader::getInfoLog () const {
    if (this->m_isCompiled) {
        return this->getInfoLog(this->m_glObjectHandle.get()->getObjectName());
    } else {
        return "";
    }
}

std::string Shader::getInfoLog (const GLint& shaderId) const {
    GLint infoLogLength;
    api::GetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0) {
        std::string infoLog(infoLogLength, 0);
        api::GetShaderInfoLog(shaderId, infoLogLength, &infoLogLength, &infoLog[0] );
        return infoLog;
    } else {
        return "";
    }
}
