#include <lab/gl/Program.hpp>

using namespace lab::gl;

Program::operator GLuint () const {
    return this->m_glObjectHandle.get()->getObjectName();
}

GLint Program::getAttributeLocation (const std::string& name) {
    return glGetAttribLocation(this->m_glObjectHandle.get()->getObjectName(), name.c_str());
}

GLint Program::getUniformLocation (const std::string& name) {
    LocationMap::const_iterator uniformIt = this->m_uniformLocations.find(name);
    if (uniformIt == m_uniformLocations.end()) {
        GLint loc = glGetUniformLocation(this->m_glObjectHandle.get()->getObjectName(), name.c_str());
        this->m_uniformLocations[name] = loc;
        return loc;
    } else {
        return uniformIt->second;
    }
}

void Program::uniform (const std::string &name, const int &value) {
    GLuint loc = this->getUniformLocation(name);
    api::Uniform1i(loc, value);
}

void Program::uniform (const std::string &name, const float &value) {
    GLuint loc = this->getUniformLocation(name);
    api::Uniform1f(loc, value);
}

void Program::link (const VertexShader& vertexShader, const FragmentShader& fragmentShader) {

    ProgramObject* program = new ProgramObject();
    GLint programId = program->getObjectName();

    api::AttachShader(programId, vertexShader);
    api::AttachShader(programId, fragmentShader);
    api::LinkProgram(programId);

    GLint linkStatus;
    api::GetProgramiv(programId, GL_LINK_STATUS, &linkStatus);

    if (linkStatus == GL_FALSE) {
        std::string errorMessage(this->getInfoLog(programId));
        delete program;
        throw LinkException(errorMessage);
    } else {
        this->m_isLinked = true;
        this->m_glObjectHandle = std::unique_ptr<ProgramObject>(program);
    }

}

std::string Program::getInfoLog () const {
    if (this->m_isLinked) {
        return this->getInfoLog(this->m_glObjectHandle.get()->getObjectName());
    } else {
        return "";
    }
}

std::string Program::getInfoLog (const GLint& programId) const {
    GLint infoLogLength;
    api::GetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0) {
        std::string infoLog(infoLogLength, 0);
        api::GetProgramInfoLog(programId, infoLogLength, &infoLogLength, &infoLog[0]);
        return infoLog;
    } else {
        return "";
    }
}
