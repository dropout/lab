#include <lab/gl/VertexArray.hpp>

using namespace lab::gl;

VertexArray::VertexArray () : m_isInitialized(false) {}

void VertexArray::initialize () {
    if (this->m_isInitialized == false) {
        this->m_glObjectHandle = std::unique_ptr<VertexArrayObject>(new VertexArrayObject());
        this->m_isInitialized = true;
    }
}

bool VertexArray::isInitialized () {
    return this->m_isInitialized;
}

void VertexArray::bindAttribute (const GLint& attrLoc, const Buffer& buffer, lab::gl::Type::T type, GLuint count, GLuint stride, std::intptr_t offset) {
    if (m_isInitialized == false) {
        this->initialize();
    }
    api::BindVertexArray(this->m_glObjectHandle.get()->getObjectName());
    api::BindBuffer(GL_ARRAY_BUFFER, buffer);
    api::EnableVertexAttribArray(attrLoc);
    api::VertexAttribPointer(attrLoc, count, type, GL_FALSE, stride, (const GLvoid*) offset);
}

void VertexArray::bindAttribute (const GLint& attrLoc, const Buffer& buffer, Type::T type, GLuint count) {
    this->bindAttribute(attrLoc, buffer, Type::Float, 2, 0, 0);
}


VertexArray::~VertexArray () {}
