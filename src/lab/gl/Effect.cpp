#include "lab/gl/Effect.hpp"

namespace lab { namespace gl {

Effect::Effect () {}

Effect::Effect (std::string& vs, std::string& fs) {
	this->load(vs, fs);
}

oglplus::Program& Effect::getProg () {
    return this->m_program;
}

void Effect::load (std::string& vsEffectKey, std::string& fsEffectKey) {
	const char* vs = glswGetShader(vsEffectKey.c_str());
	const char* fs = glswGetShader(fsEffectKey.c_str());

	this->m_vertexShader.Source(vs).Compile();
	this->m_fragmentShader.Source(fs).Compile();

	this->m_program.AttachShader(m_vertexShader);
	this->m_program.AttachShader(m_fragmentShader);

	this->m_program.Link();
}

}}
