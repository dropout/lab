#include <fstream>
#include <sstream>
#include <lab/io/FileLoader.hpp>
#include <lab/io/FileNotAccessibleException.hpp>

namespace lab { namespace io {

std::string FileLoader::load (const std::string& path) {
	std::ifstream fileStream(path);
	if (fileStream.good() == false) {
		throw FileNotAccessibleException(path);
	}
	std::stringstream buffer;
	buffer << fileStream.rdbuf();
	return buffer.str();
}

}}
