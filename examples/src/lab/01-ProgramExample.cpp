#include "examples.hpp"
#include "lab/gl.hpp"

namespace lab { namespace examples {

class ProgramExample : public lab::core::Application {

    public:
        using lab::core::Application::Application;

    private:
        lab::gl::Context m_gl;
        lab::gl::Buffer m_vbo;

        
        lab::gl::VertexShader m_vertexShader;
        lab::gl::FragmentShader m_fragmentShader;
        

        lab::gl::Program m_program;
        lab::gl::VertexArray m_vao;

        const char* vertexShaderSource = R"vs(
            #version 410
            in vec2 vertPosition;
            void main (void) {
                gl_Position = vec4(vertPosition, 0.0, 1.0);
            }
        )vs";

        const char* fragmentShaderSource = R"fs(
            #version 410
            out vec4 fragColor;
            void main (void) {
                fragColor = vec4(0.0, 0.82, 0.82, 1.0);
            }
        )fs";

        const GLfloat vertexBufferData[6] = {
             1.0f,  0.0f, // TL
             0.0f,  1.0f, // BR
             0.0f,  0.0f  // BL
        };

        void setup () override;
        void render () override;

};

void ProgramExample::setup () {
    // Set buffer data
    this->m_vbo.setData(sizeof(this->vertexBufferData), this->vertexBufferData, lab::gl::BufferUsage::T::StaticDraw);    

    // Compile shaders
    this->m_vertexShader.compile(this->vertexShaderSource);
    this->m_fragmentShader.compile(this->fragmentShaderSource);

    // Link program
    this->m_program.link(this->m_vertexShader, this->m_fragmentShader);    

    // Setup Vertex Array
    VertexArrayAttribute vaoAttr(
        "vertPosition",
        this->m_program,
        this->m_vbo,
        2,
    );

    this->m_vao.bindAttribute(
        this->m_vbo,
        this->m_program.getAttributeLocation("vertPosition"),
    );

    this->m_vao.enableAttribute(
        this->m_program.getAttributeLocation("vertPosition"),
        2,                     // Items in array
        lab::gl::Type::Float,  // Type of array
        GL_FALSE,              // Normalize flag
        2*sizeof(GLfloat),     // stride
        0                      // Array buffer offset
    );

    // Bind the VAO here for the simplicity of this example
    this->m_program.use();
    this->m_vao.bind();
}

void ProgramExample::render () {
    // clear the frame
    this->m_gl.clear();

    // draw the frame
    this->m_gl.drawArrays(lab::gl::Primitive::Triangles, 0, 3);
}

}}

LAB_EXAMPLE_MAIN(lab::examples::ProgramExample)
