#include <lab/core/Application.hpp>
#include <lab/gl/Context.hpp>
#include <lab/gl/Buffer.hpp>
#include <lab/gl/Shader.hpp>
#include <lab/gl/Program.hpp>
#include <lab/gl/VertexArray.hpp>

namespace lab { namespace examples {

class ShaderProgramExample : public lab::core::Application {

    public:
        using lab::core::Application::Application;

    private:
        lab::graphics::Renderer m_renderer;
        lab::graphics::Camera m_camera;
        lab::graphics::Box m_box;

        void setup () override;
        void update (const double& time) override;
        void render () override;
};

void ShaderProgramExample::setup () {
    this->m_camera.setPosition(1.0f, 1.0f, 0.0f);
    this->m_camera.setTarget(0.0f, 0.0f, 0.0f);
}

void update (const double& time) {
    //this->m_box->setRotation();
}

void ShaderProgramExample::render () {
    this->m_gl.clear();
    this->m_renderer.render(this->m_box, this->m_camera);
}

}}

LAB_APPLICATION_MAIN(lab::examples::ShaderProgramExample)
