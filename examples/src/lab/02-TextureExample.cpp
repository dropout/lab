#include <lab/core/Application.hpp>
#include <lab/gl/Context.hpp>
#include <lab/gl/Buffer.hpp>
#include <lab/gl/Shader.hpp>
#include <lab/gl/Program.hpp>
#include <lab/gl/VertexArray.hpp>

namespace lab { namespace examples {

        class TextureExample : public lab::core::Application {

        public:
            using lab::core::Application::Application;

        private:
            lab::gl::Context m_gl;
            lab::gl::Buffer m_vbo;
            lab::gl::VertexShader m_vertexShader;
            lab::gl::FragmentShader m_fragmentShader;
            lab::gl::Program m_program;
            lab::gl::VertexArray m_vao;
            lab::gl::Texture m_texture;

            const GLfloat vertexBufferData[6] = {
                1.0f,  0.0f, // TL
                0.0f,  1.0f, // BR
                0.0f,  0.0f  // BL
            };

            void setup () override;
            void render () override;

        };

        void TextureExample::setup () {
            // Set buffer data
            this->m_vbo.setData(sizeof(this->vertexBufferData), this->vertexBufferData, lab::gl::BufferUsage::T::StaticDraw);
            this->m_vbo.bind();

            // Compile shaders
            this->m_vertexShader.compile(this->vertexShaderSource);
            this->m_fragmentShader.compile(this->fragmentShaderSource);

            // Link program
            this->m_program.link(this->m_vertexShader, this->m_fragmentShader);
            this->m_program.use();

            // Setup Vertex Array
            this->m_vao.enableAttribute(
                    this->m_program.getAttributeLocation("vertPosition"),
                    2,                  // Items in array
                    lab::gl::Type::Float, // Type of array
                    GL_FALSE,           // Normalize flag
                    2*sizeof(GLfloat),  // stride
                    0                   // Array buffer offset
            );
            this->m_vao.bind();
        }

        void TextureExample::render () {
            // clear the frame
            this->m_gl.clear();

            // draw the frame
            this->m_gl.drawArrays(lab::gl::Primitive::Triangles, 0, 3);
        }

    }}

LAB_MAIN(lab::examples::TextureExample)
