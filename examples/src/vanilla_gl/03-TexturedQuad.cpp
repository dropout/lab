#define STBI_FAILURE_USERMSG

#include "Application.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <macrologger.h>
#include <src/examples.hpp>

namespace lab { namespace examples {

class TexturedQuad : public lab::core::Application {

    public:
        using lab::core::Application::Application;

    protected:

        // A GL program handles
        GLuint quadVBO;
        GLuint quadVAO;
        GLuint quadIBO;

        // Shading
        GLuint vertexShader;
        GLuint fragmentShader;
        GLuint program;
        GLuint texture;

        // Vertex attribute locations
        GLint vertPositionLoc;
        GLint vertTexCoordLoc;

        // Uniform locations
        GLint textureLoc;
        GLint mvpMatrixLoc;

        // Vertex shader source
        const char* vertexShaderCode = R"vs(
            #version 420

            uniform mat4 mvp;
            in vec2 vertPosition;
            in vec2 vertTexCoord;

            out vec2 fragTexCoord;

            void main (void) {
                fragTexCoord = vertTexCoord;
                gl_Position = mvp * vec4(vertPosition, 0.0, 1.0);
            }
        )vs";

        // Fragment shader source
        const char* fragmentShaderCode = R"fs(
            #version 420

            uniform sampler2D tex;
            in vec2 fragTexCoord;

            out vec4 fragColor;

            void main (void) {
                fragColor = texture(tex, fragTexCoord);
            }
        )fs";

        // Quad data in format: XY UV
        // Counter-clockwise ordering
        const GLfloat vertexBufferData[16] = {
             0.5f,  0.5f,  1.0f,  1.0f,   // TR
            -0.5f,  0.5f,  0.0f,  1.0f,   // TL
            -0.5f, -0.5f,  0.0f,  0.0f,   // BL
             0.5f, -0.5f,  1.0f,  0.0f    // BR
        };

        // Index data for drawing quad as triangles
        const GLushort indexBufferData[6] = {
            0, 1, 2,
            0, 2, 3
        };

        // Override methods in super class
        void configure () override;
        void setup () override;
        void display () override;
        void teardown () override;

};

void TexturedQuad::configure () {
    this->m_configuration->set("title", "Textured Quad Example");
}

void TexturedQuad::setup () {

    // Create shader program
    this->program = glCreateProgram();

    // Compile Vertex Shader
    this->vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(this->vertexShader, 1, &this->vertexShaderCode, NULL);
    glCompileShader(this->vertexShader);

    // Compile Fragment Shader
    this->fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(this->fragmentShader, 1, &this->fragmentShaderCode , NULL);
    glCompileShader(this->fragmentShader);

    // Link the shaders to program
    glAttachShader(this->program, this->vertexShader);
    glAttachShader(this->program, this->fragmentShader);
    glBindFragDataLocation(this->program, 0, "fragData");
    glLinkProgram(this->program);

    // Create vertex buffer object
    glGenBuffers(1, &this->quadVBO);
    glBindBuffer(GL_ARRAY_BUFFER, this->quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->vertexBufferData), this->vertexBufferData, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Create vertex array object
    glGenVertexArrays(1, &this->quadVAO);
    glBindVertexArray(this->quadVAO);
    glUseProgram(this->program);

    // Bind quad vbo to be used in vao
    glBindBuffer(GL_ARRAY_BUFFER, this->quadVBO);

    // Get uniform locations, we will set the value for it in display loop
    this->textureLoc = glGetUniformLocation(this->program, "tex");

    /*
        Create orthographic projection matrix. Our quad becomes a square.
        Set mvp value with projection matrix, since model & view are identity matrices
        no need to define and multipy them for now
    */
    this->mvpMatrixLoc = glGetUniformLocation(this->program, "mvp");
    float aspect = (float) this->m_window.get()->getFrameBufferWidth() / this->m_window.get()->getFrameBufferHeight();
    glm::mat4 projection = glm::ortho(
        -1.0f * aspect,
        1.0f * aspect,
        -1.0f,
        1.0f,
        -1.0f,
        1.0f
    );
    glUniformMatrix4fv(this->mvpMatrixLoc, 1, false, glm::value_ptr(projection));

    // Set value for vertPosition shader attribute variable
    this->vertPositionLoc = glGetAttribLocation(this->program, "vertPosition");
    glEnableVertexAttribArray(this->vertPositionLoc);
    glVertexAttribPointer(
        this->vertPositionLoc,
        2,
        GL_FLOAT,
        GL_FALSE,
        4*sizeof(GLfloat),
        0
    );

    // Set value for vertTexCoord shader attribute variable
    this->vertTexCoordLoc = glGetAttribLocation(this->program, "vertTexCoord");
    glEnableVertexAttribArray(this->vertTexCoordLoc);
    glVertexAttribPointer(
        this->vertTexCoordLoc,
        2,
        GL_FLOAT,
        GL_FALSE,
        4*sizeof(GLfloat),
        (const GLvoid*)(2*sizeof(GLfloat))
    );

    // Load bitmap data from image file and data vertically
    int width, height, channels;
    unsigned char* pixels = lab::examples::LoadBitmapFromFile("assets/images/uvgrid04.png", width, height, channels);
    lab::examples::FlipBitmapVertically(pixels, channels, width, height);

    // Configure texture object
    glGenTextures(1, &this->texture);
    glBindTexture(GL_TEXTURE_2D, this->texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGB8,
        width,
        height,
        0,
        GL_RGB,
        GL_UNSIGNED_BYTE,
        pixels
    );

    // Unbind object
    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Create index buffer object
    glGenBuffers(1, &this->quadIBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->quadIBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(this->indexBufferData), this->indexBufferData, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Pixel pointer data is no longer needed
    delete pixels;
}

void TexturedQuad::display () {

    // Clear
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    // Use the shader program
    glUseProgram(this->program);

    // Bind the texture to be used
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->texture);

    // Set texture sampler uniform
    glUniform1i(this->textureLoc, 0);

    // Bind vertex array
    glBindVertexArray(this->quadVAO);

    // Bind index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->quadIBO);

    // Draw
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

    // Reset state
    glUseProgram(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Swap the buffers
    this->m_window.get()->swapBuffers();

}

void TexturedQuad::teardown () {
    glDeleteBuffers(1, &this->quadVBO);
    glDeleteVertexArrays(1, &this->quadVAO);
    glDeleteBuffers(1, &this->quadIBO);
    glDeleteTextures(1, &this->texture);
    glDeleteShader(this->vertexShader);
    glDeleteShader(this->fragmentShader);
    glDeleteProgram(this->program);
}

}}

LAB_APPLICATION_MAIN(lab::examples::TexturedQuad)
