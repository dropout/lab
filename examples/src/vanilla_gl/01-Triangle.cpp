#include "Application.hpp"

namespace lab { namespace examples {

class Triangle : public lab::core::Application {

	public:
        // using parent class constructor
		using lab::core::Application::Application;

    protected:

        // A GL object handles
        GLuint triangleVBO;
        GLuint triangleVAO;

        // Shader handlers
        GLuint vertexShader;
        GLuint fragmentShader;
        GLuint program;
    	
        // Vertex shader source
        const char* vertexShaderSource = R"vs(
			#version 420                
            in vec2 vertPosition;
            void main (void) {
                gl_Position = vec4(vertPosition, 0.0, 1.0);
			}
		)vs";

        // Fragment shader source
        const char* fragmentShaderSource = R"fs(
			#version 420
            out vec4 fragColor;
            void main (void) {
                fragColor = vec4(0.0, 0.82, 0.82, 1.0);
			}
		)fs";

        // Triangle shape data
        const GLfloat vertexBufferData[6] = {
             1.0f,  0.0f, // TL
             0.0f,  1.0f, // BR
             0.0f,  0.0f  // BL
    	};

    	// Override methods in super class
        void configure () override;
        void setup () override;
        void display () override;
        void teardown () override;
        
};

void Triangle::configure () {
    this->m_configuration->set("title", "Triangle Example");
}

void Triangle::setup () {

    // Create shader program
    this->program = glCreateProgram();

    // Compile Vertex Shader
    this->vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(this->vertexShader, 1, &this->vertexShaderSource, NULL);
    glCompileShader(this->vertexShader);

    // Compile Fragment Shader
    this->fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(this->fragmentShader, 1, &this->fragmentShaderSource , NULL);
    glCompileShader(this->fragmentShader);

    // Link the shaders to program
    glAttachShader(this->program, this->vertexShader);
    glAttachShader(this->program, this->fragmentShader);
    glBindFragDataLocation(this->program, 0, "fragData");
    glLinkProgram(this->program);

    // Create vertex buffer object
    glGenBuffers(1, &this->triangleVBO);
    glBindBuffer(GL_ARRAY_BUFFER, this->triangleVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(this->vertexBufferData), this->vertexBufferData, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // good practice?

    // Create vertex array object
    glGenVertexArrays(1, &this->triangleVAO);
    glBindVertexArray(this->triangleVAO);
    glUseProgram(this->program);

    // Bind the VBO to be used
    glBindBuffer(GL_ARRAY_BUFFER, this->triangleVBO);

    // Set the vertex attribute for the triangle shape
    GLint vertPositionLoc = glGetAttribLocation(this->program, "vertPosition");
    glEnableVertexAttribArray(vertPositionLoc);
    glVertexAttribPointer(
        vertPositionLoc,		// Location of vertex attribute in shader
        2,                      // Items in array
        GL_FLOAT,               // Type of array
        GL_FALSE,               // Normalize flag
        2*sizeof(GLfloat),      // stride can be zero if data is continous, now its two floats means two times four bytes
        0                       // Array buffer offset
    );

    // Unbind objects
    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Triangle::display () {
	// Clear screen
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Draw triangle
    glUseProgram(this->program);
    glBindVertexArray(this->triangleVAO);
	glDrawArrays(GL_TRIANGLES, 0, 3);

    // Reset state
    glBindVertexArray(0);
    glUseProgram(0);

	// Swap the buffers
    this->swapBuffers();
}

void Triangle::teardown () {
    glDeleteBuffers(1, &this->triangleVBO);
    glDeleteVertexArrays(1, &this->triangleVAO);
    glDeleteShader(this->vertexShader);
    glDeleteShader(this->fragmentShader);
    glDeleteProgram(this->program);
}

}}

// Macro is used for entry point
LAB_APPLICATION_MAIN(lab::examples::Triangle)
