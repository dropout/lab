#include "Application.hpp"
#include <macrologger.h>

namespace lab { namespace examples {

class Quad : public lab::core::Application {

	public:
		using lab::core::Application::Application;

    protected:

        // A GL program handles
        GLuint quadVBO;
        GLuint quadVAO;
        GLuint quadIBO;

        // Shader handlers
        GLuint vertexShader;
        GLuint fragmentShader;
        GLuint program;

        // Vertex shader source
        const char* vertexShaderCode = R"vs(
            #version 420
            in vec2 vertPosition;
            void main (void) {
                gl_Position = vec4(vertPosition, 0.0, 1.0);
            }
        )vs";

        // Fragment shader source
        const char* fragmentShaderCode = R"fs(
            #version 420
            out vec4 fragData;
            void main (void) {
                fragData = vec4(1.0, 1.0, 0.0, 1.0);
            }
        )fs";

        // Quad data
        const GLfloat vertexBufferData[8] = {
             0.5f,	 0.5f, // TR
            -0.5f,	 0.5f, // TL
            -0.5f,	-0.5f, // BL
             0.5f,	-0.5f  // BR
        };

        // Index data
        const GLushort indexBufferData[6] = {
            0, 1, 2,
            0, 2, 3
        };

        // Override methods in super class
        void configure () override;
        void setup () override;
        void display () override;
        void teardown () override;

};

void Quad::configure () {
    this->m_configuration->set("title", "Quad Example");
}

void Quad::setup () {

    // Create shader program
    this->program = glCreateProgram();

    // Compile Vertex Shader
    this->vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(this->vertexShader, 1, &this->vertexShaderCode, NULL);
    glCompileShader(this->vertexShader);

    // Compile Fragment Shader
    this->fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(this->fragmentShader, 1, &this->fragmentShaderCode , NULL);
    glCompileShader(this->fragmentShader);

    // Link the shaders to program
    glAttachShader(this->program, this->vertexShader);
    glAttachShader(this->program, this->fragmentShader);
    glBindFragDataLocation(this->program, 0, "fragData");
    glLinkProgram(this->program);

    // Create vertex buffer object
    glGenBuffers(1, &this->quadVBO);
    glBindBuffer(GL_ARRAY_BUFFER, this->quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->vertexBufferData), this->vertexBufferData, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Create vertex array object
    glGenVertexArrays(1, &this->quadVAO);
    glBindVertexArray(this->quadVAO);

    // Bind vertex buffer data to be used
    glBindBuffer(GL_ARRAY_BUFFER, this->quadVBO);

    // With our program...
    glUseProgram(this->program);

    // ... set the vertex attribute to vertices in buffer object
    GLint vertPositionLoc = glGetAttribLocation(this->program, "vertPosition");
    glEnableVertexAttribArray(vertPositionLoc);

    glVertexAttribPointer(
        vertPositionLoc,    // Pointer location
        2,                  // Items in array
        GL_FLOAT,           // Type of array
        GL_FALSE,           // Normalize flag
        2*sizeof(GLfloat),  // stride
        0                   // Array buffer offset
    );

    // Unbind objects
    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Create the index buffer object
    glGenBuffers(1, &this->quadIBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->quadIBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(this->indexBufferData), this->indexBufferData, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void Quad::display () {

    // Clear display
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw quad
    glUseProgram(this->program);
    glBindVertexArray(this->quadVAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->quadIBO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

    // Reset state
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    // Swap buffers
    this->m_window.get()->swapBuffers();
}

void Quad::teardown () {
    glDeleteBuffers(1, &this->quadVBO);
    glDeleteVertexArrays(1, &this->quadVAO);
    glDeleteBuffers(1, &this->quadIBO);
    glDeleteShader(this->vertexShader);
    glDeleteShader(this->fragmentShader);
    glDeleteProgram(this->program);
}

}}

LAB_APPLICATION_MAIN(lab::examples::Quad)
