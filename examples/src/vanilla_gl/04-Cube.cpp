#include "Application.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "macrologger.h"

namespace lab { namespace examples {

class Cube : public lab::core::Application {

    public:
        using lab::core::Application::Application;

    protected:

        // Buffer object handles
        GLuint cubeVBO;
        GLuint cubeVAO;
        GLuint cubeIBO;

        // Shader handlers
        GLuint vertexShader;
        GLuint fragmentShader;
        GLuint program;

        // Vertex attribute locations
        GLint vertPositionLoc;
        GLint vertColorLoc;

        // Matrices for rendering
        glm::mat4 projectionMat;
        GLint projectionMatLoc;
        glm::mat4 viewMat;
        GLint viewMatLoc;
        glm::mat4 modelMat;
        GLint modelMatLoc;

        // Vertex shader source
        const char* vertexShaderCode = R"vs(
            #version 420

            uniform mat4 projection;
            uniform mat4 view;
            uniform mat4 model;

            in vec4 vertPosition;
            in vec4 vertColor;

            out vec4 fragColor;

            void main (void)    {
                fragColor = vertColor;
                gl_Position = projection * view * model * vertPosition;
            }
        )vs";

        // Fragment shader source
        const char* fragmentShaderCode = R"fs(
            #version 420
            in vec4 fragColor;
            out vec4 fragData;

            void main (void)    {
                fragData = fragColor;
            }
        )fs";

        /*
            This is the cube we are going to draw.
            Format: XYZW RGBA

              G----H
             /|   /|
            B----A |
            | F--|-E
            |/   |/
            C----D

        */
        const GLfloat vertexBufferData[192] = {

            // Front ABCD
            0.5f,  0.5f,  0.5f,  1.0f,  1.0f,  1.0f,  0.0f,  1.0f,
           -0.5f,  0.5f,  0.5f,  1.0f,  1.0f,  1.0f,  0.0f,  1.0f,
           -0.5f, -0.5f,  0.5f,  1.0f,  1.0f,  1.0f,  0.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  1.0f,  1.0f,  0.0f,  1.0f,

            // Back GHEF
           -0.5f,  0.5f, -0.5f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f,
            0.5f,  0.5f, -0.5f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f,
           -0.5f, -0.5f, -0.5f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f,

            // Left BGFC
           -0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  1.0f,  1.0f,  1.0f,
           -0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  1.0f,  1.0f,  1.0f,
           -0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  1.0f,  1.0f,  1.0f,
           -0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  1.0f,  1.0f,  1.0f,

            // Right HADE
            0.5f,  0.5f, -0.5f,  1.0f,  1.0f,  0.0f,  1.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  1.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  1.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  1.0f,  0.0f,  1.0f,  1.0f,

            // Top HGBA
            0.5f,  0.5f, -0.5f,  1.0f,  0.5f,  0.5f,  1.0f,  1.0f,
           -0.5f,  0.5f, -0.5f,  1.0f,  0.5f,  0.5f,  1.0f,  1.0f,
           -0.5f,  0.5f,  0.5f,  1.0f,  0.5f,  0.5f,  1.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  0.5f,  0.5f,  1.0f,  1.0f,

            // Bottom FEDC
            0.5f, -0.5f, -0.5f,  1.0f,  1.0f,  0.2f,  0.2f, 1.0f,
           -0.5f, -0.5f, -0.5f,  1.0f,  1.0f,  0.2f,  0.2f, 1.0f,
           -0.5f, -0.5f,  0.5f,  1.0f,  1.0f,  0.2f,  0.2f, 1.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  1.0f,  0.2f,  0.2f, 1.0f

        };


        // Index data for that will draw cube vertices with triangles
        const GLushort indexBufferData[36] = {

            // front
            0, 1, 2,
            0, 2, 3,

            // back
            4, 5, 6,
            4, 6, 7,

            // left
            8, 9, 10,
            8, 10, 11,

            // right
            12, 13, 14,
            12, 14, 15,

            // top
            16, 17, 18,
            16, 18, 19,

            // bottom
            20, 21, 22,
            20, 22, 23

        };

        // Override methods in super class
        void setup () override;
        void display () override;
        void teardown () override;

};

void Cube::setup () {

    // Create shader program
    this->program = glCreateProgram();

    // Vertex Shader
    this->vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(this->vertexShader, 1, &this->vertexShaderCode, NULL);
    glCompileShader(this->vertexShader);

    // Fragment Shader
    this->fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(this->fragmentShader, 1, &this->fragmentShaderCode , NULL);
    glCompileShader(this->fragmentShader);

    // Program
    glAttachShader(this->program, this->vertexShader);
    glAttachShader(this->program, this->fragmentShader);
    glBindFragDataLocation(this->program, 0, "fragData");
    glLinkProgram(this->program);

    // VBO
    glGenBuffers(1, &this->cubeVBO);
    glBindBuffer(GL_ARRAY_BUFFER, this->cubeVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->vertexBufferData), this->vertexBufferData, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // VAO
    glGenVertexArrays(1, &this->cubeVAO);
    glBindVertexArray(this->cubeVAO);
    glUseProgram(this->program);
    glBindBuffer(GL_ARRAY_BUFFER, this->cubeVBO);

    // View matrix uniform
    this->viewMatLoc = glGetUniformLocation(this->program, "view");
    viewMat = glm::lookAt(
        glm::vec3(5.0, -2.0, 5.0),  // location of viewer
        glm::vec3(0.0, 0.0, 0.0),   // point looking at
        glm::vec3(0.0, 1.0, 0.0)    // upvector
    );
    glUniformMatrix4fv(viewMatLoc, 1, false, glm::value_ptr(viewMat));

    // Projection matrix uniform
    this->projectionMatLoc = glGetUniformLocation(this->program, "projection");
    float aspect = (float) this->m_window.get()->getFrameBufferWidth() / this->m_window.get()->getFrameBufferHeight();
    projectionMat = glm::perspective(45.0f, aspect, 0.1f, -10.0f);
    glUniformMatrix4fv(projectionMatLoc, 1, false, glm::value_ptr(projectionMat));

    // Model matrix uniform
    this->modelMatLoc = glGetUniformLocation(this->program, "model");
    modelMat = glm::mat4();
    glUniformMatrix4fv(modelMatLoc, 1, false, glm::value_ptr(modelMat));    

    // Vertex position attribute
    this->vertPositionLoc = glGetAttribLocation(this->program, "vertPosition");
    glEnableVertexAttribArray(this->vertPositionLoc);
    glVertexAttribPointer(
        this->vertPositionLoc,
        4,
        GL_FLOAT,
        GL_FALSE,
        8*sizeof(GLfloat),
        NULL
    );

    // Vertex color attribute
    this->vertColorLoc = glGetAttribLocation(this->program, "vertColor");
    glEnableVertexAttribArray(this->vertColorLoc);
    glVertexAttribPointer(
        this->vertColorLoc,
        4,
        GL_FLOAT,
        GL_FALSE,
        8*sizeof(GLfloat),
        (const GLvoid*)(4*sizeof(GLfloat))
    );

    // Unbind objects
    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Create index buffer object
    glGenBuffers(1, &this->cubeIBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeIBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(this->indexBufferData), this->indexBufferData, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void Cube::display () {

    // Clear screen
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Draw cube
    glEnable(GL_DEPTH_TEST);
    glUseProgram(this->program);
    glBindVertexArray(this->cubeVAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeIBO);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, nullptr);

    // Reset state
    glDisable(GL_DEPTH_TEST);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    // Swap buffers
    this->m_window.get()->swapBuffers();
}

void Cube::teardown () {
    glDeleteBuffers(1, &this->cubeVBO);
    glDeleteVertexArrays(1, &this->cubeVAO);
    glDeleteBuffers(1, &this->cubeIBO);
    glDeleteShader(this->vertexShader);
    glDeleteShader(this->fragmentShader);
    glDeleteProgram(this->program);
}

}}

LAB_APPLICATION_MAIN(lab::examples::Cube)
