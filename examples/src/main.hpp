#ifndef LAB_EXAMPLES_MAIN_HPP
#define LAB_EXAMPLES_MAIN_HPP

#include <memory>
#include "lab/core/Application.hpp"
#include "lab/core/impl/GLFWWindow.hpp"

#define LAB_EXAMPLE_MAIN(className)                                                 \
    int main(int argc, char** argv) {                                                   \
        std::unique_ptr<lab::core::Window> window(new lab::core::GLFWWindow());         \
        className app(std::move(window));                                               \
        return app.run();                                                               \
    }                                                                                   \

#endif // LAB_EXAMPLES_MAIN_HPP
