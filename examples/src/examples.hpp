#ifndef LAB_EXAMPLES_EXAMPLES_HPP
#define LAB_EXAMPLES_EXAMPLES_HPP

#define STB_IMAGE_IMPLEMENTATION
#include <main.hpp>
#include <stb/stb_image.h>

namespace lab { namespace examples {

    static inline bool IsShaderCompileSuccessful (const GLuint& shaderHandler) {
        GLint status;
        glGetShaderiv(shaderHandler, GL_COMPILE_STATUS, &status);
        if(status == GL_FALSE)
        {
            GLint length;
            glGetShaderiv(shaderHandler, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> log(length);
            glGetShaderInfoLog(shaderHandler, length, &length, &log[0]);
            std::cerr << &log[0];
            return false;
        }
        return true;
    }

    static inline bool IsProgramLinkSuccessful (const GLuint& programHandle) {
        GLint status;
        glGetProgramiv(programHandle, GL_LINK_STATUS, &status);
        if(status == GL_FALSE)
        {
            GLint length;
            glGetProgramiv(programHandle, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> log(length);
            glGetProgramInfoLog(programHandle, length, &length, &log[0]);
            std::cerr << &log[0];
            return false;
        }
        return true;
    }

    static inline unsigned GetPixelOffset(unsigned col, unsigned row, unsigned width, unsigned height, int format) {
        return (row*width + col)*format;
    }

    static unsigned char* LoadBitmapFromFile (const std::string& filePath, int& width, int& height, int& channels) {

       unsigned char* pixels = stbi_load(filePath.c_str(), &width, &height, &channels, 0);
        if (!pixels) {
            throw std::runtime_error(stbi_failure_reason());
        } else {
            return pixels;
        }
    }

    static inline void FlipBitmapVertically (unsigned char* pixels, int format, int width, int height) {
        unsigned long rowSize = format*width;
        unsigned char* rowBuffer = new unsigned char[rowSize];
        unsigned halfRows = height / 2;

        for (unsigned rowIdx = 0; rowIdx < halfRows; ++rowIdx){
            unsigned char* row = pixels + lab::examples::GetPixelOffset(0, rowIdx, width, height, format);
            unsigned char* oppositeRow = pixels + GetPixelOffset(0, height - rowIdx - 1, width, height, format);

            memcpy(rowBuffer, row, rowSize);
            memcpy(row, oppositeRow, rowSize);
            memcpy(oppositeRow, rowBuffer, rowSize);
        }

        delete rowBuffer;
    }

}}

#endif
