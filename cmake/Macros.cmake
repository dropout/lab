# set option helper
macro(lab_option var default type docstring)
	if(NOT DEFINED ${var})
		set(${var} ${default})
	endif()
	set(${var} ${${var}} CACHE ${type} ${docstring} FORCE)
endmacro()

# print variable value
macro(lab_print_var _variableName)
	message(STATUS "${_variableName}=${${_variableName}}")
endmacro()

# copy file if changed
MACRO(lab_copy_file_if_changed in_file out_file target)
    IF(${in_file} IS_NEWER_THAN ${out_file})
    #    message("COpying file: ${in_file} to: ${out_file}")
        ADD_CUSTOM_COMMAND (
            TARGET     ${target}
            POST_BUILD
            COMMAND    ${CMAKE_COMMAND}
            ARGS       -E copy ${in_file} ${out_file}
        )
    ENDIF(${in_file} IS_NEWER_THAN ${out_file})
ENDMACRO(lab_copy_file_if_changed)

# copy file into directory if changed
MACRO(lab_copy_file_into_directory_if_changed target in_file out_dir )
    GET_FILENAME_COMPONENT(file_name ${in_file} NAME)
    lab_copy_file_if_changed(${in_file} ${out_dir}/${file_name}
${target})
ENDMACRO(lab_copy_file_into_directory_if_changed)

# copy all files from file list to output directory
# sub-trees are ignored, flat hiearchy created
MACRO(lab_copy_files_into_directory_if_changed in_file_list out_dir target)
    FOREACH(in_file ${in_file_list})
        lab_copy_file_into_directory_if_changed(${in_file}
${out_dir} ${target})
    ENDFOREACH(in_file)
ENDMACRO(lab_copy_files_into_directory_if_changed)

# Copy all files and directories in in_dir to out_dir.
# Subtrees remain intact.
MACRO(lab_copy_directory_if_changed target in_dir out_dir)
    #message("Copying directory ${in_dir}")
    FILE(GLOB_RECURSE in_file_list ${in_dir}/*)
    FOREACH(in_file ${in_file_list})
        if(NOT ${in_file} MATCHES ".*/CVS.*")
            STRING(REGEX REPLACE ${in_dir} ${out_dir} out_file
${in_file})
            lab_copy_file_if_changed(${in_file} ${out_file} ${target})
        endif(NOT ${in_file} MATCHES ".*/CVS.*")
    ENDFOREACH(in_file)
ENDMACRO(lab_copy_directory_if_changed)
