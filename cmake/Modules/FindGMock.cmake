include(FindPackageHandleStandardArgs)

find_path(GMOCK_INCLUDE_DIR
  NAMES gmock/gmock.h
  PATHS
    ${GMOCK_LOCATION}
    $ENV{GMOCK_LOCATION}
    ${GMOCK_LOCATION}/include
    $ENV{GMOCK_LOCATION}/include
)
mark_as_advanced(GMOCK_INCLUDE_DIR)

find_library(GMOCK_LIBRARY
  NAMES 
    libgmock.a
  PATHS
    ${GMOCK_LOCATION}
    ${GMOCK_LOCATION}/lib
    $ENV{GMOCK_LOCATION}/lib
    ${GMOCK_LOCATION}/lib/x11
    $ENV{GMOCK_LOCATION}/lib/x11
    /usr/lib64
    /usr/lib
    /usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}
    /usr/local/lib64
    /usr/local/lib
    /usr/local/lib/${CMAKE_LIBRARY_ARCHITECTURE}
    /usr/openwin/lib
    /usr/X11R6/lib
  DOC 
    "The GMock library"
)
mark_as_advanced(GMOCK_LIBRARY)

find_library(GMOCK_MAIN_LIBRARY
  NAMES 
    libgmock_main.a 
  PATHS
    ${GMOCK_LOCATION}
    ${GMOCK_LOCATION}/lib
    $ENV{GMOCK_LOCATION}/lib
    ${GMOCK_LOCATION}/lib/x11
    $ENV{GMOCK_LOCATION}/lib/x11
    /usr/lib64
    /usr/lib
    /usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}
    /usr/local/lib64
    /usr/local/lib
    /usr/local/lib/${CMAKE_LIBRARY_ARCHITECTURE}
    /usr/openwin/lib
    /usr/X11R6/lib
  DOC 
    "The GMock main library"
)
mark_as_advanced(GMOCK_MAIN_LIBRARY)

set(GMOCK_LIBRARIES ${GMOCK_LIBRARY} ${GMOCK_MAIN_LIBRARY})
mark_as_advanced(GMOCK_LIBRARIES)

find_package_handle_standard_args(GTest
  REQUIRED_VARS
    GMOCK_INCLUDE_DIR
    GMOCK_LIBRARY
    GMOCK_MAIN_LIBRARY
    GMOCK_LIBRARIES
)

# if(GMOCK_FOUND)
#   set(GMOCK_INCLUDE_DIRS ${GMOCK_INCLUDE_DIR})
#   set(GMOCK_LIBRARIES gtest)
#   set(GMOCK_MAIN_LIBRARIES GMOCK_main)
#     set(GMOCK_BOTH_LIBRARIES ${GMOCK_LIBRARIES} ${GMOCK_MAIN_LIBRARIES})
# endif()