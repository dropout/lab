include(FindPackageHandleStandardArgs)

find_path(GTEST_INCLUDE_DIR
	NAMES gtest/gtest.h
	PATHS
		${GTEST_LOCATION}
		$ENV{GTEST_LOCATION}
		${GTEST_LOCATION}/include
		$ENV{GTEST_LOCATION}/include
)
mark_as_advanced(GTEST_INCLUDE_DIR)

find_library(GTEST_LIBRARY
	NAMES 
		libgtest.a		
	PATHS
		${GTEST_LOCATION}
		${GTEST_LOCATION}/lib
		$ENV{GTEST_LOCATION}/lib
		${GTEST_LOCATION}/lib/x11
		$ENV{GTEST_LOCATION}/lib/x11
		/usr/lib64
		/usr/lib
		/usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}
		/usr/local/lib64
		/usr/local/lib
		/usr/local/lib/${CMAKE_LIBRARY_ARCHITECTURE}
		/usr/openwin/lib
		/usr/X11R6/lib
	DOC 
		"The GTest library"
)
mark_as_advanced(GTEST_LIBRARY)

find_library(GTEST_MAIN_LIBRARY
	NAMES 
		libgtest_main.a	
	PATHS
		${GTEST_LOCATION}
		${GTEST_LOCATION}/lib
		$ENV{GTEST_LOCATION}/lib
		${GTEST_LOCATION}/lib/x11
		$ENV{GTEST_LOCATION}/lib/x11
		/usr/lib64
		/usr/lib
		/usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}
		/usr/local/lib64
		/usr/local/lib
		/usr/local/lib/${CMAKE_LIBRARY_ARCHITECTURE}
		/usr/openwin/lib
		/usr/X11R6/lib
	DOC 
		"The GTest main library"
)
mark_as_advanced(GTEST_MAIN_LIBRARY)

set(GTEST_LIBRARIES	${GTEST_LIBRARY} ${GTEST_MAIN_LIBRARY})
mark_as_advanced(GTEST_LIBRARIES)

find_package_handle_standard_args(GTest
	REQUIRED_VARS
		GTEST_INCLUDE_DIR
		GTEST_LIBRARY
		GTEST_MAIN_LIBRARY
		GTEST_LIBRARIES
)

# if(GTEST_FOUND)
#   set(GTEST_INCLUDE_DIRS ${GTEST_INCLUDE_DIR})
#   set(GTEST_LIBRARIES gtest)
#   set(GTEST_MAIN_LIBRARIES gtest_main)
#     set(GTEST_BOTH_LIBRARIES ${GTEST_LIBRARIES} ${GTEST_MAIN_LIBRARIES})
# endif()